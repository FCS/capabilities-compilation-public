#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "zlib.h"

#define MAX_IN 2097152
#define MAX_OUT MAX_IN

char a[50] = "Hello World!";

uLong ucompSize;
uLong compSize;

int zlib_test(int argc, char *argv[], char *in, size_t len)
{
        char *input, *output, *original;

        //printf("%s\n", zlibVersion());

        if (!in) {
                input = a;
                ucompSize = strlen(a) + 1;
        } else {
                if (len > MAX_IN)
                        return -1;

                ucompSize = len;
                input = in;
        }

        compSize = compressBound(ucompSize);
        output = malloc(sizeof(char) * compSize);
        original = malloc(sizeof(char) * ucompSize);
        if (!output || !original)
                return -2;

        compress((Bytef *)output, &compSize, (Bytef *)input, ucompSize);
        uncompress((Bytef *)original, &ucompSize, (Bytef *)output, compSize);
        //printf("Uncompressed msg: %s\n", original);

        return 0;
}
