typedef long __intcap_t;
typedef long __uintcap_t;

typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;

typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int32_t __clock_t;
typedef double __double_t;
typedef float __float_t;






typedef __intcap_t __critical_t;
typedef __intcap_t __intfptr_t;
typedef __intcap_t __intptr_t;






typedef __int64_t __intmax_t;
typedef __int32_t __int_fast8_t;
typedef __int32_t __int_fast16_t;
typedef __int32_t __int_fast32_t;
typedef __int64_t __int_fast64_t;
typedef __int8_t __int_least8_t;
typedef __int16_t __int_least16_t;
typedef __int32_t __int_least32_t;
typedef __int64_t __int_least64_t;

typedef __int64_t __register_t;
typedef __int64_t f_register_t;





typedef __int64_t __ptrdiff_t;
typedef __int64_t __segsz_t;
typedef __uint64_t __size_t;
typedef __int64_t __ssize_t;




typedef __uintcap_t __uintfptr_t;
typedef __uintcap_t __uintptr_t;
typedef __int64_t __time_t;
typedef __uint64_t __uintmax_t;
typedef __uint32_t __uint_fast8_t;
typedef __uint32_t __uint_fast16_t;
typedef __uint32_t __uint_fast32_t;
typedef __uint64_t __uint_fast64_t;
typedef __uint8_t __uint_least8_t;
typedef __uint16_t __uint_least16_t;
typedef __uint32_t __uint_least32_t;
typedef __uint64_t __uint_least64_t;

typedef __uint64_t __u_register_t;




typedef __uint64_t __vm_offset_t;
typedef __uint64_t __vm_size_t;





typedef __uint64_t __vm_paddr_t;




typedef __int64_t __vm_ooffset_t;
typedef __uint64_t __vm_pindex_t;
typedef int ___wchar_t;
typedef __builtin_va_list __va_list;






typedef __va_list __gnuc_va_list;




typedef __int32_t __blksize_t;
typedef __int64_t __blkcnt_t;
typedef __int32_t __clockid_t;
typedef __uint32_t __fflags_t;
typedef __uint64_t __fsblkcnt_t;
typedef __uint64_t __fsfilcnt_t;
typedef __uint32_t __gid_t;
typedef __int64_t __id_t;
typedef __uint32_t __ino_t;
typedef long __key_t;
typedef __int32_t __lwpid_t;
typedef __uint16_t __mode_t;
typedef int __accmode_t;
typedef int __nl_item;
typedef __uint16_t __nlink_t;
typedef __int64_t __off_t;
typedef __int64_t __off64_t;
typedef __int32_t __pid_t;
typedef __int64_t __rlim_t;


typedef __uint8_t __sa_family_t;
typedef __uint32_t __socklen_t;
typedef long __suseconds_t;
typedef struct __timer *__timer_t;
typedef struct __mq *__mqd_t;
typedef __uint32_t __uid_t;
typedef unsigned int __useconds_t;
typedef int __cpuwhich_t;
typedef int __cpulevel_t;
typedef int __cpusetid_t;
typedef int __ct_rune_t;
typedef __ct_rune_t __rune_t;
typedef __ct_rune_t __wint_t;



typedef __uint_least16_t __char16_t;
typedef __uint_least32_t __char32_t;







typedef struct {
        long long __max_align1 ;
        long double __max_align2 ;
} __max_align_t;

typedef __uint32_t __dev_t;

typedef __uint32_t __fixpt_t;





typedef union {
 char __mbstate8[128];
 __int64_t _mbstateL;
} __mbstate_t;

typedef __uintmax_t __rman_res_t;


typedef __ptrdiff_t ptrdiff_t;





typedef __rune_t rune_t;





typedef __size_t size_t;





typedef ___wchar_t wchar_t;






typedef __max_align_t max_align_t;
 typedef size_t z_size_t;
typedef unsigned char Byte;

typedef unsigned int uInt;
typedef unsigned long uLong;





   typedef Byte Bytef;

typedef char charf;
typedef int intf;
typedef uInt uIntf;
typedef uLong uLongf;


   typedef void const *voidpc;
   typedef void *voidpf;
   typedef void *voidp;










   typedef unsigned z_crc_t;
static __inline __uint16_t
__bswap16_var(__uint16_t _x)
{

 return ((_x >> 8) | ((_x << 8) & 0xff00));
}

static __inline __uint32_t
__bswap32_var(__uint32_t _x)
{

 return ((_x >> 24) | ((_x >> 8) & 0xff00) | ((_x << 8) & 0xff0000) |
     ((_x << 24) & 0xff000000));
}

static __inline __uint64_t
__bswap64_var(__uint64_t _x)
{

 return ((_x >> 56) | ((_x >> 40) & 0xff00) | ((_x >> 24) & 0xff0000) |
     ((_x >> 8) & 0xff000000) | ((_x << 8) & ((__uint64_t)0xff << 32)) |
     ((_x << 24) & ((__uint64_t)0xff << 40)) |
     ((_x << 40) & ((__uint64_t)0xff << 48)) | ((_x << 56)));
}


struct pthread;
struct pthread_attr;
struct pthread_cond;
struct pthread_cond_attr;
struct pthread_mutex;
struct pthread_mutex_attr;
struct pthread_once;
struct pthread_rwlock;
struct pthread_rwlockattr;
struct pthread_barrier;
struct pthread_barrier_attr;
struct pthread_spinlock;
typedef struct pthread *pthread_t;


typedef struct pthread_attr *pthread_attr_t;
typedef struct pthread_mutex *pthread_mutex_t;
typedef struct pthread_mutex_attr *pthread_mutexattr_t;
typedef struct pthread_cond *pthread_cond_t;
typedef struct pthread_cond_attr *pthread_condattr_t;
typedef int pthread_key_t;
typedef struct pthread_once pthread_once_t;
typedef struct pthread_rwlock *pthread_rwlock_t;
typedef struct pthread_rwlockattr *pthread_rwlockattr_t;
typedef struct pthread_barrier *pthread_barrier_t;
typedef struct pthread_barrierattr *pthread_barrierattr_t;
typedef struct pthread_spinlock *pthread_spinlock_t;







typedef void *pthread_addr_t;
typedef void *(*pthread_startroutine_t)(void *);




struct pthread_once {
 int state;
 pthread_mutex_t mutex;
};


typedef unsigned char u_char;
typedef unsigned short u_short;
typedef unsigned int u_int;
typedef unsigned long u_long;

typedef unsigned short ushort;
typedef unsigned int uint;







typedef __int8_t int8_t;




typedef __int16_t int16_t;




typedef __int32_t int32_t;




typedef __int64_t int64_t;




typedef __uint8_t uint8_t;




typedef __uint16_t uint16_t;




typedef __uint32_t uint32_t;




typedef __uint64_t uint64_t;




typedef __intptr_t intptr_t;



typedef __uintptr_t uintptr_t;



typedef __intmax_t intmax_t;



typedef __uintmax_t uintmax_t;






typedef __uint64_t vaddr_t;

typedef __uint8_t u_int8_t;
typedef __uint16_t u_int16_t;
typedef __uint32_t u_int32_t;
typedef __uint64_t u_int64_t;

typedef __uint64_t u_quad_t;
typedef __int64_t quad_t;
typedef quad_t * qaddr_t;

typedef char * caddr_t;
typedef const char * c_caddr_t;


typedef __blksize_t blksize_t;



typedef __cpuwhich_t cpuwhich_t;
typedef __cpulevel_t cpulevel_t;
typedef __cpusetid_t cpusetid_t;


typedef __blkcnt_t blkcnt_t;




typedef __clock_t clock_t;




typedef __clockid_t clockid_t;



typedef __critical_t critical_t;
typedef __int64_t daddr_t;


typedef __dev_t dev_t;




typedef __fflags_t fflags_t;



typedef __fixpt_t fixpt_t;


typedef __fsblkcnt_t fsblkcnt_t;
typedef __fsfilcnt_t fsfilcnt_t;




typedef __gid_t gid_t;




typedef __uint32_t in_addr_t;




typedef __uint16_t in_port_t;




typedef __id_t id_t;




typedef __ino_t ino_t;




typedef __key_t key_t;




typedef __lwpid_t lwpid_t;




typedef __mode_t mode_t;




typedef __accmode_t accmode_t;




typedef __nlink_t nlink_t;




typedef __off_t off_t;




typedef __off64_t off64_t;




typedef __pid_t pid_t;



typedef __register_t register_t;


typedef __rlim_t rlim_t;



typedef __int64_t sbintime_t;

typedef __segsz_t segsz_t;







typedef __ssize_t ssize_t;




typedef __suseconds_t suseconds_t;




typedef __time_t time_t;




typedef __timer_t timer_t;




typedef __mqd_t mqd_t;



typedef __u_register_t u_register_t;


typedef __uid_t uid_t;




typedef __useconds_t useconds_t;





typedef unsigned long cap_ioctl_t;




struct cap_rights;

typedef struct cap_rights cap_rights_t;


typedef __vm_offset_t vm_offset_t;
typedef __vm_ooffset_t vm_ooffset_t;
typedef __vm_paddr_t vm_paddr_t;
typedef __vm_pindex_t vm_pindex_t;
typedef __vm_size_t vm_size_t;

typedef __rman_res_t rman_res_t;
static __inline __uint16_t
__bitcount16(__uint16_t _x)
{

 _x = (_x & 0x5555) + ((_x & 0xaaaa) >> 1);
 _x = (_x & 0x3333) + ((_x & 0xcccc) >> 2);
 _x = (_x + (_x >> 4)) & 0x0f0f;
 _x = (_x + (_x >> 8)) & 0x00ff;
 return (_x);
}

static __inline __uint32_t
__bitcount32(__uint32_t _x)
{

 _x = (_x & 0x55555555) + ((_x & 0xaaaaaaaa) >> 1);
 _x = (_x & 0x33333333) + ((_x & 0xcccccccc) >> 2);
 _x = (_x + (_x >> 4)) & 0x0f0f0f0f;
 _x = (_x + (_x >> 8));
 _x = (_x + (_x >> 16)) & 0x000000ff;
 return (_x);
}


static __inline __uint64_t
__bitcount64(__uint64_t _x)
{

 _x = (_x & 0x5555555555555555) + ((_x & 0xaaaaaaaaaaaaaaaa) >> 1);
 _x = (_x & 0x3333333333333333) + ((_x & 0xcccccccccccccccc) >> 2);
 _x = (_x + (_x >> 4)) & 0x0f0f0f0f0f0f0f0f;
 _x = (_x + (_x >> 8));
 _x = (_x + (_x >> 16));
 _x = (_x + (_x >> 32)) & 0x000000ff;
 return (_x);
}
typedef struct __sigset {
 __uint32_t __bits[4];
} __sigset_t;
struct timeval {
 time_t tv_sec;
 suseconds_t tv_usec;
};
struct timespec {
 time_t tv_sec;
 long tv_nsec;
};
struct itimerspec {
 struct timespec it_interval;
 struct timespec it_value;
};

typedef unsigned long __fd_mask;

typedef __fd_mask fd_mask;




typedef __sigset_t sigset_t;
typedef struct fd_set {
 __fd_mask __fds_bits[(((1024) + (((sizeof(__fd_mask) * 8)) - 1)) / ((sizeof(__fd_mask) * 8)))];
} fd_set;
int pselect(int, fd_set *restrict, fd_set *restrict, fd_set *restrict,
 const struct timespec *restrict, const sigset_t *restrict);



int select(int, fd_set *, fd_set *, fd_set *, struct timeval *);
int ftruncate(int, off_t);



off_t lseek(int, off_t, int);



void * mmap(void *, size_t, int, int, int, off_t);



int truncate(const char *, off_t);





typedef __va_list va_list;
void _exit(int) __attribute__((__noreturn__));
int access(const char *, int);
unsigned int alarm(unsigned int);
int chdir(const char *);
int chown(const char *, uid_t, gid_t);
int close(int);
void closefrom(int);
int dup(int);
int dup2(int, int);
int execl(const char *, const char *, ...) __attribute__((__sentinel__));
int execle(const char *, const char *, ...);
int execlp(const char *, const char *, ...) __attribute__((__sentinel__));
int execv(const char *, char * const *);
int execve(const char *, char * const *, char * const *);
int execvp(const char *, char * const *);
pid_t fork(void);
long fpathconf(int, int);
char *getcwd(char *, size_t);
gid_t getegid(void);
uid_t geteuid(void);
gid_t getgid(void);
int getgroups(int, gid_t []);
char *getlogin(void);
pid_t getpgrp(void);
pid_t getpid(void);
pid_t getppid(void);
uid_t getuid(void);
int isatty(int);
int link(const char *, const char *);




long pathconf(const char *, int);
int pause(void);
int pipe(int *);
ssize_t read(int, void *, size_t);
int rmdir(const char *);
int setgid(gid_t);
int setpgid(pid_t, pid_t);
pid_t setsid(void);
int setuid(uid_t);
unsigned int sleep(unsigned int);
long sysconf(int);
pid_t tcgetpgrp(int);
int tcsetpgrp(int, pid_t);
char *ttyname(int);
int ttyname_r(int, char *, size_t);
int unlink(const char *);
ssize_t write(int, const void *, size_t);



size_t confstr(int, char *, size_t);


int getopt(int, char * const [], const char *);

extern char *optarg;
extern int optind, opterr, optopt;





int fsync(int);
int fdatasync(int);
int getlogin_r(char *, int);




int fchown(int, uid_t, gid_t);
ssize_t readlink(const char * restrict, char * restrict, size_t);


int gethostname(char *, size_t);
int setegid(gid_t);
int seteuid(uid_t);




int getsid(pid_t _pid);
int fchdir(int);
int getpgid(pid_t _pid);
int lchown(const char *, uid_t, gid_t);
ssize_t pread(int, void *, size_t, off_t);
ssize_t pwrite(int, const void *, size_t, off_t);
int faccessat(int, const char *, int, int);
int fchownat(int, const char *, uid_t, gid_t, int);
int fexecve(int, char *const [], char *const []);
int linkat(int, const char *, int, const char *, int);
ssize_t readlinkat(int, const char * restrict, char * restrict, size_t);
int symlinkat(const char *, int, const char *);
int unlinkat(int, const char *, int);
int symlink(const char * restrict, const char * restrict);




char *crypt(const char *, const char *);
long gethostid(void);
int lockf(int, int, off_t);
int nice(int);
int setregid(gid_t, gid_t);
int setreuid(uid_t, uid_t);



void swab(const void * restrict, void * restrict, ssize_t);


void sync(void);




int brk(const void *);
int chroot(const char *);
int getdtablesize(void);
int getpagesize(void) __attribute__((__const__));
char *getpass(const char *);
void *sbrk(intptr_t);



char *getwd(char *);
useconds_t
  ualarm(useconds_t, useconds_t);
int usleep(useconds_t);
pid_t vfork(void) __attribute__((__returns_twice__));



struct timeval;

struct crypt_data {
 int initialized;
 char __buf[256];
};

int acct(const char *);
int async_daemon(void);
int check_utility_compat(const char *);
const char *
  crypt_get_format(void);
char *crypt_r(const char *, const char *, struct crypt_data *);
int crypt_set_format(const char *);
int dup3(int, int, int);
int eaccess(const char *, int);
void endusershell(void);
int exect(const char *, char * const *, char * const *);
int execvP(const char *, const char *, char * const *);
int feature_present(const char *);
char *fflagstostr(u_long);
int getdomainname(char *, int);
int getgrouplist(const char *, gid_t, gid_t *, int *);
int getloginclass(char *, size_t);
mode_t getmode(const void *, mode_t);
int getosreldate(void);
int getpeereid(int, uid_t *, gid_t *);
int getresgid(gid_t *, gid_t *, gid_t *);
int getresuid(uid_t *, uid_t *, uid_t *);
char *getusershell(void);
int initgroups(const char *, gid_t);
int iruserok(unsigned long, int, const char *, const char *);
int iruserok_sa(const void *, int, int, const char *, const char *);
int issetugid(void);
void __FreeBSD_libc_enter_restricted_mode(void);
long lpathconf(const char *, int);

char *mkdtemp(char *);



int mknod(const char *, mode_t, dev_t);



int mkstemp(char *);


int mkstemps(char *, int);

char *mktemp(char *);


int nfssvc(int, void *);
int nlm_syscall(int, int, int, char **);
int pipe2(int *, int);
int profil(char *, size_t, vm_offset_t, int);
int rcmd(char **, int, const char *, const char *, const char *, int *);
int rcmd_af(char **, int, const char *,
  const char *, const char *, int *, int);
int rcmdsh(char **, int, const char *,
  const char *, const char *, const char *);
char *re_comp(const char *);
int re_exec(const char *);
int reboot(int);
int revoke(const char *);
pid_t rfork(int);
pid_t rfork_thread(int, void *, int (*)(void *), void *);
int rresvport(int *);
int rresvport_af(int *, int);
int ruserok(const char *, int, const char *, const char *);






int setdomainname(const char *, int);
int setgroups(int, const gid_t *);
void sethostid(long);
int sethostname(const char *, int);
int setlogin(const char *);
int setloginclass(const char *);
void *setmode(const char *);
int setpgrp(pid_t, pid_t);
void setproctitle(const char *_fmt, ...) __attribute__((__format__ (__printf0__, 1, 2)));
int setresgid(gid_t, gid_t, gid_t);
int setresuid(uid_t, uid_t, uid_t);
int setrgid(gid_t);
int setruid(uid_t);
void setusershell(void);
int strtofflags(char **, u_long *, u_long *);
int swapon(const char *);
int swapoff(const char *);
int syscall(int, ...);
off_t __syscall(quad_t, ...);
int undelete(const char *);
int unwhiteout(const char *);
void *valloc(size_t);



extern int optreset;



ssize_t write_c(int, __attribute__((address_space(200))) const void *, size_t);
typedef voidpf (*alloc_func) (voidpf opaque, uInt items, uInt size);
typedef void (*free_func) (voidpf opaque, voidpf address);

struct internal_state;

typedef struct z_stream_s {
            Bytef *next_in;
    uInt avail_in;
    uLong total_in;

    Bytef *next_out;
    uInt avail_out;
    uLong total_out;

            char *msg;
    struct internal_state *state;

    alloc_func zalloc;
    free_func zfree;
    voidpf opaque;

    int data_type;

    uLong adler;
    uLong reserved;
} z_stream;

typedef z_stream *z_streamp;





typedef struct gz_header_s {
    int text;
    uLong time;
    int xflags;
    int os;
    Bytef *extra;
    uInt extra_len;
    uInt extra_max;
    Bytef *name;
    uInt name_max;
    Bytef *comment;
    uInt comm_max;
    int hcrc;
    int done;

} gz_header;

typedef gz_header *gz_headerp;
typedef unsigned (*in_func) (void *, unsigned char * *);

typedef int (*out_func) (void *, unsigned char *, unsigned);
typedef struct gzFile_s *gzFile;

struct gzFile_s {
    unsigned have;
    unsigned char *next;
    off_t pos;
};





int bcmp(const void *, const void *, size_t) __attribute__((__pure__));
void bcopy(const void *, void *, size_t);
void bzero(void *, size_t);


void explicit_bzero(void *, size_t);


int ffs(int) __attribute__((__const__));


int ffsl(long) __attribute__((__const__));
int ffsll(long long) __attribute__((__const__));
int fls(int) __attribute__((__const__));
int flsl(long) __attribute__((__const__));
int flsll(long long) __attribute__((__const__));


char *index(const char *, int) __attribute__((__pure__));
char *rindex(const char *, int) __attribute__((__pure__));

int strcasecmp(const char *, const char *) __attribute__((__pure__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__pure__));



typedef struct _xlocale *locale_t;
int strcasecmp_l(const char *, const char *, locale_t);
int strncasecmp_l(const char *, const char *, size_t, locale_t);
void *memccpy(void * restrict, const void * restrict, int, size_t);

void *memchr(const void *, int, size_t) __attribute__((__pure__));

void *memrchr(const void *, int, size_t) __attribute__((__pure__));

int memcmp(const void *, const void *, size_t) __attribute__((__pure__));
void *memcpy(void * restrict, const void * restrict, size_t);

void *memmem(const void *, size_t, const void *, size_t) __attribute__((__pure__));

void *memmove(void *, const void *, size_t);
void *memset(void *, int, size_t);

char *stpcpy(char * restrict, const char * restrict);
char *stpncpy(char * restrict, const char * restrict, size_t);


char *strcasestr(const char *, const char *) __attribute__((__pure__));

char *strcat(char * restrict, const char * restrict);
char *strchr(const char *, int) __attribute__((__pure__));

char *strchrnul(const char*, int) __attribute__((__pure__));

int strcmp(const char *, const char *) __attribute__((__pure__));
int strcoll(const char *, const char *);
char *strcpy(char * restrict, const char * restrict);
size_t strcspn(const char *, const char *) __attribute__((__pure__));

char *strdup(const char *) __attribute__((__malloc__));

char *strerror(int);

int strerror_r(int, char *, size_t);


size_t strlcat(char * restrict, const char * restrict, size_t);
size_t strlcpy(char * restrict, const char * restrict, size_t);

size_t strlen(const char *) __attribute__((__pure__));

void strmode(int, char *);

char *strncat(char * restrict, const char * restrict, size_t);
int strncmp(const char *, const char *, size_t) __attribute__((__pure__));
char *strncpy(char * restrict, const char * restrict, size_t);

char *strndup(const char *, size_t) __attribute__((__malloc__));
size_t strnlen(const char *, size_t) __attribute__((__pure__));


char *strnstr(const char *, const char *, size_t) __attribute__((__pure__));

char *strpbrk(const char *, const char *) __attribute__((__pure__));
char *strrchr(const char *, int) __attribute__((__pure__));

char *strsep(char **, const char *);


char *strsignal(int);

size_t strspn(const char *, const char *) __attribute__((__pure__));
char *strstr(const char *, const char *) __attribute__((__pure__));
char *strtok(char * restrict, const char * restrict);

char *strtok_r(char *, const char *, char **);

size_t strxfrm(char * restrict, const char * restrict, size_t);
__attribute__((address_space(200))) const void *
  memchr_c_const(__attribute__((address_space(200))) const void *, int, size_t) __attribute__((__pure__));
__attribute__((address_space(200))) void
 *memchr_c(__attribute__((address_space(200))) const void *, int, size_t) __attribute__((__pure__));
int memcmp_c(__attribute__((address_space(200))) const void *, __attribute__((address_space(200))) const void *, size_t)
     __attribute__((__pure__));
__attribute__((address_space(200))) void
 *memcpy_c(__attribute__((address_space(200))) void * restrict,
     __attribute__((address_space(200))) const void * restrict, size_t);
void *memcpy_c_fromcap(void * restrict,
     __attribute__((address_space(200))) const void * restrict, size_t);
__attribute__((address_space(200))) void
 *memcpy_c_tocap(__attribute__((address_space(200))) void * restrict,
     const void * restrict, size_t);
__attribute__((address_space(200))) void
 *memmove_c(__attribute__((address_space(200))) void * restrict,
     __attribute__((address_space(200))) const void * restrict, size_t);
__attribute__((address_space(200))) void
 *memset_c(__attribute__((address_space(200))) void *, int, size_t);

__attribute__((address_space(200))) char
 *strchr_c(__attribute__((address_space(200))) const char *, int) __attribute__((__pure__));
int strcmp_c(__attribute__((address_space(200))) const char *,
     __attribute__((address_space(200))) const char *s2) __attribute__((__pure__));
__attribute__((address_space(200))) char
 *strcpy_c(__attribute__((address_space(200))) char * restrict,
     __attribute__((address_space(200))) const char * restrict);
int strncmp_c(__attribute__((address_space(200))) const char *, __attribute__((address_space(200))) const char *,
     size_t) __attribute__((__pure__));
__attribute__((address_space(200))) char
*strncpy_c(__attribute__((address_space(200))) char * restrict, __attribute__((address_space(200))) const char * restrict,
     size_t);
char *strncpy_c_fromcap(char * restrict,
     __attribute__((address_space(200))) const char * restrict, size_t);
__attribute__((address_space(200))) char
 *strncpy_c_tocap(__attribute__((address_space(200))) char * restrict,
     const char * restrict, size_t);
size_t strnlen_c(__attribute__((address_space(200))) const char *, size_t) __attribute__((__pure__));


int timingsafe_bcmp(const void *, const void *, size_t);
int timingsafe_memcmp(const void *, const void *, size_t);




int strcoll_l(const char *, const char *, locale_t);
size_t strxfrm_l(char *, const char *, size_t, locale_t);
typedef struct {
 int quot;
 int rem;
} div_t;

typedef struct {
 long quot;
 long rem;
} ldiv_t;
extern int __mb_cur_max;
extern int ___mb_cur_max(void);


void abort(void);
int abs(int) __attribute__((__const__));
int atexit(void (*)(void));
double atof(const char *);
int atoi(const char *);
long atol(const char *);
void *bsearch(const void *, const void *, size_t,
     size_t, int (*)(const void *, const void *));
void *calloc(size_t, size_t) __attribute__((__malloc__)) __attribute__((__warn_unused_result__))
                                     ;
div_t div(int, int) __attribute__((__const__));
void exit(int);
void free(void *);
char *getenv(const char *);
long labs(long) __attribute__((__const__));
ldiv_t ldiv(long, long) __attribute__((__const__));
void *malloc(size_t) __attribute__((__malloc__)) __attribute__((__warn_unused_result__)) ;
int mblen(const char *, size_t);
size_t mbstowcs(wchar_t * restrict , const char * restrict, size_t);
int mbtowc(wchar_t * restrict, const char * restrict, size_t);
void qsort(void *, size_t, size_t,
     int (*)(const void *, const void *));
int rand(void);
void *realloc(void *, size_t) __attribute__((__warn_unused_result__)) ;
void srand(unsigned);
double strtod(const char * restrict, char ** restrict);
float strtof(const char * restrict, char ** restrict);
long strtol(const char * restrict, char ** restrict, int);
long double
  strtold(const char * restrict, char ** restrict);
unsigned long
  strtoul(const char * restrict, char ** restrict, int);
int system(const char *);
int wctomb(char *, wchar_t);
size_t wcstombs(char * restrict, const wchar_t * restrict, size_t);
typedef struct {
 long long quot;
 long long rem;
} lldiv_t;


long long
  atoll(const char *);

long long
  llabs(long long) __attribute__((__const__));

lldiv_t lldiv(long long, long long) __attribute__((__const__));

long long
  strtoll(const char * restrict, char ** restrict, int);

unsigned long long
  strtoull(const char * restrict, char ** restrict, int);


void _Exit(int);






void * aligned_alloc(size_t, size_t) __attribute__((__malloc__))
                    ;
int at_quick_exit(void (*)(void));
void  quick_exit(int);





char *realpath(const char * restrict, char * restrict);


int rand_r(unsigned *);


int posix_memalign(void **, size_t, size_t) __attribute__((__nonnull__(1)));
int setenv(const char *, const char *, int);
int unsetenv(const char *);



int getsubopt(char **, char *const *, char **);
long a64l(const char *);
double drand48(void);

double erand48(unsigned short[3]);


int grantpt(int);
char *initstate(unsigned int, char *, size_t);
long jrand48(unsigned short[3]);
char *l64a(long);
void lcong48(unsigned short[7]);
long lrand48(void);




long mrand48(void);
long nrand48(unsigned short[3]);
int posix_openpt(int);
char *ptsname(int);
int putenv(char *);
long random(void);
unsigned short
 *seed48(unsigned short[3]);
char *setstate( char *);
void srand48(long);
void srandom(unsigned int);
int unlockpt(int);



extern const char *malloc_conf;
extern void (*malloc_message)(void *, const char *);
void abort2(const char *, int, void **) __attribute__((__noreturn__));
__uint32_t
  arc4random(void);
void arc4random_addrandom(unsigned char *, int);
void arc4random_buf(void *, size_t);
void arc4random_stir(void);
__uint32_t
  arc4random_uniform(__uint32_t);





char *getbsize(int *, long *);

char *cgetcap(char *, const char *, int);
int cgetclose(void);
int cgetent(char **, char **, const char *);
int cgetfirst(char **, char **);
int cgetmatch(const char *, const char *);
int cgetnext(char **, char **);
int cgetnum(char *, const char *, long *);
int cgetset(const char *);
int cgetstr(char *, const char *, char **);
int cgetustr(char *, const char *, char **);

int daemon(int, int);
char *devname(__dev_t, __mode_t);
char *devname_r(__dev_t, __mode_t, char *, int);
char *fdevname(int);
char *fdevname_r(int, char *, int);
int getloadavg(double [], int);
const char *
  getprogname(void);

int heapsort(void *, size_t, size_t, int (*)(const void *, const void *));





int l64a_r(long, char *, int);
int mergesort(void *, size_t, size_t, int (*)(const void *, const void *));



int mkostemp(char *, int);
int mkostemps(char *, int, int);
void qsort_r(void *, size_t, size_t, void *,
     int (*)(void *, const void *, const void *));
int radixsort(const unsigned char **, int, const unsigned char *,
     unsigned);
void *reallocarray(void *, size_t, size_t) __attribute__((__warn_unused_result__))
                    ;
void *reallocf(void *, size_t) ;
int rpmatch(const char *);
void setprogname(const char *);
int sradixsort(const unsigned char **, int, const unsigned char *,
     unsigned);
void sranddev(void);
void srandomdev(void);
long long
 strtonum(const char *, long long, long long, const char **);


__int64_t
  strtoq(const char *, char **, int);
__uint64_t
  strtouq(const char *, char **, int);


long strtol_c(__attribute__((address_space(200))) const char * restrict,
     __attribute__((address_space(200))) char ** restrict, int);
long double
  strtold_c(__attribute__((address_space(200))) const char * restrict,
     __attribute__((address_space(200))) char ** restrict);


int timsort(void *base, size_t nel, size_t width,
     int (*compar) (const void *, const void *));

extern char *suboptarg;
typedef unsigned char uch;
typedef uch uchf;
typedef unsigned short ush;
typedef ush ushf;
typedef unsigned long ulg;

extern char * const z_errmsg[10];
    extern uLong adler32_combine64 (uLong, uLong, off_t);
    extern uLong crc32_combine64 (uLong, uLong, off_t);
voidpf zcalloc (voidpf opaque, unsigned items, unsigned size);



void zcfree (voidpf opaque, voidpf ptr);
typedef struct ct_data_s {
    union {
        ush freq;
        ush code;
    } fc;
    union {
        ush dad;
        ush len;
    } dl;
} ct_data;






typedef struct static_tree_desc_s static_tree_desc;

typedef struct tree_desc_s {
    ct_data *dyn_tree;
    int max_code;
    const static_tree_desc *stat_desc;
} tree_desc;

typedef ush Pos;
typedef Pos Posf;
typedef unsigned IPos;





typedef struct internal_state {
    z_streamp strm;
    int status;
    Bytef *pending_buf;
    ulg pending_buf_size;
    Bytef *pending_out;
    ulg pending;
    int wrap;
    gz_headerp gzhead;
    ulg gzindex;
    Byte method;
    int last_flush;



    uInt w_size;
    uInt w_bits;
    uInt w_mask;

    Bytef *window;
    ulg window_size;




    Posf *prev;





    Posf *head;

    uInt ins_h;
    uInt hash_size;
    uInt hash_bits;
    uInt hash_mask;

    uInt hash_shift;






    long block_start;




    uInt match_length;
    IPos prev_match;
    int match_available;
    uInt strstart;
    uInt match_start;
    uInt lookahead;

    uInt prev_length;




    uInt max_chain_length;





    uInt max_lazy_match;
    int level;
    int strategy;

    uInt good_match;


    int nice_match;



    struct ct_data_s dyn_ltree[(2*(256 +1+29)+1)];
    struct ct_data_s dyn_dtree[2*30 +1];
    struct ct_data_s bl_tree[2*19 +1];

    struct tree_desc_s l_desc;
    struct tree_desc_s d_desc;
    struct tree_desc_s bl_desc;

    ush bl_count[15 +1];


    int heap[2*(256 +1+29)+1];
    int heap_len;
    int heap_max;




    uch depth[2*(256 +1+29)+1];



    uchf *l_buf;

    uInt lit_bufsize;
    uInt last_lit;

    ushf *d_buf;





    ulg opt_len;
    ulg static_len;
    uInt matches;
    uInt insert;






    ush bi_buf;



    int bi_valid;




    ulg high_water;






} deflate_state;
void _tr_init (deflate_state *s);
int _tr_tally (deflate_state *s, unsigned dist, unsigned lc);
void _tr_flush_block (deflate_state *s, charf *buf, ulg stored_len, int last);

void _tr_flush_bits (deflate_state *s);
void _tr_align (deflate_state *s);
void _tr_stored_block (deflate_state *s, charf *buf, ulg stored_len, int last);
  extern const uch _length_code[];
  extern const uch _dist_code[];

const char deflate_copyright[] =
   " deflate 1.2.11 Copyright 1995-2017 Jean-loup Gailly and Mark Adler ";
typedef enum {
    need_more,
    block_done,
    finish_started,
    finish_done
} block_state;

typedef block_state (*compress_func) (deflate_state *s, int flush);


static int deflateStateCheck (z_streamp strm);
static void slide_hash (deflate_state *s);
static void fill_window (deflate_state *s);
static block_state deflate_stored (deflate_state *s, int flush);
static block_state deflate_fast (deflate_state *s, int flush);

static block_state deflate_slow (deflate_state *s, int flush);

static block_state deflate_rle (deflate_state *s, int flush);
static block_state deflate_huff (deflate_state *s, int flush);
static void lm_init (deflate_state *s);
static void putShortMSB (deflate_state *s, uInt b);
static void flush_pending (z_streamp strm);
static unsigned read_buf (z_streamp strm, Bytef *buf, unsigned size);





static uInt longest_match (deflate_state *s, IPos cur_match);
typedef struct config_s {
   ush good_length;
   ush max_lazy;
   ush nice_length;
   ush max_chain;
   compress_func func;
} config;







static const config configuration_table[10] = {

        {0, 0, 0, 0, deflate_stored},
        {4, 4, 8, 4, deflate_fast},
        {4, 5, 16, 8, deflate_fast},
        {4, 6, 32, 32, deflate_fast},

        {4, 4, 16, 16, deflate_slow},
        {8, 16, 32, 32, deflate_slow},
        {8, 16, 128, 128, deflate_slow},
        {8, 32, 128, 256, deflate_slow},
        {32, 128, 258, 1024, deflate_slow},
        {32, 258, 258, 4096, deflate_slow}};
static void slide_hash(s)
    deflate_state *s;
{
    unsigned n, m;
    Posf *p;
    uInt wsize = s->w_size;

    n = s->hash_size;
    p = &s->head[n];
    do {
        m = *--p;
        *p = (Pos)(m >= wsize ? m - wsize : 0);
    } while (--n);
    n = wsize;

    p = &s->prev[n];
    do {
        m = *--p;
        *p = (Pos)(m >= wsize ? m - wsize : 0);



    } while (--n);

}


int deflateInit_(strm, level, version, stream_size)
    z_streamp strm;
    int level;
    const char *version;
    int stream_size;
{
    return deflateInit2_(strm, level, 8, 15, 8,
                         0, version, stream_size);

}


int deflateInit2_(strm, level, method, windowBits, memLevel, strategy,
                  version, stream_size)
    z_streamp strm;
    int level;
    int method;
    int windowBits;
    int memLevel;
    int strategy;
    const char *version;
    int stream_size;
{
    deflate_state *s;
    int wrap = 1;
    static const char my_version[] = "1.2.11";

    ushf *overlay;




    if (version == 0 || version[0] != my_version[0] ||
        stream_size != sizeof(z_stream)) {
        return (-6);
    }
    if (strm == 0) return (-2);

    strm->msg = 0;
    if (strm->zalloc == (alloc_func)0) {



        strm->zalloc = zcalloc;
        strm->opaque = (voidpf)0;

    }
    if (strm->zfree == (free_func)0)



        strm->zfree = zcfree;





    if (level == (-1)) level = 6;


    if (windowBits < 0) {
        wrap = 0;
        windowBits = -windowBits;
    }

    else if (windowBits > 15) {
        wrap = 2;
        windowBits -= 16;
    }

    if (memLevel < 1 || memLevel > 9 || method != 8 ||
        windowBits < 8 || windowBits > 15 || level < 0 || level > 9 ||
        strategy < 0 || strategy > 4 || (windowBits == 8 && wrap != 1)) {
        return (-2);
    }
    if (windowBits == 8) windowBits = 9;
    s = (deflate_state *) (*((strm)->zalloc))((strm)->opaque, (1), (sizeof(deflate_state)));
    if (s == 0) return (-4);
    strm->state = (struct internal_state *)s;
    s->strm = strm;
    s->status = 42;

    s->wrap = wrap;
    s->gzhead = 0;
    s->w_bits = (uInt)windowBits;
    s->w_size = 1 << s->w_bits;
    s->w_mask = s->w_size - 1;

    s->hash_bits = (uInt)memLevel + 7;
    s->hash_size = 1 << s->hash_bits;
    s->hash_mask = s->hash_size - 1;
    s->hash_shift = ((s->hash_bits+3 -1)/3);

    s->window = (Bytef *) (*((strm)->zalloc))((strm)->opaque, (s->w_size), (2*sizeof(Byte)));
    s->prev = (Posf *) (*((strm)->zalloc))((strm)->opaque, (s->w_size), (sizeof(Pos)));
    s->head = (Posf *) (*((strm)->zalloc))((strm)->opaque, (s->hash_size), (sizeof(Pos)));

    s->high_water = 0;

    s->lit_bufsize = 1 << (memLevel + 6);

    overlay = (ushf *) (*((strm)->zalloc))((strm)->opaque, (s->lit_bufsize), (sizeof(ush)+2));
    s->pending_buf = (uchf *) overlay;
    s->pending_buf_size = (ulg)s->lit_bufsize * (sizeof(ush)+2L);

    if (s->window == 0 || s->prev == 0 || s->head == 0 ||
        s->pending_buf == 0) {
        s->status = 666;
        strm->msg = z_errmsg[2 -((-4))];
        deflateEnd (strm);
        return (-4);
    }
    s->d_buf = overlay + s->lit_bufsize/sizeof(ush);
    s->l_buf = s->pending_buf + (1+sizeof(ush))*s->lit_bufsize;

    s->level = level;
    s->strategy = strategy;
    s->method = (Byte)method;

    return deflateReset(strm);
}




static int deflateStateCheck (strm)
    z_streamp strm;
{
    deflate_state *s;
    if (strm == 0 ||
        strm->zalloc == (alloc_func)0 || strm->zfree == (free_func)0)
        return 1;
    s = strm->state;
    if (s == 0 || s->strm != strm || (s->status != 42 &&

                                           s->status != 57 &&

                                           s->status != 69 &&
                                           s->status != 73 &&
                                           s->status != 91 &&
                                           s->status != 103 &&
                                           s->status != 113 &&
                                           s->status != 666))
        return 1;
    return 0;
}


int deflateSetDictionary (strm, dictionary, dictLength)
    z_streamp strm;
    const Bytef *dictionary;
    uInt dictLength;
{
    deflate_state *s;
    uInt str, n;
    int wrap;
    unsigned avail;
            unsigned char *next;

    if (deflateStateCheck(strm) || dictionary == 0)
        return (-2);
    s = strm->state;
    wrap = s->wrap;
    if (wrap == 2 || (wrap == 1 && s->status != 42) || s->lookahead)
        return (-2);


    if (wrap == 1)
        strm->adler = adler32(strm->adler, dictionary, dictLength);
    s->wrap = 0;


    if (dictLength >= s->w_size) {
        if (wrap == 0) {
            s->head[s->hash_size-1] = 0; memset((Bytef *)s->head, 0, (unsigned)(s->hash_size-1)*sizeof(*s->head));;
            s->strstart = 0;
            s->block_start = 0L;
            s->insert = 0;
        }
        dictionary += dictLength - s->w_size;
        dictLength = s->w_size;
    }


    avail = strm->avail_in;
    next = strm->next_in;
    strm->avail_in = dictLength;
    strm->next_in = ( Bytef *)dictionary;
    fill_window(s);
    while (s->lookahead >= 3) {
        str = s->strstart;
        n = s->lookahead - (3 -1);
        do {
            (s->ins_h = (((s->ins_h)<<s->hash_shift) ^ (s->window[str + 3 -1])) & s->hash_mask);

            s->prev[str & s->w_mask] = s->head[s->ins_h];

            s->head[s->ins_h] = (Pos)str;
            str++;
        } while (--n);
        s->strstart = str;
        s->lookahead = 3 -1;
        fill_window(s);
    }
    s->strstart += s->lookahead;
    s->block_start = (long)s->strstart;
    s->insert = s->lookahead;
    s->lookahead = 0;
    s->match_length = s->prev_length = 3 -1;
    s->match_available = 0;
    strm->next_in = next;
    strm->avail_in = avail;
    s->wrap = wrap;
    return 0;
}


int deflateGetDictionary (strm, dictionary, dictLength)
    z_streamp strm;
    Bytef *dictionary;
    uInt *dictLength;
{
    deflate_state *s;
    uInt len;

    if (deflateStateCheck(strm))
        return (-2);
    s = strm->state;
    len = s->strstart + s->lookahead;
    if (len > s->w_size)
        len = s->w_size;
    if (dictionary != 0 && len)
        memcpy(dictionary, s->window + s->strstart + s->lookahead - len, len);
    if (dictLength != 0)
        *dictLength = len;
    return 0;
}


int deflateResetKeep (strm)
    z_streamp strm;
{
    deflate_state *s;

    if (deflateStateCheck(strm)) {
        return (-2);
    }

    strm->total_in = strm->total_out = 0;
    strm->msg = 0;
    strm->data_type = 2;

    s = (deflate_state *)strm->state;
    s->pending = 0;
    s->pending_out = s->pending_buf;

    if (s->wrap < 0) {
        s->wrap = -s->wrap;
    }
    s->status =

        s->wrap == 2 ? 57 :

        s->wrap ? 42 : 113;
    strm->adler =

        s->wrap == 2 ? crc32(0L, 0, 0) :

        adler32(0L, 0, 0);
    s->last_flush = 0;

    _tr_init(s);

    return 0;
}

int deflateReset (strm)
    z_streamp strm;
{
    int ret;

    ret = deflateResetKeep(strm);
    if (ret == 0)
        lm_init(strm->state);
    return ret;
}

int deflateSetHeader (strm, head)
    z_streamp strm;
    gz_headerp head;
{
    if (deflateStateCheck(strm) || strm->state->wrap != 2)
        return (-2);
    strm->state->gzhead = head;
    return 0;
}


int deflatePending (strm, pending, bits)
    unsigned *pending;
    int *bits;
    z_streamp strm;
{
    if (deflateStateCheck(strm)) return (-2);
    if (pending != 0)
        *pending = strm->state->pending;
    if (bits != 0)
        *bits = strm->state->bi_valid;
    return 0;
}


int deflatePrime (strm, bits, value)
    z_streamp strm;
    int bits;
    int value;
{
    deflate_state *s;
    int put;

    if (deflateStateCheck(strm)) return (-2);
    s = strm->state;
    if ((Bytef *)(s->d_buf) < s->pending_out + ((16 + 7) >> 3))
        return (-5);
    do {
        put = 16 - s->bi_valid;
        if (put > bits)
            put = bits;
        s->bi_buf |= (ush)((value & ((1 << put) - 1)) << s->bi_valid);
        s->bi_valid += put;
        _tr_flush_bits(s);
        value >>= put;
        bits -= put;
    } while (bits);
    return 0;
}


int deflateParams(strm, level, strategy)
    z_streamp strm;
    int level;
    int strategy;
{
    deflate_state *s;
    compress_func func;

    if (deflateStateCheck(strm)) return (-2);
    s = strm->state;




    if (level == (-1)) level = 6;

    if (level < 0 || level > 9 || strategy < 0 || strategy > 4) {
        return (-2);
    }
    func = configuration_table[s->level].func;

    if ((strategy != s->strategy || func != configuration_table[level].func) &&
        s->high_water) {

        int err = deflate(strm, 5);
        if (err == (-2))
            return err;
        if (strm->avail_out == 0)
            return (-5);
    }
    if (s->level != level) {
        if (s->level == 0 && s->matches != 0) {
            if (s->matches == 1)
                slide_hash(s);
            else
                s->head[s->hash_size-1] = 0; memset((Bytef *)s->head, 0, (unsigned)(s->hash_size-1)*sizeof(*s->head));;
            s->matches = 0;
        }
        s->level = level;
        s->max_lazy_match = configuration_table[level].max_lazy;
        s->good_match = configuration_table[level].good_length;
        s->nice_match = configuration_table[level].nice_length;
        s->max_chain_length = configuration_table[level].max_chain;
    }
    s->strategy = strategy;
    return 0;
}


int deflateTune(strm, good_length, max_lazy, nice_length, max_chain)
    z_streamp strm;
    int good_length;
    int max_lazy;
    int nice_length;
    int max_chain;
{
    deflate_state *s;

    if (deflateStateCheck(strm)) return (-2);
    s = strm->state;
    s->good_match = (uInt)good_length;
    s->max_lazy_match = (uInt)max_lazy;
    s->nice_match = nice_length;
    s->max_chain_length = (uInt)max_chain;
    return 0;
}
uLong deflateBound(strm, sourceLen)
    z_streamp strm;
    uLong sourceLen;
{
    deflate_state *s;
    uLong complen, wraplen;


    complen = sourceLen +
              ((sourceLen + 7) >> 3) + ((sourceLen + 63) >> 6) + 5;


    if (deflateStateCheck(strm))
        return complen + 6;


    s = strm->state;
    switch (s->wrap) {
    case 0:
        wraplen = 0;
        break;
    case 1:
        wraplen = 6 + (s->strstart ? 4 : 0);
        break;

    case 2:
        wraplen = 18;
        if (s->gzhead != 0) {
            Bytef *str;
            if (s->gzhead->extra != 0)
                wraplen += 2 + s->gzhead->extra_len;
            str = s->gzhead->name;
            if (str != 0)
                do {
                    wraplen++;
                } while (*str++);
            str = s->gzhead->comment;
            if (str != 0)
                do {
                    wraplen++;
                } while (*str++);
            if (s->gzhead->hcrc)
                wraplen += 2;
        }
        break;

    default:
        wraplen = 6;
    }


    if (s->w_bits != 15 || s->hash_bits != 8 + 7)
        return complen + wraplen;


    return sourceLen + (sourceLen >> 12) + (sourceLen >> 14) +
           (sourceLen >> 25) + 13 - 6 + wraplen;
}






static void putShortMSB (s, b)
    deflate_state *s;
    uInt b;
{
    {s->pending_buf[s->pending++] = (Bytef)((Byte)(b >> 8));};
    {s->pending_buf[s->pending++] = (Bytef)((Byte)(b & 0xff));};
}







static void flush_pending(strm)
    z_streamp strm;
{
    unsigned len;
    deflate_state *s = strm->state;

    _tr_flush_bits(s);
    len = s->pending;
    if (len > strm->avail_out) len = strm->avail_out;
    if (len == 0) return;

    memcpy(strm->next_out, s->pending_out, len);
    strm->next_out += len;
    s->pending_out += len;
    strm->total_out += len;
    strm->avail_out -= len;
    s->pending -= len;
    if (s->pending == 0) {
        s->pending_out = s->pending_buf;
    }
}
int deflate (strm, flush)
    z_streamp strm;
    int flush;
{
    int old_flush;
    deflate_state *s;

    if (deflateStateCheck(strm) || flush > 5 || flush < 0) {
        return (-2);
    }
    s = strm->state;

    if (strm->next_out == 0 ||
        (strm->avail_in != 0 && strm->next_in == 0) ||
        (s->status == 666 && flush != 4)) {
        return (strm->msg = z_errmsg[2 -((-2))], ((-2)));
    }
    if (strm->avail_out == 0) return (strm->msg = z_errmsg[2 -((-5))], ((-5)));

    old_flush = s->last_flush;
    s->last_flush = flush;


    if (s->pending != 0) {
        flush_pending(strm);
        if (strm->avail_out == 0) {






            s->last_flush = -1;
            return 0;
        }





    } else if (strm->avail_in == 0 && (((flush) * 2) - ((flush) > 4 ? 9 : 0)) <= (((old_flush) * 2) - ((old_flush) > 4 ? 9 : 0)) &&
               flush != 4) {
        return (strm->msg = z_errmsg[2 -((-5))], ((-5)));
    }


    if (s->status == 666 && strm->avail_in != 0) {
        return (strm->msg = z_errmsg[2 -((-5))], ((-5)));
    }


    if (s->status == 42) {

        uInt header = (8 + ((s->w_bits-8)<<4)) << 8;
        uInt level_flags;

        if (s->strategy >= 2 || s->level < 2)
            level_flags = 0;
        else if (s->level < 6)
            level_flags = 1;
        else if (s->level == 6)
            level_flags = 2;
        else
            level_flags = 3;
        header |= (level_flags << 6);
        if (s->strstart != 0) header |= 0x20;
        header += 31 - (header % 31);

        putShortMSB(s, header);


        if (s->strstart != 0) {
            putShortMSB(s, (uInt)(strm->adler >> 16));
            putShortMSB(s, (uInt)(strm->adler & 0xffff));
        }
        strm->adler = adler32(0L, 0, 0);
        s->status = 113;


        flush_pending(strm);
        if (s->pending != 0) {
            s->last_flush = -1;
            return 0;
        }
    }

    if (s->status == 57) {

        strm->adler = crc32(0L, 0, 0);
        {s->pending_buf[s->pending++] = (Bytef)(31);};
        {s->pending_buf[s->pending++] = (Bytef)(139);};
        {s->pending_buf[s->pending++] = (Bytef)(8);};
        if (s->gzhead == 0) {
            {s->pending_buf[s->pending++] = (Bytef)(0);};
            {s->pending_buf[s->pending++] = (Bytef)(0);};
            {s->pending_buf[s->pending++] = (Bytef)(0);};
            {s->pending_buf[s->pending++] = (Bytef)(0);};
            {s->pending_buf[s->pending++] = (Bytef)(0);};
            {s->pending_buf[s->pending++] = (Bytef)(s->level == 9 ? 2 : (s->strategy >= 2 || s->level < 2 ? 4 : 0));};


            {s->pending_buf[s->pending++] = (Bytef)(3);};
            s->status = 113;


            flush_pending(strm);
            if (s->pending != 0) {
                s->last_flush = -1;
                return 0;
            }
        }
        else {
            {s->pending_buf[s->pending++] = (Bytef)((s->gzhead->text ? 1 : 0) + (s->gzhead->hcrc ? 2 : 0) + (s->gzhead->extra == 0 ? 0 : 4) + (s->gzhead->name == 0 ? 0 : 8) + (s->gzhead->comment == 0 ? 0 : 16));};





            {s->pending_buf[s->pending++] = (Bytef)((Byte)(s->gzhead->time & 0xff));};
            {s->pending_buf[s->pending++] = (Bytef)((Byte)((s->gzhead->time >> 8) & 0xff));};
            {s->pending_buf[s->pending++] = (Bytef)((Byte)((s->gzhead->time >> 16) & 0xff));};
            {s->pending_buf[s->pending++] = (Bytef)((Byte)((s->gzhead->time >> 24) & 0xff));};
            {s->pending_buf[s->pending++] = (Bytef)(s->level == 9 ? 2 : (s->strategy >= 2 || s->level < 2 ? 4 : 0));};


            {s->pending_buf[s->pending++] = (Bytef)(s->gzhead->os & 0xff);};
            if (s->gzhead->extra != 0) {
                {s->pending_buf[s->pending++] = (Bytef)(s->gzhead->extra_len & 0xff);};
                {s->pending_buf[s->pending++] = (Bytef)((s->gzhead->extra_len >> 8) & 0xff);};
            }
            if (s->gzhead->hcrc)
                strm->adler = crc32(strm->adler, s->pending_buf,
                                    s->pending);
            s->gzindex = 0;
            s->status = 69;
        }
    }
    if (s->status == 69) {
        if (s->gzhead->extra != 0) {
            ulg beg = s->pending;
            uInt left = (s->gzhead->extra_len & 0xffff) - s->gzindex;
            while (s->pending + left > s->pending_buf_size) {
                uInt copy = s->pending_buf_size - s->pending;
                memcpy(s->pending_buf + s->pending,
                        s->gzhead->extra + s->gzindex, copy);
                s->pending = s->pending_buf_size;
                do { if (s->gzhead->hcrc && s->pending > (beg)) strm->adler = crc32(strm->adler, s->pending_buf + (beg), s->pending - (beg)); } while (0);
                s->gzindex += copy;
                flush_pending(strm);
                if (s->pending != 0) {
                    s->last_flush = -1;
                    return 0;
                }
                beg = 0;
                left -= copy;
            }
            memcpy(s->pending_buf + s->pending,
                    s->gzhead->extra + s->gzindex, left);
            s->pending += left;
            do { if (s->gzhead->hcrc && s->pending > (beg)) strm->adler = crc32(strm->adler, s->pending_buf + (beg), s->pending - (beg)); } while (0);
            s->gzindex = 0;
        }
        s->status = 73;
    }
    if (s->status == 73) {
        if (s->gzhead->name != 0) {
            ulg beg = s->pending;
            int val;
            do {
                if (s->pending == s->pending_buf_size) {
                    do { if (s->gzhead->hcrc && s->pending > (beg)) strm->adler = crc32(strm->adler, s->pending_buf + (beg), s->pending - (beg)); } while (0);
                    flush_pending(strm);
                    if (s->pending != 0) {
                        s->last_flush = -1;
                        return 0;
                    }
                    beg = 0;
                }
                val = s->gzhead->name[s->gzindex++];
                {s->pending_buf[s->pending++] = (Bytef)(val);};
            } while (val != 0);
            do { if (s->gzhead->hcrc && s->pending > (beg)) strm->adler = crc32(strm->adler, s->pending_buf + (beg), s->pending - (beg)); } while (0);
            s->gzindex = 0;
        }
        s->status = 91;
    }
    if (s->status == 91) {
        if (s->gzhead->comment != 0) {
            ulg beg = s->pending;
            int val;
            do {
                if (s->pending == s->pending_buf_size) {
                    do { if (s->gzhead->hcrc && s->pending > (beg)) strm->adler = crc32(strm->adler, s->pending_buf + (beg), s->pending - (beg)); } while (0);
                    flush_pending(strm);
                    if (s->pending != 0) {
                        s->last_flush = -1;
                        return 0;
                    }
                    beg = 0;
                }
                val = s->gzhead->comment[s->gzindex++];
                {s->pending_buf[s->pending++] = (Bytef)(val);};
            } while (val != 0);
            do { if (s->gzhead->hcrc && s->pending > (beg)) strm->adler = crc32(strm->adler, s->pending_buf + (beg), s->pending - (beg)); } while (0);
        }
        s->status = 103;
    }
    if (s->status == 103) {
        if (s->gzhead->hcrc) {
            if (s->pending + 2 > s->pending_buf_size) {
                flush_pending(strm);
                if (s->pending != 0) {
                    s->last_flush = -1;
                    return 0;
                }
            }
            {s->pending_buf[s->pending++] = (Bytef)((Byte)(strm->adler & 0xff));};
            {s->pending_buf[s->pending++] = (Bytef)((Byte)((strm->adler >> 8) & 0xff));};
            strm->adler = crc32(0L, 0, 0);
        }
        s->status = 113;


        flush_pending(strm);
        if (s->pending != 0) {
            s->last_flush = -1;
            return 0;
        }
    }




    if (strm->avail_in != 0 || s->lookahead != 0 ||
        (flush != 0 && s->status != 666)) {
        block_state bstate;

        bstate = s->level == 0 ? deflate_stored(s, flush) :
                 s->strategy == 2 ? deflate_huff(s, flush) :
                 s->strategy == 3 ? deflate_rle(s, flush) :
                 (*(configuration_table[s->level].func))(s, flush);

        if (bstate == finish_started || bstate == finish_done) {
            s->status = 666;
        }
        if (bstate == need_more || bstate == finish_started) {
            if (strm->avail_out == 0) {
                s->last_flush = -1;
            }
            return 0;







        }
        if (bstate == block_done) {
            if (flush == 1) {
                _tr_align(s);
            } else if (flush != 5) {
                _tr_stored_block(s, (char*)0, 0L, 0);



                if (flush == 3) {
                    s->head[s->hash_size-1] = 0; memset((Bytef *)s->head, 0, (unsigned)(s->hash_size-1)*sizeof(*s->head));;
                    if (s->lookahead == 0) {
                        s->strstart = 0;
                        s->block_start = 0L;
                        s->insert = 0;
                    }
                }
            }
            flush_pending(strm);
            if (strm->avail_out == 0) {
              s->last_flush = -1;
              return 0;
            }
        }
    }

    if (flush != 4) return 0;
    if (s->wrap <= 0) return 1;



    if (s->wrap == 2) {
        {s->pending_buf[s->pending++] = (Bytef)((Byte)(strm->adler & 0xff));};
        {s->pending_buf[s->pending++] = (Bytef)((Byte)((strm->adler >> 8) & 0xff));};
        {s->pending_buf[s->pending++] = (Bytef)((Byte)((strm->adler >> 16) & 0xff));};
        {s->pending_buf[s->pending++] = (Bytef)((Byte)((strm->adler >> 24) & 0xff));};
        {s->pending_buf[s->pending++] = (Bytef)((Byte)(strm->total_in & 0xff));};
        {s->pending_buf[s->pending++] = (Bytef)((Byte)((strm->total_in >> 8) & 0xff));};
        {s->pending_buf[s->pending++] = (Bytef)((Byte)((strm->total_in >> 16) & 0xff));};
        {s->pending_buf[s->pending++] = (Bytef)((Byte)((strm->total_in >> 24) & 0xff));};
    }
    else

    {
        putShortMSB(s, (uInt)(strm->adler >> 16));
        putShortMSB(s, (uInt)(strm->adler & 0xffff));
    }
    flush_pending(strm);



    if (s->wrap > 0) s->wrap = -s->wrap;
    return s->pending != 0 ? 0 : 1;
}


int deflateEnd (strm)
    z_streamp strm;
{
    int status;

    if (deflateStateCheck(strm)) return (-2);

    status = strm->state->status;


    {if (strm->state->pending_buf) (*((strm)->zfree))((strm)->opaque, (voidpf)(strm->state->pending_buf));};
    {if (strm->state->head) (*((strm)->zfree))((strm)->opaque, (voidpf)(strm->state->head));};
    {if (strm->state->prev) (*((strm)->zfree))((strm)->opaque, (voidpf)(strm->state->prev));};
    {if (strm->state->window) (*((strm)->zfree))((strm)->opaque, (voidpf)(strm->state->window));};

    (*((strm)->zfree))((strm)->opaque, (voidpf)(strm->state));
    strm->state = 0;

    return status == 113 ? (-3) : 0;
}






int deflateCopy (dest, source)
    z_streamp dest;
    z_streamp source;
{



    deflate_state *ds;
    deflate_state *ss;
    ushf *overlay;


    if (deflateStateCheck(source) || dest == 0) {
        return (-2);
    }

    ss = source->state;

    memcpy((voidpf)dest, (voidpf)source, sizeof(z_stream));

    ds = (deflate_state *) (*((dest)->zalloc))((dest)->opaque, (1), (sizeof(deflate_state)));
    if (ds == 0) return (-4);
    dest->state = (struct internal_state *) ds;
    memcpy((voidpf)ds, (voidpf)ss, sizeof(deflate_state));
    ds->strm = dest;

    ds->window = (Bytef *) (*((dest)->zalloc))((dest)->opaque, (ds->w_size), (2*sizeof(Byte)));
    ds->prev = (Posf *) (*((dest)->zalloc))((dest)->opaque, (ds->w_size), (sizeof(Pos)));
    ds->head = (Posf *) (*((dest)->zalloc))((dest)->opaque, (ds->hash_size), (sizeof(Pos)));
    overlay = (ushf *) (*((dest)->zalloc))((dest)->opaque, (ds->lit_bufsize), (sizeof(ush)+2));
    ds->pending_buf = (uchf *) overlay;

    if (ds->window == 0 || ds->prev == 0 || ds->head == 0 ||
        ds->pending_buf == 0) {
        deflateEnd (dest);
        return (-4);
    }

    memcpy(ds->window, ss->window, ds->w_size * 2 * sizeof(Byte));
    memcpy((voidpf)ds->prev, (voidpf)ss->prev, ds->w_size * sizeof(Pos));
    memcpy((voidpf)ds->head, (voidpf)ss->head, ds->hash_size * sizeof(Pos));
    memcpy(ds->pending_buf, ss->pending_buf, (uInt)ds->pending_buf_size);

    ds->pending_out = ds->pending_buf + (ss->pending_out - ss->pending_buf);
    ds->d_buf = overlay + ds->lit_bufsize/sizeof(ush);
    ds->l_buf = ds->pending_buf + (1+sizeof(ush))*ds->lit_bufsize;

    ds->l_desc.dyn_tree = ds->dyn_ltree;
    ds->d_desc.dyn_tree = ds->dyn_dtree;
    ds->bl_desc.dyn_tree = ds->bl_tree;

    return 0;

}
static unsigned read_buf(strm, buf, size)
    z_streamp strm;
    Bytef *buf;
    unsigned size;
{
    unsigned len = strm->avail_in;

    if (len > size) len = size;
    if (len == 0) return 0;

    strm->avail_in -= len;

    memcpy(buf, strm->next_in, len);
    if (strm->state->wrap == 1) {
        strm->adler = adler32(strm->adler, buf, len);
    }

    else if (strm->state->wrap == 2) {
        strm->adler = crc32(strm->adler, buf, len);
    }

    strm->next_in += len;
    strm->total_in += len;

    return len;
}




static void lm_init (s)
    deflate_state *s;
{
    s->window_size = (ulg)2L*s->w_size;

    s->head[s->hash_size-1] = 0; memset((Bytef *)s->head, 0, (unsigned)(s->hash_size-1)*sizeof(*s->head));;



    s->max_lazy_match = configuration_table[s->level].max_lazy;
    s->good_match = configuration_table[s->level].good_length;
    s->nice_match = configuration_table[s->level].nice_length;
    s->max_chain_length = configuration_table[s->level].max_chain;

    s->strstart = 0;
    s->block_start = 0L;
    s->lookahead = 0;
    s->insert = 0;
    s->match_length = s->prev_length = 3 -1;
    s->match_available = 0;
    s->ins_h = 0;





}
static uInt longest_match(s, cur_match)
    deflate_state *s;
    IPos cur_match;
{
    unsigned chain_length = s->max_chain_length;
    register Bytef *scan = s->window + s->strstart;
    register Bytef *match;
    register int len;
    int best_len = (int)s->prev_length;
    int nice_match = s->nice_match;
    IPos limit = s->strstart > (IPos)((s)->w_size-(258 +3 +1)) ?
        s->strstart - (IPos)((s)->w_size-(258 +3 +1)) : 0;



    Posf *prev = s->prev;
    uInt wmask = s->w_mask;
    register Bytef *strend = s->window + s->strstart + 258;
    register Byte scan_end1 = scan[best_len-1];
    register Byte scan_end = scan[best_len];





                                                                    ;


    if (s->prev_length >= s->good_match) {
        chain_length >>= 2;
    }



    if ((uInt)nice_match > s->lookahead) nice_match = (int)s->lookahead;

                                                                              ;

    do {
                                                    ;
        match = s->window + cur_match;
        if (match[best_len] != scan_end ||
            match[best_len-1] != scan_end1 ||
            *match != *scan ||
            *++match != scan[1]) continue;







        scan += 2, match++;
                                            ;




        do {
        } while (*++scan == *++match && *++scan == *++match &&
                 *++scan == *++match && *++scan == *++match &&
                 *++scan == *++match && *++scan == *++match &&
                 *++scan == *++match && *++scan == *++match &&
                 scan < strend);

                                                                           ;

        len = 258 - (int)(strend - scan);
        scan = strend - 258;



        if (len > best_len) {
            s->match_start = cur_match;
            best_len = len;
            if (len >= nice_match) break;



            scan_end1 = scan[best_len-1];
            scan_end = scan[best_len];

        }
    } while ((cur_match = prev[cur_match & wmask]) > limit
             && --chain_length != 0);

    if ((uInt)best_len <= s->lookahead) return (uInt)best_len;
    return s->lookahead;
}
static void fill_window(s)
    deflate_state *s;
{
    unsigned n;
    unsigned more;
    uInt wsize = s->w_size;

                                                                    ;

    do {
        more = (unsigned)(s->window_size -(ulg)s->lookahead -(ulg)s->strstart);


        if (sizeof(int) <= 2) {
            if (more == 0 && s->strstart == 0 && s->lookahead == 0) {
                more = wsize;

            } else if (more == (unsigned)(-1)) {



                more--;
            }
        }




        if (s->strstart >= wsize+((s)->w_size-(258 +3 +1))) {

            memcpy(s->window, s->window+wsize, (unsigned)wsize - more);
            s->match_start -= wsize;
            s->strstart -= wsize;
            s->block_start -= (long) wsize;
            slide_hash(s);
            more += wsize;
        }
        if (s->strm->avail_in == 0) break;
                                     ;

        n = read_buf(s->strm, s->window + s->strstart + s->lookahead, more);
        s->lookahead += n;


        if (s->lookahead + s->insert >= 3) {
            uInt str = s->strstart - s->insert;
            s->ins_h = s->window[str];
            (s->ins_h = (((s->ins_h)<<s->hash_shift) ^ (s->window[str + 1])) & s->hash_mask);



            while (s->insert) {
                (s->ins_h = (((s->ins_h)<<s->hash_shift) ^ (s->window[str + 3 -1])) & s->hash_mask);

                s->prev[str & s->w_mask] = s->head[s->ins_h];

                s->head[s->ins_h] = (Pos)str;
                str++;
                s->insert--;
                if (s->lookahead + s->insert < 3)
                    break;
            }
        }




    } while (s->lookahead < (258 +3 +1) && s->strm->avail_in != 0);
    if (s->high_water < s->window_size) {
        ulg curr = s->strstart + (ulg)(s->lookahead);
        ulg init;

        if (s->high_water < curr) {



            init = s->window_size - curr;
            if (init > 258)
                init = 258;
            memset(s->window + curr, 0, (unsigned)init);
            s->high_water = curr + init;
        }
        else if (s->high_water < (ulg)curr + 258) {




            init = (ulg)curr + 258 - s->high_water;
            if (init > s->window_size - s->high_water)
                init = s->window_size - s->high_water;
            memset(s->window + s->high_water, 0, (unsigned)init);
            s->high_water += init;
        }
    }


                                        ;
}
static block_state deflate_stored(s, flush)
    deflate_state *s;
    int flush;
{




    unsigned min_block = ((s->pending_buf_size - 5) > (s->w_size) ? (s->w_size) : (s->pending_buf_size - 5));





    unsigned len, left, have, last = 0;
    unsigned used = s->strm->avail_in;
    do {




        len = 65535;
        have = (s->bi_valid + 42) >> 3;
        if (s->strm->avail_out < have)
            break;

        have = s->strm->avail_out - have;
        left = s->strstart - s->block_start;
        if (len > (ulg)left + s->strm->avail_in)
            len = left + s->strm->avail_in;
        if (len > have)
            len = have;






        if (len < min_block && ((len == 0 && flush != 4) ||
                                flush == 0 ||
                                len != left + s->strm->avail_in))
            break;




        last = flush == 4 && len == left + s->strm->avail_in ? 1 : 0;
        _tr_stored_block(s, (char *)0, 0L, last);


        s->pending_buf[s->pending - 4] = len;
        s->pending_buf[s->pending - 3] = len >> 8;
        s->pending_buf[s->pending - 2] = ~len;
        s->pending_buf[s->pending - 1] = ~len >> 8;


        flush_pending(s->strm);
        if (left) {
            if (left > len)
                left = len;
            memcpy(s->strm->next_out, s->window + s->block_start, left);
            s->strm->next_out += left;
            s->strm->avail_out -= left;
            s->strm->total_out += left;
            s->block_start += left;
            len -= left;
        }




        if (len) {
            read_buf(s->strm, s->strm->next_out, len);
            s->strm->next_out += len;
            s->strm->avail_out -= len;
            s->strm->total_out += len;
        }
    } while (last == 0);







    used -= s->strm->avail_in;
    if (used) {



        if (used >= s->w_size) {
            s->matches = 2;
            memcpy(s->window, s->strm->next_in - s->w_size, s->w_size);
            s->strstart = s->w_size;
        }
        else {
            if (s->window_size - s->strstart <= used) {

                s->strstart -= s->w_size;
                memcpy(s->window, s->window + s->w_size, s->strstart);
                if (s->matches < 2)
                    s->matches++;
            }
            memcpy(s->window + s->strstart, s->strm->next_in - used, used);
            s->strstart += used;
        }
        s->block_start = s->strstart;
        s->insert += ((used) > (s->w_size - s->insert) ? (s->w_size - s->insert) : (used));
    }
    if (s->high_water < s->strstart)
        s->high_water = s->strstart;


    if (last)
        return finish_done;


    if (flush != 0 && flush != 4 &&
        s->strm->avail_in == 0 && (long)s->strstart == s->block_start)
        return block_done;


    have = s->window_size - s->strstart - 1;
    if (s->strm->avail_in > have && s->block_start >= (long)s->w_size) {

        s->block_start -= s->w_size;
        s->strstart -= s->w_size;
        memcpy(s->window, s->window + s->w_size, s->strstart);
        if (s->matches < 2)
            s->matches++;
        have += s->w_size;
    }
    if (have > s->strm->avail_in)
        have = s->strm->avail_in;
    if (have) {
        read_buf(s->strm, s->window + s->strstart, have);
        s->strstart += have;
    }
    if (s->high_water < s->strstart)
        s->high_water = s->strstart;






    have = (s->bi_valid + 42) >> 3;

    have = ((s->pending_buf_size - have) > (65535) ? (65535) : (s->pending_buf_size - have));
    min_block = ((have) > (s->w_size) ? (s->w_size) : (have));
    left = s->strstart - s->block_start;
    if (left >= min_block ||
        ((left || flush == 4) && flush != 0 &&
         s->strm->avail_in == 0 && left <= have)) {
        len = ((left) > (have) ? (have) : (left));
        last = flush == 4 && s->strm->avail_in == 0 &&
               len == left ? 1 : 0;
        _tr_stored_block(s, (charf *)s->window + s->block_start, len, last);
        s->block_start += len;
        flush_pending(s->strm);
    }


    return last ? finish_started : need_more;
}
static block_state deflate_fast(s, flush)
    deflate_state *s;
    int flush;
{
    IPos hash_head;
    int bflush;

    for (;;) {





        if (s->lookahead < (258 +3 +1)) {
            fill_window(s);
            if (s->lookahead < (258 +3 +1) && flush == 0) {
                return need_more;
            }
            if (s->lookahead == 0) break;
        }




        hash_head = 0;
        if (s->lookahead >= 3) {
            ((s->ins_h = (((s->ins_h)<<s->hash_shift) ^ (s->window[(s->strstart) + (3 -1)])) & s->hash_mask), hash_head = s->prev[(s->strstart) & s->w_mask] = s->head[s->ins_h], s->head[s->ins_h] = (Pos)(s->strstart));
        }




        if (hash_head != 0 && s->strstart - hash_head <= ((s)->w_size-(258 +3 +1))) {




            s->match_length = longest_match (s, hash_head);

        }
        if (s->match_length >= 3) {
                                                                        ;

            { uch len = (uch)(s->match_length - 3); ush dist = (ush)(s->strstart - s->match_start); s->d_buf[s->last_lit] = dist; s->l_buf[s->last_lit++] = len; dist--; s->dyn_ltree[_length_code[len]+256 +1].fc.freq++; s->dyn_dtree[((dist) < 256 ? _dist_code[dist] : _dist_code[256+((dist)>>7)])].fc.freq++; bflush = (s->last_lit == s->lit_bufsize-1); };


            s->lookahead -= s->match_length;





            if (s->match_length <= s->max_lazy_match &&
                s->lookahead >= 3) {
                s->match_length--;
                do {
                    s->strstart++;
                    ((s->ins_h = (((s->ins_h)<<s->hash_shift) ^ (s->window[(s->strstart) + (3 -1)])) & s->hash_mask), hash_head = s->prev[(s->strstart) & s->w_mask] = s->head[s->ins_h], s->head[s->ins_h] = (Pos)(s->strstart));



                } while (--s->match_length != 0);
                s->strstart++;
            } else

            {
                s->strstart += s->match_length;
                s->match_length = 0;
                s->ins_h = s->window[s->strstart];
                (s->ins_h = (((s->ins_h)<<s->hash_shift) ^ (s->window[s->strstart+1])) & s->hash_mask);






            }
        } else {

                                                          ;
            { uch cc = (s->window[s->strstart]); s->d_buf[s->last_lit] = 0; s->l_buf[s->last_lit++] = cc; s->dyn_ltree[cc].fc.freq++; bflush = (s->last_lit == s->lit_bufsize-1); };
            s->lookahead--;
            s->strstart++;
        }
        if (bflush) { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (0)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (0) ? finish_started : need_more; };
    }
    s->insert = s->strstart < 3 -1 ? s->strstart : 3 -1;
    if (flush == 4) {
        { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (1)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (1) ? finish_started : need_more; };
        return finish_done;
    }
    if (s->last_lit)
        { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (0)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (0) ? finish_started : need_more; };
    return block_done;
}







static block_state deflate_slow(s, flush)
    deflate_state *s;
    int flush;
{
    IPos hash_head;
    int bflush;


    for (;;) {





        if (s->lookahead < (258 +3 +1)) {
            fill_window(s);
            if (s->lookahead < (258 +3 +1) && flush == 0) {
                return need_more;
            }
            if (s->lookahead == 0) break;
        }




        hash_head = 0;
        if (s->lookahead >= 3) {
            ((s->ins_h = (((s->ins_h)<<s->hash_shift) ^ (s->window[(s->strstart) + (3 -1)])) & s->hash_mask), hash_head = s->prev[(s->strstart) & s->w_mask] = s->head[s->ins_h], s->head[s->ins_h] = (Pos)(s->strstart));
        }



        s->prev_length = s->match_length, s->prev_match = s->match_start;
        s->match_length = 3 -1;

        if (hash_head != 0 && s->prev_length < s->max_lazy_match &&
            s->strstart - hash_head <= ((s)->w_size-(258 +3 +1))) {




            s->match_length = longest_match (s, hash_head);


            if (s->match_length <= 5 && (s->strategy == 1

                || (s->match_length == 3 &&
                    s->strstart - s->match_start > 4096)

                )) {




                s->match_length = 3 -1;
            }
        }



        if (s->prev_length >= 3 && s->match_length <= s->prev_length) {
            uInt max_insert = s->strstart + s->lookahead - 3;


                                                                        ;

            { uch len = (uch)(s->prev_length - 3); ush dist = (ush)(s->strstart -1 - s->prev_match); s->d_buf[s->last_lit] = dist; s->l_buf[s->last_lit++] = len; dist--; s->dyn_ltree[_length_code[len]+256 +1].fc.freq++; s->dyn_dtree[((dist) < 256 ? _dist_code[dist] : _dist_code[256+((dist)>>7)])].fc.freq++; bflush = (s->last_lit == s->lit_bufsize-1); };







            s->lookahead -= s->prev_length-1;
            s->prev_length -= 2;
            do {
                if (++s->strstart <= max_insert) {
                    ((s->ins_h = (((s->ins_h)<<s->hash_shift) ^ (s->window[(s->strstart) + (3 -1)])) & s->hash_mask), hash_head = s->prev[(s->strstart) & s->w_mask] = s->head[s->ins_h], s->head[s->ins_h] = (Pos)(s->strstart));
                }
            } while (--s->prev_length != 0);
            s->match_available = 0;
            s->match_length = 3 -1;
            s->strstart++;

            if (bflush) { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (0)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (0) ? finish_started : need_more; };

        } else if (s->match_available) {




                                                            ;
            { uch cc = (s->window[s->strstart-1]); s->d_buf[s->last_lit] = 0; s->l_buf[s->last_lit++] = cc; s->dyn_ltree[cc].fc.freq++; bflush = (s->last_lit == s->lit_bufsize-1); };
            if (bflush) {
                { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (0)); s->block_start = s->strstart; flush_pending(s->strm); ; };
            }
            s->strstart++;
            s->lookahead--;
            if (s->strm->avail_out == 0) return need_more;
        } else {



            s->match_available = 1;
            s->strstart++;
            s->lookahead--;
        }
    }
                                             ;
    if (s->match_available) {
                                                        ;
        { uch cc = (s->window[s->strstart-1]); s->d_buf[s->last_lit] = 0; s->l_buf[s->last_lit++] = cc; s->dyn_ltree[cc].fc.freq++; bflush = (s->last_lit == s->lit_bufsize-1); };
        s->match_available = 0;
    }
    s->insert = s->strstart < 3 -1 ? s->strstart : 3 -1;
    if (flush == 4) {
        { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (1)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (1) ? finish_started : need_more; };
        return finish_done;
    }
    if (s->last_lit)
        { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (0)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (0) ? finish_started : need_more; };
    return block_done;
}







static block_state deflate_rle(s, flush)
    deflate_state *s;
    int flush;
{
    int bflush;
    uInt prev;
    Bytef *scan, *strend;

    for (;;) {




        if (s->lookahead <= 258) {
            fill_window(s);
            if (s->lookahead <= 258 && flush == 0) {
                return need_more;
            }
            if (s->lookahead == 0) break;
        }


        s->match_length = 0;
        if (s->lookahead >= 3 && s->strstart > 0) {
            scan = s->window + s->strstart - 1;
            prev = *scan;
            if (prev == *++scan && prev == *++scan && prev == *++scan) {
                strend = s->window + s->strstart + 258;
                do {
                } while (prev == *++scan && prev == *++scan &&
                         prev == *++scan && prev == *++scan &&
                         prev == *++scan && prev == *++scan &&
                         prev == *++scan && prev == *++scan &&
                         scan < strend);
                s->match_length = 258 - (uInt)(strend - scan);
                if (s->match_length > s->lookahead)
                    s->match_length = s->lookahead;
            }
                                                                           ;
        }


        if (s->match_length >= 3) {
                                                                         ;

            { uch len = (uch)(s->match_length - 3); ush dist = (ush)(1); s->d_buf[s->last_lit] = dist; s->l_buf[s->last_lit++] = len; dist--; s->dyn_ltree[_length_code[len]+256 +1].fc.freq++; s->dyn_dtree[((dist) < 256 ? _dist_code[dist] : _dist_code[256+((dist)>>7)])].fc.freq++; bflush = (s->last_lit == s->lit_bufsize-1); };

            s->lookahead -= s->match_length;
            s->strstart += s->match_length;
            s->match_length = 0;
        } else {

                                                          ;
            { uch cc = (s->window[s->strstart]); s->d_buf[s->last_lit] = 0; s->l_buf[s->last_lit++] = cc; s->dyn_ltree[cc].fc.freq++; bflush = (s->last_lit == s->lit_bufsize-1); };
            s->lookahead--;
            s->strstart++;
        }
        if (bflush) { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (0)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (0) ? finish_started : need_more; };
    }
    s->insert = 0;
    if (flush == 4) {
        { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (1)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (1) ? finish_started : need_more; };
        return finish_done;
    }
    if (s->last_lit)
        { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (0)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (0) ? finish_started : need_more; };
    return block_done;
}





static block_state deflate_huff(s, flush)
    deflate_state *s;
    int flush;
{
    int bflush;

    for (;;) {

        if (s->lookahead == 0) {
            fill_window(s);
            if (s->lookahead == 0) {
                if (flush == 0)
                    return need_more;
                break;
            }
        }


        s->match_length = 0;
                                                      ;
        { uch cc = (s->window[s->strstart]); s->d_buf[s->last_lit] = 0; s->l_buf[s->last_lit++] = cc; s->dyn_ltree[cc].fc.freq++; bflush = (s->last_lit == s->lit_bufsize-1); };
        s->lookahead--;
        s->strstart++;
        if (bflush) { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (0)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (0) ? finish_started : need_more; };
    }
    s->insert = 0;
    if (flush == 4) {
        { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (1)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (1) ? finish_started : need_more; };
        return finish_done;
    }
    if (s->last_lit)
        { { _tr_flush_block(s, (s->block_start >= 0L ? (charf *)&s->window[(unsigned)s->block_start] : (charf *)0), (ulg)((long)s->strstart - s->block_start), (0)); s->block_start = s->strstart; flush_pending(s->strm); ; }; if (s->strm->avail_out == 0) return (0) ? finish_started : need_more; };
    return block_done;
}
