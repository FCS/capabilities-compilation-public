typedef long __intcap_t;
typedef long __uintcap_t;

typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;

typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int32_t __clock_t;
typedef double __double_t;
typedef float __float_t;






typedef __intcap_t __critical_t;
typedef __intcap_t __intfptr_t;
typedef __intcap_t __intptr_t;






typedef __int64_t __intmax_t;
typedef __int32_t __int_fast8_t;
typedef __int32_t __int_fast16_t;
typedef __int32_t __int_fast32_t;
typedef __int64_t __int_fast64_t;
typedef __int8_t __int_least8_t;
typedef __int16_t __int_least16_t;
typedef __int32_t __int_least32_t;
typedef __int64_t __int_least64_t;

typedef __int64_t __register_t;
typedef __int64_t f_register_t;





typedef __int64_t __ptrdiff_t;
typedef __int64_t __segsz_t;
typedef __uint64_t __size_t;
typedef __int64_t __ssize_t;




typedef __uintcap_t __uintfptr_t;
typedef __uintcap_t __uintptr_t;
typedef __int64_t __time_t;
typedef __uint64_t __uintmax_t;
typedef __uint32_t __uint_fast8_t;
typedef __uint32_t __uint_fast16_t;
typedef __uint32_t __uint_fast32_t;
typedef __uint64_t __uint_fast64_t;
typedef __uint8_t __uint_least8_t;
typedef __uint16_t __uint_least16_t;
typedef __uint32_t __uint_least32_t;
typedef __uint64_t __uint_least64_t;

typedef __uint64_t __u_register_t;




typedef __uint64_t __vm_offset_t;
typedef __uint64_t __vm_size_t;





typedef __uint64_t __vm_paddr_t;




typedef __int64_t __vm_ooffset_t;
typedef __uint64_t __vm_pindex_t;
typedef int ___wchar_t;
typedef __builtin_va_list __va_list;






typedef __va_list __gnuc_va_list;




typedef __int32_t __blksize_t;
typedef __int64_t __blkcnt_t;
typedef __int32_t __clockid_t;
typedef __uint32_t __fflags_t;
typedef __uint64_t __fsblkcnt_t;
typedef __uint64_t __fsfilcnt_t;
typedef __uint32_t __gid_t;
typedef __int64_t __id_t;
typedef __uint32_t __ino_t;
typedef long __key_t;
typedef __int32_t __lwpid_t;
typedef __uint16_t __mode_t;
typedef int __accmode_t;
typedef int __nl_item;
typedef __uint16_t __nlink_t;
typedef __int64_t __off_t;
typedef __int64_t __off64_t;
typedef __int32_t __pid_t;
typedef __int64_t __rlim_t;


typedef __uint8_t __sa_family_t;
typedef __uint32_t __socklen_t;
typedef long __suseconds_t;
typedef struct __timer *__timer_t;
typedef struct __mq *__mqd_t;
typedef __uint32_t __uid_t;
typedef unsigned int __useconds_t;
typedef int __cpuwhich_t;
typedef int __cpulevel_t;
typedef int __cpusetid_t;
typedef int __ct_rune_t;
typedef __ct_rune_t __rune_t;
typedef __ct_rune_t __wint_t;



typedef __uint_least16_t __char16_t;
typedef __uint_least32_t __char32_t;







typedef struct {
        long long __max_align1;
 long double __max_align2;
} __max_align_t;

typedef __uint32_t __dev_t;

typedef __uint32_t __fixpt_t;





typedef union {
 char __mbstate8[128];
 __int64_t _mbstateL;
} __mbstate_t;

typedef __uintmax_t __rman_res_t;


typedef __ptrdiff_t ptrdiff_t;





typedef __rune_t rune_t;





typedef __size_t size_t;





typedef ___wchar_t wchar_t;






typedef __max_align_t max_align_t;
 typedef size_t z_size_t;
typedef unsigned char Byte;

typedef unsigned int uInt;
typedef unsigned long uLong;





   typedef Byte Bytef;

typedef char charf;
typedef int intf;
typedef uInt uIntf;
typedef uLong uLongf;


   typedef void const *voidpc;
   typedef void *voidpf;
   typedef void *voidp;










   typedef unsigned z_crc_t;
static __inline __uint16_t
__bswap16_var(__uint16_t _x)
{

 return ((_x >> 8) | ((_x << 8) & 0xff00));
}

static __inline __uint32_t
__bswap32_var(__uint32_t _x)
{

 return ((_x >> 24) | ((_x >> 8) & 0xff00) | ((_x << 8) & 0xff0000) |
     ((_x << 24) & 0xff000000));
}

static __inline __uint64_t
__bswap64_var(__uint64_t _x)
{

 return ((_x >> 56) | ((_x >> 40) & 0xff00) | ((_x >> 24) & 0xff0000) |
     ((_x >> 8) & 0xff000000) | ((_x << 8) & ((__uint64_t)0xff << 32)) |
     ((_x << 24) & ((__uint64_t)0xff << 40)) |
     ((_x << 40) & ((__uint64_t)0xff << 48)) | ((_x << 56)));
}


struct pthread;
struct pthread_attr;
struct pthread_cond;
struct pthread_cond_attr;
struct pthread_mutex;
struct pthread_mutex_attr;
struct pthread_once;
struct pthread_rwlock;
struct pthread_rwlockattr;
struct pthread_barrier;
struct pthread_barrier_attr;
struct pthread_spinlock;
typedef struct pthread *pthread_t;


typedef struct pthread_attr *pthread_attr_t;
typedef struct pthread_mutex *pthread_mutex_t;
typedef struct pthread_mutex_attr *pthread_mutexattr_t;
typedef struct pthread_cond *pthread_cond_t;
typedef struct pthread_cond_attr *pthread_condattr_t;
typedef int pthread_key_t;
typedef struct pthread_once pthread_once_t;
typedef struct pthread_rwlock *pthread_rwlock_t;
typedef struct pthread_rwlockattr *pthread_rwlockattr_t;
typedef struct pthread_barrier *pthread_barrier_t;
typedef struct pthread_barrierattr *pthread_barrierattr_t;
typedef struct pthread_spinlock *pthread_spinlock_t;







typedef void *pthread_addr_t;
typedef void *(*pthread_startroutine_t)(void *);




struct pthread_once {
 int state;
 pthread_mutex_t mutex;
};


typedef unsigned char u_char;
typedef unsigned short u_short;
typedef unsigned int u_int;
typedef unsigned long u_long;

typedef unsigned short ushort;
typedef unsigned int uint;







typedef __int8_t int8_t;




typedef __int16_t int16_t;




typedef __int32_t int32_t;




typedef __int64_t int64_t;




typedef __uint8_t uint8_t;




typedef __uint16_t uint16_t;




typedef __uint32_t uint32_t;




typedef __uint64_t uint64_t;




typedef __intptr_t intptr_t;



typedef __uintptr_t uintptr_t;



typedef __intmax_t intmax_t;



typedef __uintmax_t uintmax_t;






typedef __uint64_t vaddr_t;

typedef __uint8_t u_int8_t;
typedef __uint16_t u_int16_t;
typedef __uint32_t u_int32_t;
typedef __uint64_t u_int64_t;

typedef __uint64_t u_quad_t;
typedef __int64_t quad_t;
typedef quad_t * qaddr_t;

typedef char * caddr_t;
typedef const char * c_caddr_t;


typedef __blksize_t blksize_t;



typedef __cpuwhich_t cpuwhich_t;
typedef __cpulevel_t cpulevel_t;
typedef __cpusetid_t cpusetid_t;


typedef __blkcnt_t blkcnt_t;




typedef __clock_t clock_t;




typedef __clockid_t clockid_t;



typedef __critical_t critical_t;
typedef __int64_t daddr_t;


typedef __dev_t dev_t;




typedef __fflags_t fflags_t;



typedef __fixpt_t fixpt_t;


typedef __fsblkcnt_t fsblkcnt_t;
typedef __fsfilcnt_t fsfilcnt_t;




typedef __gid_t gid_t;




typedef __uint32_t in_addr_t;




typedef __uint16_t in_port_t;




typedef __id_t id_t;




typedef __ino_t ino_t;




typedef __key_t key_t;




typedef __lwpid_t lwpid_t;




typedef __mode_t mode_t;




typedef __accmode_t accmode_t;




typedef __nlink_t nlink_t;




typedef __off_t off_t;




typedef __off64_t off64_t;




typedef __pid_t pid_t;



typedef __register_t register_t;


typedef __rlim_t rlim_t;



typedef __int64_t sbintime_t;

typedef __segsz_t segsz_t;







typedef __ssize_t ssize_t;




typedef __suseconds_t suseconds_t;




typedef __time_t time_t;




typedef __timer_t timer_t;




typedef __mqd_t mqd_t;



typedef __u_register_t u_register_t;


typedef __uid_t uid_t;




typedef __useconds_t useconds_t;





typedef unsigned long cap_ioctl_t;




struct cap_rights;

typedef struct cap_rights cap_rights_t;


typedef __vm_offset_t vm_offset_t;
typedef __vm_ooffset_t vm_ooffset_t;
typedef __vm_paddr_t vm_paddr_t;
typedef __vm_pindex_t vm_pindex_t;
typedef __vm_size_t vm_size_t;

typedef __rman_res_t rman_res_t;
static __inline __uint16_t
__bitcount16(__uint16_t _x)
{

 _x = (_x & 0x5555) + ((_x & 0xaaaa) >> 1);
 _x = (_x & 0x3333) + ((_x & 0xcccc) >> 2);
 _x = (_x + (_x >> 4)) & 0x0f0f;
 _x = (_x + (_x >> 8)) & 0x00ff;
 return (_x);
}

static __inline __uint32_t
__bitcount32(__uint32_t _x)
{

 _x = (_x & 0x55555555) + ((_x & 0xaaaaaaaa) >> 1);
 _x = (_x & 0x33333333) + ((_x & 0xcccccccc) >> 2);
 _x = (_x + (_x >> 4)) & 0x0f0f0f0f;
 _x = (_x + (_x >> 8));
 _x = (_x + (_x >> 16)) & 0x000000ff;
 return (_x);
}


static __inline __uint64_t
__bitcount64(__uint64_t _x)
{

 _x = (_x & 0x5555555555555555) + ((_x & 0xaaaaaaaaaaaaaaaa) >> 1);
 _x = (_x & 0x3333333333333333) + ((_x & 0xcccccccccccccccc) >> 2);
 _x = (_x + (_x >> 4)) & 0x0f0f0f0f0f0f0f0f;
 _x = (_x + (_x >> 8));
 _x = (_x + (_x >> 16));
 _x = (_x + (_x >> 32)) & 0x000000ff;
 return (_x);
}
typedef struct __sigset {
 __uint32_t __bits[4];
} __sigset_t;
struct timeval {
 time_t tv_sec;
 suseconds_t tv_usec;
};
struct timespec {
 time_t tv_sec;
 long tv_nsec;
};
struct itimerspec {
 struct timespec it_interval;
 struct timespec it_value;
};

typedef unsigned long __fd_mask;

typedef __fd_mask fd_mask;




typedef __sigset_t sigset_t;
typedef struct fd_set {
 __fd_mask __fds_bits[(((1024) + (((sizeof(__fd_mask) * 8)) - 1)) / ((sizeof(__fd_mask) * 8)))];
} fd_set;
int pselect(int, fd_set *restrict, fd_set *restrict, fd_set *restrict,
 const struct timespec *restrict, const sigset_t *restrict);



int select(int, fd_set *, fd_set *, fd_set *, struct timeval *);
int ftruncate(int, off_t);



off_t lseek(int, off_t, int);



void * mmap(void *, size_t, int, int, int, off_t);



int truncate(const char *, off_t);





typedef __va_list va_list;
void _exit(int) __attribute__((__noreturn__));
int access(const char *, int);
unsigned int alarm(unsigned int);
int chdir(const char *);
int chown(const char *, uid_t, gid_t);
int close(int);
void closefrom(int);
int dup(int);
int dup2(int, int);
int execl(const char *, const char *, ...) __attribute__((__sentinel__));
int execle(const char *, const char *, ...);
int execlp(const char *, const char *, ...) __attribute__((__sentinel__));
int execv(const char *, char * const *);
int execve(const char *, char * const *, char * const *);
int execvp(const char *, char * const *);
pid_t fork(void);
long fpathconf(int, int);
char *getcwd(char *, size_t);
gid_t getegid(void);
uid_t geteuid(void);
gid_t getgid(void);
int getgroups(int, gid_t []);
char *getlogin(void);
pid_t getpgrp(void);
pid_t getpid(void);
pid_t getppid(void);
uid_t getuid(void);
int isatty(int);
int link(const char *, const char *);




long pathconf(const char *, int);
int pause(void);
int pipe(int *);
ssize_t read(int, void *, size_t);
int rmdir(const char *);
int setgid(gid_t);
int setpgid(pid_t, pid_t);
pid_t setsid(void);
int setuid(uid_t);
unsigned int sleep(unsigned int);
long sysconf(int);
pid_t tcgetpgrp(int);
int tcsetpgrp(int, pid_t);
char *ttyname(int);
int ttyname_r(int, char *, size_t);
int unlink(const char *);
ssize_t write(int, const void *, size_t);



size_t confstr(int, char *, size_t);


int getopt(int, char * const [], const char *);

extern char *optarg;
extern int optind, opterr, optopt;





int fsync(int);
int fdatasync(int);
int getlogin_r(char *, int);




int fchown(int, uid_t, gid_t);
ssize_t readlink(const char * restrict, char * restrict, size_t);


int gethostname(char *, size_t);
int setegid(gid_t);
int seteuid(uid_t);




int getsid(pid_t _pid);
int fchdir(int);
int getpgid(pid_t _pid);
int lchown(const char *, uid_t, gid_t);
ssize_t pread(int, void *, size_t, off_t);
ssize_t pwrite(int, const void *, size_t, off_t);
int faccessat(int, const char *, int, int);
int fchownat(int, const char *, uid_t, gid_t, int);
int fexecve(int, char *const [], char *const []);
int linkat(int, const char *, int, const char *, int);
ssize_t readlinkat(int, const char * restrict, char * restrict, size_t);
int symlinkat(const char *, int, const char *);
int unlinkat(int, const char *, int);
int symlink(const char * restrict, const char * restrict);




char *crypt(const char *, const char *);
long gethostid(void);
int lockf(int, int, off_t);
int nice(int);
int setregid(gid_t, gid_t);
int setreuid(uid_t, uid_t);



void swab(const void * restrict, void * restrict, ssize_t);


void sync(void);




int brk(const void *);
int chroot(const char *);
int getdtablesize(void);
int getpagesize(void) __attribute__((__const__));
char *getpass(const char *);
void *sbrk(intptr_t);



char *getwd(char *);
useconds_t
  ualarm(useconds_t, useconds_t);
int usleep(useconds_t);
pid_t vfork(void) __attribute__((__returns_twice__));



struct timeval;

struct crypt_data {
 int initialized;
 char __buf[256];
};

int acct(const char *);
int async_daemon(void);
int check_utility_compat(const char *);
const char *
  crypt_get_format(void);
char *crypt_r(const char *, const char *, struct crypt_data *);
int crypt_set_format(const char *);
int dup3(int, int, int);
int eaccess(const char *, int);
void endusershell(void);
int exect(const char *, char * const *, char * const *);
int execvP(const char *, const char *, char * const *);
int feature_present(const char *);
char *fflagstostr(u_long);
int getdomainname(char *, int);
int getgrouplist(const char *, gid_t, gid_t *, int *);
int getloginclass(char *, size_t);
mode_t getmode(const void *, mode_t);
int getosreldate(void);
int getpeereid(int, uid_t *, gid_t *);
int getresgid(gid_t *, gid_t *, gid_t *);
int getresuid(uid_t *, uid_t *, uid_t *);
char *getusershell(void);
int initgroups(const char *, gid_t);
int iruserok(unsigned long, int, const char *, const char *);
int iruserok_sa(const void *, int, int, const char *, const char *);
int issetugid(void);
void __FreeBSD_libc_enter_restricted_mode(void);
long lpathconf(const char *, int);

char *mkdtemp(char *);



int mknod(const char *, mode_t, dev_t);



int mkstemp(char *);


int mkstemps(char *, int);

char *mktemp(char *);


int nfssvc(int, void *);
int nlm_syscall(int, int, int, char **);
int pipe2(int *, int);
int profil(char *, size_t, vm_offset_t, int);
int rcmd(char **, int, const char *, const char *, const char *, int *);
int rcmd_af(char **, int, const char *,
  const char *, const char *, int *, int);
int rcmdsh(char **, int, const char *,
  const char *, const char *, const char *);
char *re_comp(const char *);
int re_exec(const char *);
int reboot(int);
int revoke(const char *);
pid_t rfork(int);
pid_t rfork_thread(int, void *, int (*)(void *), void *);
int rresvport(int *);
int rresvport_af(int *, int);
int ruserok(const char *, int, const char *, const char *);






int setdomainname(const char *, int);
int setgroups(int, const gid_t *);
void sethostid(long);
int sethostname(const char *, int);
int setlogin(const char *);
int setloginclass(const char *);
void *setmode(const char *);
int setpgrp(pid_t, pid_t);
void setproctitle(const char *_fmt, ...) __attribute__((__format__ (__printf0__, 1, 2)));
int setresgid(gid_t, gid_t, gid_t);
int setresuid(uid_t, uid_t, uid_t);
int setrgid(gid_t);
int setruid(uid_t);
void setusershell(void);
int strtofflags(char **, u_long *, u_long *);
int swapon(const char *);
int swapoff(const char *);
int syscall(int, ...);
off_t __syscall(quad_t, ...);
int undelete(const char *);
int unwhiteout(const char *);
void *valloc(size_t);



extern int optreset;



ssize_t write_c(int, __attribute__((address_space(200))) const void *, size_t);
typedef voidpf (*alloc_func) (voidpf opaque, uInt items, uInt size);
typedef void (*free_func) (voidpf opaque, voidpf address);

struct internal_state;

typedef struct z_stream_s {
            Bytef *next_in;
    uInt avail_in;
    uLong total_in;

    Bytef *next_out;
    uInt avail_out;
    uLong total_out;

            char *msg;
    struct internal_state *state;

    alloc_func zalloc;
    free_func zfree;
    voidpf opaque;

    int data_type;

    uLong adler;
    uLong reserved;
} z_stream;

typedef z_stream *z_streamp;





typedef struct gz_header_s {
    int text;
    uLong time;
    int xflags;
    int os;
    Bytef *extra;
    uInt extra_len;
    uInt extra_max;
    Bytef *name;
    uInt name_max;
    Bytef *comment;
    uInt comm_max;
    int hcrc;
    int done;

} gz_header;

typedef gz_header *gz_headerp;
typedef unsigned (*in_func) (void *, unsigned char * *);

typedef int (*out_func) (void *, unsigned char *, unsigned);
typedef struct gzFile_s *gzFile;

struct gzFile_s {
    unsigned have;
    unsigned char *next;
    off_t pos;
};





int bcmp(const void *, const void *, size_t) __attribute__((__pure__));
void bcopy(const void *, void *, size_t);
void bzero(void *, size_t);


void explicit_bzero(void *, size_t);


int ffs(int) __attribute__((__const__));


int ffsl(long) __attribute__((__const__));
int ffsll(long long) __attribute__((__const__));
int fls(int) __attribute__((__const__));
int flsl(long) __attribute__((__const__));
int flsll(long long) __attribute__((__const__));


char *index(const char *, int) __attribute__((__pure__));
char *rindex(const char *, int) __attribute__((__pure__));

int strcasecmp(const char *, const char *) __attribute__((__pure__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__pure__));



typedef struct _xlocale *locale_t;
int strcasecmp_l(const char *, const char *, locale_t);
int strncasecmp_l(const char *, const char *, size_t, locale_t);
void *memccpy(void * restrict, const void * restrict, int, size_t);

void *memchr(const void *, int, size_t) __attribute__((__pure__));

void *memrchr(const void *, int, size_t) __attribute__((__pure__));

int memcmp(const void *, const void *, size_t) __attribute__((__pure__));
void *memcpy(void * restrict, const void * restrict, size_t);

void *memmem(const void *, size_t, const void *, size_t) __attribute__((__pure__));

void *memmove(void *, const void *, size_t);
void *memset(void *, int, size_t);

char *stpcpy(char * restrict, const char * restrict);
char *stpncpy(char * restrict, const char * restrict, size_t);


char *strcasestr(const char *, const char *) __attribute__((__pure__));

char *strcat(char * restrict, const char * restrict);
char *strchr(const char *, int) __attribute__((__pure__));

char *strchrnul(const char*, int) __attribute__((__pure__));

int strcmp(const char *, const char *) __attribute__((__pure__));
int strcoll(const char *, const char *);
char *strcpy(char * restrict, const char * restrict);
size_t strcspn(const char *, const char *) __attribute__((__pure__));

char *strdup(const char *) __attribute__((__malloc__));

char *strerror(int);

int strerror_r(int, char *, size_t);


size_t strlcat(char * restrict, const char * restrict, size_t);
size_t strlcpy(char * restrict, const char * restrict, size_t);

size_t strlen(const char *) __attribute__((__pure__));

void strmode(int, char *);

char *strncat(char * restrict, const char * restrict, size_t);
int strncmp(const char *, const char *, size_t) __attribute__((__pure__));
char *strncpy(char * restrict, const char * restrict, size_t);

char *strndup(const char *, size_t) __attribute__((__malloc__));
size_t strnlen(const char *, size_t) __attribute__((__pure__));


char *strnstr(const char *, const char *, size_t) __attribute__((__pure__));

char *strpbrk(const char *, const char *) __attribute__((__pure__));
char *strrchr(const char *, int) __attribute__((__pure__));

char *strsep(char **, const char *);


char *strsignal(int);

size_t strspn(const char *, const char *) __attribute__((__pure__));
char *strstr(const char *, const char *) __attribute__((__pure__));
char *strtok(char * restrict, const char * restrict);

char *strtok_r(char *, const char *, char **);

size_t strxfrm(char * restrict, const char * restrict, size_t);
__attribute__((address_space(200))) const void *
  memchr_c_const(__attribute__((address_space(200))) const void *, int, size_t) __attribute__((__pure__));
__attribute__((address_space(200))) void
 *memchr_c(__attribute__((address_space(200))) const void *, int, size_t) __attribute__((__pure__));
int memcmp_c(__attribute__((address_space(200))) const void *, __attribute__((address_space(200))) const void *, size_t)
     __attribute__((__pure__));
__attribute__((address_space(200))) void
 *memcpy_c(__attribute__((address_space(200))) void * restrict,
     __attribute__((address_space(200))) const void * restrict, size_t);
void *memcpy_c_fromcap(void * restrict,
     __attribute__((address_space(200))) const void * restrict, size_t);
__attribute__((address_space(200))) void
 *memcpy_c_tocap(__attribute__((address_space(200))) void * restrict,
     const void * restrict, size_t);
__attribute__((address_space(200))) void
 *memmove_c(__attribute__((address_space(200))) void * restrict,
     __attribute__((address_space(200))) const void * restrict, size_t);
__attribute__((address_space(200))) void
 *memset_c(__attribute__((address_space(200))) void *, int, size_t);

__attribute__((address_space(200))) char
 *strchr_c(__attribute__((address_space(200))) const char *, int) __attribute__((__pure__));
int strcmp_c(__attribute__((address_space(200))) const char *,
     __attribute__((address_space(200))) const char *s2) __attribute__((__pure__));
__attribute__((address_space(200))) char
 *strcpy_c(__attribute__((address_space(200))) char * restrict,
     __attribute__((address_space(200))) const char * restrict);
int strncmp_c(__attribute__((address_space(200))) const char *, __attribute__((address_space(200))) const char *,
     size_t) __attribute__((__pure__));
__attribute__((address_space(200))) char
*strncpy_c(__attribute__((address_space(200))) char * restrict, __attribute__((address_space(200))) const char * restrict,
     size_t);
char *strncpy_c_fromcap(char * restrict,
     __attribute__((address_space(200))) const char * restrict, size_t);
__attribute__((address_space(200))) char
 *strncpy_c_tocap(__attribute__((address_space(200))) char * restrict,
     const char * restrict, size_t);
size_t strnlen_c(__attribute__((address_space(200))) const char *, size_t) __attribute__((__pure__));


int timingsafe_bcmp(const void *, const void *, size_t);
int timingsafe_memcmp(const void *, const void *, size_t);




int strcoll_l(const char *, const char *, locale_t);
size_t strxfrm_l(char *, const char *, size_t, locale_t);
typedef struct {
 int quot;
 int rem;
} div_t;

typedef struct {
 long quot;
 long rem;
} ldiv_t;
extern int __mb_cur_max;
extern int ___mb_cur_max(void);


 void abort(void);
int abs(int) __attribute__((__const__));
int atexit(void (*)(void));
double atof(const char *);
int atoi(const char *);
long atol(const char *);
void *bsearch(const void *, const void *, size_t,
     size_t, int (*)(const void *, const void *));
void *calloc(size_t, size_t) __attribute__((__malloc__)) __attribute__((__warn_unused_result__))
                                     ;
div_t div(int, int) __attribute__((__const__));
void exit(int);
void free(void *);
char *getenv(const char *);
long labs(long) __attribute__((__const__));
ldiv_t ldiv(long, long) __attribute__((__const__));
void *malloc(size_t) __attribute__((__malloc__)) __attribute__((__warn_unused_result__)) ;
int mblen(const char *, size_t);
size_t mbstowcs(wchar_t * restrict , const char * restrict, size_t);
int mbtowc(wchar_t * restrict, const char * restrict, size_t);
void qsort(void *, size_t, size_t,
     int (*)(const void *, const void *));
int rand(void);
void *realloc(void *, size_t) __attribute__((__warn_unused_result__)) ;
void srand(unsigned);
double strtod(const char * restrict, char ** restrict);
float strtof(const char * restrict, char ** restrict);
long strtol(const char * restrict, char ** restrict, int);
long double
  strtold(const char * restrict, char ** restrict);
unsigned long
  strtoul(const char * restrict, char ** restrict, int);
int system(const char *);
int wctomb(char *, wchar_t);
size_t wcstombs(char * restrict, const wchar_t * restrict, size_t);
typedef struct {
 long long quot;
 long long rem;
} lldiv_t;


long long
  atoll(const char *);

long long
  llabs(long long) __attribute__((__const__));

lldiv_t lldiv(long long, long long) __attribute__((__const__));

long long
  strtoll(const char * restrict, char ** restrict, int);

unsigned long long
  strtoull(const char * restrict, char ** restrict, int);


 void _Exit(int);






void * aligned_alloc(size_t, size_t) __attribute__((__malloc__))
                    ;
int at_quick_exit(void (*)(void));
void quick_exit(int);





char *realpath(const char * restrict, char * restrict);


int rand_r(unsigned *);


int posix_memalign(void **, size_t, size_t) __attribute__((__nonnull__(1)));
int setenv(const char *, const char *, int);
int unsetenv(const char *);



int getsubopt(char **, char *const *, char **);
long a64l(const char *);
double drand48(void);

double erand48(unsigned short[3]);


int grantpt(int);
char *initstate(unsigned int, char *, size_t);
long jrand48(unsigned short[3]);
char *l64a(long);
void lcong48(unsigned short[7]);
long lrand48(void);




long mrand48(void);
long nrand48(unsigned short[3]);
int posix_openpt(int);
char *ptsname(int);
int putenv(char *);
long random(void);
unsigned short
 *seed48(unsigned short[3]);
char *setstate( char *);
void srand48(long);
void srandom(unsigned int);
int unlockpt(int);



extern const char *malloc_conf;
extern void (*malloc_message)(void *, const char *);
void abort2(const char *, int, void **) __attribute__((__noreturn__));
__uint32_t
  arc4random(void);
void arc4random_addrandom(unsigned char *, int);
void arc4random_buf(void *, size_t);
void arc4random_stir(void);
__uint32_t
  arc4random_uniform(__uint32_t);





char *getbsize(int *, long *);

char *cgetcap(char *, const char *, int);
int cgetclose(void);
int cgetent(char **, char **, const char *);
int cgetfirst(char **, char **);
int cgetmatch(const char *, const char *);
int cgetnext(char **, char **);
int cgetnum(char *, const char *, long *);
int cgetset(const char *);
int cgetstr(char *, const char *, char **);
int cgetustr(char *, const char *, char **);

int daemon(int, int);
char *devname(__dev_t, __mode_t);
char *devname_r(__dev_t, __mode_t, char *, int);
char *fdevname(int);
char *fdevname_r(int, char *, int);
int getloadavg(double [], int);
const char *
  getprogname(void);

int heapsort(void *, size_t, size_t, int (*)(const void *, const void *));





int l64a_r(long, char *, int);
int mergesort(void *, size_t, size_t, int (*)(const void *, const void *));



int mkostemp(char *, int);
int mkostemps(char *, int, int);
void qsort_r(void *, size_t, size_t, void *,
     int (*)(void *, const void *, const void *));
int radixsort(const unsigned char **, int, const unsigned char *,
     unsigned);
void *reallocarray(void *, size_t, size_t) __attribute__((__warn_unused_result__))
                    ;
void *reallocf(void *, size_t) ;
int rpmatch(const char *);
void setprogname(const char *);
int sradixsort(const unsigned char **, int, const unsigned char *,
     unsigned);
void sranddev(void);
void srandomdev(void);
long long
 strtonum(const char *, long long, long long, const char **);


__int64_t
  strtoq(const char *, char **, int);
__uint64_t
  strtouq(const char *, char **, int);


long strtol_c(__attribute__((address_space(200))) const char * restrict,
     __attribute__((address_space(200))) char ** restrict, int);
long double
  strtold_c(__attribute__((address_space(200))) const char * restrict,
     __attribute__((address_space(200))) char ** restrict);


int timsort(void *base, size_t nel, size_t width,
     int (*compar) (const void *, const void *));

extern char *suboptarg;
typedef unsigned char uch;
typedef uch uchf;
typedef unsigned short ush;
typedef ush ushf;
typedef unsigned long ulg;

static char * const z_errmsg[10] = {
        ( char *)"need dictionary",
        ( char *)"stream end",
        ( char *)"",
        ( char *)"file error",
        ( char *)"stream error",
        ( char *)"data error",
        ( char *)"insufficient memory",
        ( char *)"buffer error",
        ( char *)"incompatible version",
        ( char *)""
};
    extern uLong adler32_combine64 (uLong, uLong, off_t);
    extern uLong crc32_combine64 (uLong, uLong, off_t);
   voidpf zcalloc (voidpf opaque, unsigned items, unsigned size);

   void zcfree (voidpf opaque, voidpf ptr);
typedef struct {
    unsigned char op;
    unsigned char bits;
    unsigned short val;
} code;
typedef enum {
    CODES,
    LENS,
    DISTS
} codetype;

int inflate_table (codetype type, unsigned short *lens, unsigned codes, code * *table, unsigned *bits, unsigned short *work);
typedef enum {
    HEAD = 16180,
    FLAGS,
    TIME,
    OS,
    EXLEN,
    EXTRA,
    NAME,
    COMMENT,
    HCRC,
    DICTID,
    DICT,
        TYPE,
        TYPEDO,
        STORED,
        COPY_,
        COPY,
        TABLE,
        LENLENS,
        CODELENS,
            LEN_,
            LEN,
            LENEXT,
            DIST,
            DISTEXT,
            MATCH,
            LIT,
    CHECK,
    LENGTH,
    DONE,
    BAD,
    MEM,
    SYNC
} inflate_mode;
struct inflate_state {
    z_streamp strm;
    inflate_mode mode;
    int last;
    int wrap;

    int havedict;
    int flags;
    unsigned dmax;
    unsigned long check;
    unsigned long total;
    gz_headerp head;

    unsigned wbits;
    unsigned wsize;
    unsigned whave;
    unsigned wnext;
    unsigned char *window;

    unsigned long hold;
    unsigned bits;

    unsigned length;
    unsigned offset;

    unsigned extra;

    code const *lencode;
    code const *distcode;
    unsigned lenbits;
    unsigned distbits;

    unsigned ncode;
    unsigned nlen;
    unsigned ndist;
    unsigned have;
    code *next;
    unsigned short lens[320];
    unsigned short work[288];
    code codes[(852 +592)];
    int sane;
    int back;
    unsigned was;
};
void inflate_fast (z_streamp strm, unsigned start);








static int inflateStateCheck (z_streamp strm);
static void fixedtables (struct inflate_state *state);
static int updatewindow (z_streamp strm, const unsigned char *end, unsigned copy);




static unsigned syncsearch (unsigned *have, const unsigned char *buf, unsigned len);


static int inflateStateCheck(strm)
z_streamp strm;
{
    struct inflate_state *state;
    if (strm == 0 ||
        strm->zalloc == (alloc_func)0 || strm->zfree == (free_func)0)
        return 1;
    state = (struct inflate_state *)strm->state;
    if (state == 0 || state->strm != strm ||
        state->mode < HEAD || state->mode > SYNC)
        return 1;
    return 0;
}

int inflateResetKeep(strm)
z_streamp strm;
{
    struct inflate_state *state;

    if (inflateStateCheck(strm)) return (-2);
    state = (struct inflate_state *)strm->state;
    strm->total_in = strm->total_out = state->total = 0;
    strm->msg = 0;
    if (state->wrap)
        strm->adler = state->wrap & 1;
    state->mode = HEAD;
    state->last = 0;
    state->havedict = 0;
    state->dmax = 32768U;
    state->head = 0;
    state->hold = 0;
    state->bits = 0;
    state->lencode = state->distcode = state->next = state->codes;
    state->sane = 1;
    state->back = -1;
                                        ;
    return 0;
}

int inflateReset(strm)
z_streamp strm;
{
    struct inflate_state *state;

    if (inflateStateCheck(strm)) return (-2);
    state = (struct inflate_state *)strm->state;
    state->wsize = 0;
    state->whave = 0;
    state->wnext = 0;
    return inflateResetKeep(strm);
}

int inflateReset2(strm, windowBits)
z_streamp strm;
int windowBits;
{
    int wrap;
    struct inflate_state *state;


    if (inflateStateCheck(strm)) return (-2);
    state = (struct inflate_state *)strm->state;


    if (windowBits < 0) {
        wrap = 0;
        windowBits = -windowBits;
    }
    else {
        wrap = (windowBits >> 4) + 5;

        if (windowBits < 48)
            windowBits &= 15;

    }


    if (windowBits && (windowBits < 8 || windowBits > 15))
        return (-2);
    if (state->window != 0 && state->wbits != (unsigned)windowBits) {
        (*((strm)->zfree))((strm)->opaque, (voidpf)(state->window));
        state->window = 0;
    }


    state->wrap = wrap;
    state->wbits = (unsigned)windowBits;
    return inflateReset(strm);
}

int inflateInit2_(strm, windowBits, version, stream_size)
z_streamp strm;
int windowBits;
const char *version;
int stream_size;
{
    int ret;
    struct inflate_state *state;

    if (version == 0 || version[0] != "1.2.11"[0] ||
        stream_size != (int)(sizeof(z_stream)))
        return (-6);
    if (strm == 0) return (-2);
    strm->msg = 0;
    if (strm->zalloc == (alloc_func)0) {



        strm->zalloc = zcalloc;
        strm->opaque = (voidpf)0;

    }
    if (strm->zfree == (free_func)0)



        strm->zfree = zcfree;

    state = (struct inflate_state *)
            (*((strm)->zalloc))((strm)->opaque, (1), (sizeof(struct inflate_state)));
    if (state == 0) return (-4);
                                            ;
    strm->state = (struct internal_state *)state;
    state->strm = strm;
    state->window = 0;
    state->mode = HEAD;
    ret = inflateReset2(strm, windowBits);
    if (ret != 0) {
        (*((strm)->zfree))((strm)->opaque, (voidpf)(state));
        strm->state = 0;
    }
    return ret;
}

int inflateInit_(strm, version, stream_size)
z_streamp strm;
const char *version;
int stream_size;
{
    return inflateInit2_(strm, 15, version, stream_size);
}

int inflatePrime(strm, bits, value)
z_streamp strm;
int bits;
int value;
{
    struct inflate_state *state;

    if (inflateStateCheck(strm)) return (-2);
    state = (struct inflate_state *)strm->state;
    if (bits < 0) {
        state->hold = 0;
        state->bits = 0;
        return 0;
    }
    if (bits > 16 || state->bits + (uInt)bits > 32) return (-2);
    value &= (1L << bits) - 1;
    state->hold += (unsigned)value << state->bits;
    state->bits += (uInt)bits;
    return 0;
}
static void fixedtables(state)
struct inflate_state *state;
{
    static const code lenfix[512] = {
        {96,7,0},{0,8,80},{0,8,16},{20,8,115},{18,7,31},{0,8,112},{0,8,48},
        {0,9,192},{16,7,10},{0,8,96},{0,8,32},{0,9,160},{0,8,0},{0,8,128},
        {0,8,64},{0,9,224},{16,7,6},{0,8,88},{0,8,24},{0,9,144},{19,7,59},
        {0,8,120},{0,8,56},{0,9,208},{17,7,17},{0,8,104},{0,8,40},{0,9,176},
        {0,8,8},{0,8,136},{0,8,72},{0,9,240},{16,7,4},{0,8,84},{0,8,20},
        {21,8,227},{19,7,43},{0,8,116},{0,8,52},{0,9,200},{17,7,13},{0,8,100},
        {0,8,36},{0,9,168},{0,8,4},{0,8,132},{0,8,68},{0,9,232},{16,7,8},
        {0,8,92},{0,8,28},{0,9,152},{20,7,83},{0,8,124},{0,8,60},{0,9,216},
        {18,7,23},{0,8,108},{0,8,44},{0,9,184},{0,8,12},{0,8,140},{0,8,76},
        {0,9,248},{16,7,3},{0,8,82},{0,8,18},{21,8,163},{19,7,35},{0,8,114},
        {0,8,50},{0,9,196},{17,7,11},{0,8,98},{0,8,34},{0,9,164},{0,8,2},
        {0,8,130},{0,8,66},{0,9,228},{16,7,7},{0,8,90},{0,8,26},{0,9,148},
        {20,7,67},{0,8,122},{0,8,58},{0,9,212},{18,7,19},{0,8,106},{0,8,42},
        {0,9,180},{0,8,10},{0,8,138},{0,8,74},{0,9,244},{16,7,5},{0,8,86},
        {0,8,22},{64,8,0},{19,7,51},{0,8,118},{0,8,54},{0,9,204},{17,7,15},
        {0,8,102},{0,8,38},{0,9,172},{0,8,6},{0,8,134},{0,8,70},{0,9,236},
        {16,7,9},{0,8,94},{0,8,30},{0,9,156},{20,7,99},{0,8,126},{0,8,62},
        {0,9,220},{18,7,27},{0,8,110},{0,8,46},{0,9,188},{0,8,14},{0,8,142},
        {0,8,78},{0,9,252},{96,7,0},{0,8,81},{0,8,17},{21,8,131},{18,7,31},
        {0,8,113},{0,8,49},{0,9,194},{16,7,10},{0,8,97},{0,8,33},{0,9,162},
        {0,8,1},{0,8,129},{0,8,65},{0,9,226},{16,7,6},{0,8,89},{0,8,25},
        {0,9,146},{19,7,59},{0,8,121},{0,8,57},{0,9,210},{17,7,17},{0,8,105},
        {0,8,41},{0,9,178},{0,8,9},{0,8,137},{0,8,73},{0,9,242},{16,7,4},
        {0,8,85},{0,8,21},{16,8,258},{19,7,43},{0,8,117},{0,8,53},{0,9,202},
        {17,7,13},{0,8,101},{0,8,37},{0,9,170},{0,8,5},{0,8,133},{0,8,69},
        {0,9,234},{16,7,8},{0,8,93},{0,8,29},{0,9,154},{20,7,83},{0,8,125},
        {0,8,61},{0,9,218},{18,7,23},{0,8,109},{0,8,45},{0,9,186},{0,8,13},
        {0,8,141},{0,8,77},{0,9,250},{16,7,3},{0,8,83},{0,8,19},{21,8,195},
        {19,7,35},{0,8,115},{0,8,51},{0,9,198},{17,7,11},{0,8,99},{0,8,35},
        {0,9,166},{0,8,3},{0,8,131},{0,8,67},{0,9,230},{16,7,7},{0,8,91},
        {0,8,27},{0,9,150},{20,7,67},{0,8,123},{0,8,59},{0,9,214},{18,7,19},
        {0,8,107},{0,8,43},{0,9,182},{0,8,11},{0,8,139},{0,8,75},{0,9,246},
        {16,7,5},{0,8,87},{0,8,23},{64,8,0},{19,7,51},{0,8,119},{0,8,55},
        {0,9,206},{17,7,15},{0,8,103},{0,8,39},{0,9,174},{0,8,7},{0,8,135},
        {0,8,71},{0,9,238},{16,7,9},{0,8,95},{0,8,31},{0,9,158},{20,7,99},
        {0,8,127},{0,8,63},{0,9,222},{18,7,27},{0,8,111},{0,8,47},{0,9,190},
        {0,8,15},{0,8,143},{0,8,79},{0,9,254},{96,7,0},{0,8,80},{0,8,16},
        {20,8,115},{18,7,31},{0,8,112},{0,8,48},{0,9,193},{16,7,10},{0,8,96},
        {0,8,32},{0,9,161},{0,8,0},{0,8,128},{0,8,64},{0,9,225},{16,7,6},
        {0,8,88},{0,8,24},{0,9,145},{19,7,59},{0,8,120},{0,8,56},{0,9,209},
        {17,7,17},{0,8,104},{0,8,40},{0,9,177},{0,8,8},{0,8,136},{0,8,72},
        {0,9,241},{16,7,4},{0,8,84},{0,8,20},{21,8,227},{19,7,43},{0,8,116},
        {0,8,52},{0,9,201},{17,7,13},{0,8,100},{0,8,36},{0,9,169},{0,8,4},
        {0,8,132},{0,8,68},{0,9,233},{16,7,8},{0,8,92},{0,8,28},{0,9,153},
        {20,7,83},{0,8,124},{0,8,60},{0,9,217},{18,7,23},{0,8,108},{0,8,44},
        {0,9,185},{0,8,12},{0,8,140},{0,8,76},{0,9,249},{16,7,3},{0,8,82},
        {0,8,18},{21,8,163},{19,7,35},{0,8,114},{0,8,50},{0,9,197},{17,7,11},
        {0,8,98},{0,8,34},{0,9,165},{0,8,2},{0,8,130},{0,8,66},{0,9,229},
        {16,7,7},{0,8,90},{0,8,26},{0,9,149},{20,7,67},{0,8,122},{0,8,58},
        {0,9,213},{18,7,19},{0,8,106},{0,8,42},{0,9,181},{0,8,10},{0,8,138},
        {0,8,74},{0,9,245},{16,7,5},{0,8,86},{0,8,22},{64,8,0},{19,7,51},
        {0,8,118},{0,8,54},{0,9,205},{17,7,15},{0,8,102},{0,8,38},{0,9,173},
        {0,8,6},{0,8,134},{0,8,70},{0,9,237},{16,7,9},{0,8,94},{0,8,30},
        {0,9,157},{20,7,99},{0,8,126},{0,8,62},{0,9,221},{18,7,27},{0,8,110},
        {0,8,46},{0,9,189},{0,8,14},{0,8,142},{0,8,78},{0,9,253},{96,7,0},
        {0,8,81},{0,8,17},{21,8,131},{18,7,31},{0,8,113},{0,8,49},{0,9,195},
        {16,7,10},{0,8,97},{0,8,33},{0,9,163},{0,8,1},{0,8,129},{0,8,65},
        {0,9,227},{16,7,6},{0,8,89},{0,8,25},{0,9,147},{19,7,59},{0,8,121},
        {0,8,57},{0,9,211},{17,7,17},{0,8,105},{0,8,41},{0,9,179},{0,8,9},
        {0,8,137},{0,8,73},{0,9,243},{16,7,4},{0,8,85},{0,8,21},{16,8,258},
        {19,7,43},{0,8,117},{0,8,53},{0,9,203},{17,7,13},{0,8,101},{0,8,37},
        {0,9,171},{0,8,5},{0,8,133},{0,8,69},{0,9,235},{16,7,8},{0,8,93},
        {0,8,29},{0,9,155},{20,7,83},{0,8,125},{0,8,61},{0,9,219},{18,7,23},
        {0,8,109},{0,8,45},{0,9,187},{0,8,13},{0,8,141},{0,8,77},{0,9,251},
        {16,7,3},{0,8,83},{0,8,19},{21,8,195},{19,7,35},{0,8,115},{0,8,51},
        {0,9,199},{17,7,11},{0,8,99},{0,8,35},{0,9,167},{0,8,3},{0,8,131},
        {0,8,67},{0,9,231},{16,7,7},{0,8,91},{0,8,27},{0,9,151},{20,7,67},
        {0,8,123},{0,8,59},{0,9,215},{18,7,19},{0,8,107},{0,8,43},{0,9,183},
        {0,8,11},{0,8,139},{0,8,75},{0,9,247},{16,7,5},{0,8,87},{0,8,23},
        {64,8,0},{19,7,51},{0,8,119},{0,8,55},{0,9,207},{17,7,15},{0,8,103},
        {0,8,39},{0,9,175},{0,8,7},{0,8,135},{0,8,71},{0,9,239},{16,7,9},
        {0,8,95},{0,8,31},{0,9,159},{20,7,99},{0,8,127},{0,8,63},{0,9,223},
        {18,7,27},{0,8,111},{0,8,47},{0,9,191},{0,8,15},{0,8,143},{0,8,79},
        {0,9,255}
    };

    static const code distfix[32] = {
        {16,5,1},{23,5,257},{19,5,17},{27,5,4097},{17,5,5},{25,5,1025},
        {21,5,65},{29,5,16385},{16,5,3},{24,5,513},{20,5,33},{28,5,8193},
        {18,5,9},{26,5,2049},{22,5,129},{64,5,0},{16,5,2},{23,5,385},
        {19,5,25},{27,5,6145},{17,5,7},{25,5,1537},{21,5,97},{29,5,24577},
        {16,5,4},{24,5,769},{20,5,49},{28,5,12289},{18,5,13},{26,5,3073},
        {22,5,193},{64,5,0}
    };

    state->lencode = lenfix;
    state->lenbits = 9;
    state->distcode = distfix;
    state->distbits = 5;
}
static int updatewindow(strm, end, copy)
z_streamp strm;
const Bytef *end;
unsigned copy;
{
    struct inflate_state *state;
    unsigned dist;

    state = (struct inflate_state *)strm->state;


    if (state->window == 0) {
        state->window = (unsigned char *)
                        (*((strm)->zalloc))((strm)->opaque, (1U << state->wbits), (sizeof(unsigned char)));

        if (state->window == 0) return 1;
    }


    if (state->wsize == 0) {
        state->wsize = 1U << state->wbits;
        state->wnext = 0;
        state->whave = 0;
    }


    if (copy >= state->wsize) {
        memcpy(state->window, end - state->wsize, state->wsize);
        state->wnext = 0;
        state->whave = state->wsize;
    }
    else {
        dist = state->wsize - state->wnext;
        if (dist > copy) dist = copy;
        memcpy(state->window + state->wnext, end - copy, dist);
        copy -= dist;
        if (copy) {
            memcpy(state->window, end - copy, copy);
            state->wnext = copy;
            state->whave = state->wsize;
        }
        else {
            state->wnext += dist;
            if (state->wnext == state->wsize) state->wnext = 0;
            if (state->whave < state->wsize) state->whave += dist;
        }
    }
    return 0;
}
int inflate(strm, flush)
z_streamp strm;
int flush;
{
    struct inflate_state *state;
            unsigned char *next;
    unsigned char *put;
    unsigned have, left;
    unsigned long hold;
    unsigned bits;
    unsigned in, out;
    unsigned copy;
    unsigned char *from;
    code here;
    code last;
    unsigned len;
    int ret;

    unsigned char hbuf[4];

    static const unsigned short order[19] =
        {16, 17, 18, 0, 8, 7, 9, 6, 10, 5, 11, 4, 12, 3, 13, 2, 14, 1, 15};

    if (inflateStateCheck(strm) || strm->next_out == 0 ||
        (strm->next_in == 0 && strm->avail_in != 0))
        return (-2);

    state = (struct inflate_state *)strm->state;
    if (state->mode == TYPE) state->mode = TYPEDO;
    do { put = strm->next_out; left = strm->avail_out; next = strm->next_in; have = strm->avail_in; hold = state->hold; bits = state->bits; } while (0);
    in = have;
    out = left;
    ret = 0;
    for (;;)
        switch (state->mode) {
        case HEAD:
            if (state->wrap == 0) {
                state->mode = TYPEDO;
                break;
            }
            do { while (bits < (unsigned)(16)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);

            if ((state->wrap & 2) && hold == 0x8b1f) {
                if (state->wbits == 0)
                    state->wbits = 15;
                state->check = crc32(0L, 0, 0);
                do { hbuf[0] = (unsigned char)(hold); hbuf[1] = (unsigned char)((hold) >> 8); state->check = crc32(state->check, hbuf, 2); } while (0);
                do { hold = 0; bits = 0; } while (0);
                state->mode = FLAGS;
                break;
            }
            state->flags = 0;
            if (state->head != 0)
                state->head->done = -1;
            if (!(state->wrap & 1) ||



                ((((unsigned)hold & ((1U << (8)) - 1)) << 8) + (hold >> 8)) % 31) {
                strm->msg = (char *)"incorrect header check";
                state->mode = BAD;
                break;
            }
            if (((unsigned)hold & ((1U << (4)) - 1)) != 8) {
                strm->msg = (char *)"unknown compression method";
                state->mode = BAD;
                break;
            }
            do { hold >>= (4); bits -= (unsigned)(4); } while (0);
            len = ((unsigned)hold & ((1U << (4)) - 1)) + 8;
            if (state->wbits == 0)
                state->wbits = len;
            if (len > 15 || len > state->wbits) {
                strm->msg = (char *)"invalid window size";
                state->mode = BAD;
                break;
            }
            state->dmax = 1U << len;
                                                           ;
            strm->adler = state->check = adler32(0L, 0, 0);
            state->mode = hold & 0x200 ? DICTID : TYPE;
            do { hold = 0; bits = 0; } while (0);
            break;

        case FLAGS:
            do { while (bits < (unsigned)(16)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
            state->flags = (int)(hold);
            if ((state->flags & 0xff) != 8) {
                strm->msg = (char *)"unknown compression method";
                state->mode = BAD;
                break;
            }
            if (state->flags & 0xe000) {
                strm->msg = (char *)"unknown header flags set";
                state->mode = BAD;
                break;
            }
            if (state->head != 0)
                state->head->text = (int)((hold >> 8) & 1);
            if ((state->flags & 0x0200) && (state->wrap & 4))
                do { hbuf[0] = (unsigned char)(hold); hbuf[1] = (unsigned char)((hold) >> 8); state->check = crc32(state->check, hbuf, 2); } while (0);
            do { hold = 0; bits = 0; } while (0);
            state->mode = TIME;
        case TIME:
            do { while (bits < (unsigned)(32)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
            if (state->head != 0)
                state->head->time = hold;
            if ((state->flags & 0x0200) && (state->wrap & 4))
                do { hbuf[0] = (unsigned char)(hold); hbuf[1] = (unsigned char)((hold) >> 8); hbuf[2] = (unsigned char)((hold) >> 16); hbuf[3] = (unsigned char)((hold) >> 24); state->check = crc32(state->check, hbuf, 4); } while (0);
            do { hold = 0; bits = 0; } while (0);
            state->mode = OS;
        case OS:
            do { while (bits < (unsigned)(16)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
            if (state->head != 0) {
                state->head->xflags = (int)(hold & 0xff);
                state->head->os = (int)(hold >> 8);
            }
            if ((state->flags & 0x0200) && (state->wrap & 4))
                do { hbuf[0] = (unsigned char)(hold); hbuf[1] = (unsigned char)((hold) >> 8); state->check = crc32(state->check, hbuf, 2); } while (0);
            do { hold = 0; bits = 0; } while (0);
            state->mode = EXLEN;
        case EXLEN:
            if (state->flags & 0x0400) {
                do { while (bits < (unsigned)(16)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
                state->length = (unsigned)(hold);
                if (state->head != 0)
                    state->head->extra_len = (unsigned)hold;
                if ((state->flags & 0x0200) && (state->wrap & 4))
                    do { hbuf[0] = (unsigned char)(hold); hbuf[1] = (unsigned char)((hold) >> 8); state->check = crc32(state->check, hbuf, 2); } while (0);
                do { hold = 0; bits = 0; } while (0);
            }
            else if (state->head != 0)
                state->head->extra = 0;
            state->mode = EXTRA;
        case EXTRA:
            if (state->flags & 0x0400) {
                copy = state->length;
                if (copy > have) copy = have;
                if (copy) {
                    if (state->head != 0 &&
                        state->head->extra != 0) {
                        len = state->head->extra_len - state->length;
                        memcpy(state->head->extra + len, next,
                                len + copy > state->head->extra_max ?
                                state->head->extra_max - len : copy);
                    }
                    if ((state->flags & 0x0200) && (state->wrap & 4))
                        state->check = crc32(state->check, next, copy);
                    have -= copy;
                    next += copy;
                    state->length -= copy;
                }
                if (state->length) goto inf_leave;
            }
            state->length = 0;
            state->mode = NAME;
        case NAME:
            if (state->flags & 0x0800) {
                if (have == 0) goto inf_leave;
                copy = 0;
                do {
                    len = (unsigned)(next[copy++]);
                    if (state->head != 0 &&
                            state->head->name != 0 &&
                            state->length < state->head->name_max)
                        state->head->name[state->length++] = (Bytef)len;
                } while (len && copy < have);
                if ((state->flags & 0x0200) && (state->wrap & 4))
                    state->check = crc32(state->check, next, copy);
                have -= copy;
                next += copy;
                if (len) goto inf_leave;
            }
            else if (state->head != 0)
                state->head->name = 0;
            state->length = 0;
            state->mode = COMMENT;
        case COMMENT:
            if (state->flags & 0x1000) {
                if (have == 0) goto inf_leave;
                copy = 0;
                do {
                    len = (unsigned)(next[copy++]);
                    if (state->head != 0 &&
                            state->head->comment != 0 &&
                            state->length < state->head->comm_max)
                        state->head->comment[state->length++] = (Bytef)len;
                } while (len && copy < have);
                if ((state->flags & 0x0200) && (state->wrap & 4))
                    state->check = crc32(state->check, next, copy);
                have -= copy;
                next += copy;
                if (len) goto inf_leave;
            }
            else if (state->head != 0)
                state->head->comment = 0;
            state->mode = HCRC;
        case HCRC:
            if (state->flags & 0x0200) {
                do { while (bits < (unsigned)(16)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
                if ((state->wrap & 4) && hold != (state->check & 0xffff)) {
                    strm->msg = (char *)"header crc mismatch";
                    state->mode = BAD;
                    break;
                }
                do { hold = 0; bits = 0; } while (0);
            }
            if (state->head != 0) {
                state->head->hcrc = (int)((state->flags >> 9) & 1);
                state->head->done = 1;
            }
            strm->adler = state->check = crc32(0L, 0, 0);
            state->mode = TYPE;
            break;

        case DICTID:
            do { while (bits < (unsigned)(32)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
            strm->adler = state->check = ((((hold) >> 24) & 0xff) + (((hold) >> 8) & 0xff00) + (((hold) & 0xff00) << 8) + (((hold) & 0xff) << 24));
            do { hold = 0; bits = 0; } while (0);
            state->mode = DICT;
        case DICT:
            if (state->havedict == 0) {
                do { strm->next_out = put; strm->avail_out = left; strm->next_in = next; strm->avail_in = have; state->hold = hold; state->bits = bits; } while (0);
                return 2;
            }
            strm->adler = state->check = adler32(0L, 0, 0);
            state->mode = TYPE;
        case TYPE:
            if (flush == 5 || flush == 6) goto inf_leave;
        case TYPEDO:
            if (state->last) {
                do { hold >>= bits & 7; bits -= bits & 7; } while (0);
                state->mode = CHECK;
                break;
            }
            do { while (bits < (unsigned)(3)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
            state->last = ((unsigned)hold & ((1U << (1)) - 1));
            do { hold >>= (1); bits -= (unsigned)(1); } while (0);
            switch (((unsigned)hold & ((1U << (2)) - 1))) {
            case 0:

                                                      ;
                state->mode = STORED;
                break;
            case 1:
                fixedtables(state);

                                                      ;
                state->mode = LEN_;
                if (flush == 6) {
                    do { hold >>= (2); bits -= (unsigned)(2); } while (0);
                    goto inf_leave;
                }
                break;
            case 2:

                                                      ;
                state->mode = TABLE;
                break;
            case 3:
                strm->msg = (char *)"invalid block type";
                state->mode = BAD;
            }
            do { hold >>= (2); bits -= (unsigned)(2); } while (0);
            break;
        case STORED:
            do { hold >>= bits & 7; bits -= bits & 7; } while (0);
            do { while (bits < (unsigned)(32)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
            if ((hold & 0xffff) != ((hold >> 16) ^ 0xffff)) {
                strm->msg = (char *)"invalid stored block lengths";
                state->mode = BAD;
                break;
            }
            state->length = (unsigned)hold & 0xffff;

                                   ;
            do { hold = 0; bits = 0; } while (0);
            state->mode = COPY_;
            if (flush == 6) goto inf_leave;
        case COPY_:
            state->mode = COPY;
        case COPY:
            copy = state->length;
            if (copy) {
                if (copy > have) copy = have;
                if (copy > left) copy = left;
                if (copy == 0) goto inf_leave;
                memcpy(put, next, copy);
                have -= copy;
                next += copy;
                left -= copy;
                put += copy;
                state->length -= copy;
                break;
            }
                                                           ;
            state->mode = TYPE;
            break;
        case TABLE:
            do { while (bits < (unsigned)(14)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
            state->nlen = ((unsigned)hold & ((1U << (5)) - 1)) + 257;
            do { hold >>= (5); bits -= (unsigned)(5); } while (0);
            state->ndist = ((unsigned)hold & ((1U << (5)) - 1)) + 1;
            do { hold >>= (5); bits -= (unsigned)(5); } while (0);
            state->ncode = ((unsigned)hold & ((1U << (4)) - 1)) + 4;
            do { hold >>= (4); bits -= (unsigned)(4); } while (0);

            if (state->nlen > 286 || state->ndist > 30) {
                strm->msg = (char *)"too many length or distance symbols";
                state->mode = BAD;
                break;
            }

                                                               ;
            state->have = 0;
            state->mode = LENLENS;
        case LENLENS:
            while (state->have < state->ncode) {
                do { while (bits < (unsigned)(3)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
                state->lens[order[state->have++]] = (unsigned short)((unsigned)hold & ((1U << (3)) - 1));
                do { hold >>= (3); bits -= (unsigned)(3); } while (0);
            }
            while (state->have < 19)
                state->lens[order[state->have++]] = 0;
            state->next = state->codes;
            state->lencode = (const code *)(state->next);
            state->lenbits = 7;
            ret = inflate_table(CODES, state->lens, 19, &(state->next),
                                &(state->lenbits), state->work);
            if (ret) {
                strm->msg = (char *)"invalid code lengths set";
                state->mode = BAD;
                break;
            }
                                                                ;
            state->have = 0;
            state->mode = CODELENS;
        case CODELENS:
            while (state->have < state->nlen + state->ndist) {
                for (;;) {
                    here = state->lencode[((unsigned)hold & ((1U << (state->lenbits)) - 1))];
                    if ((unsigned)(here.bits) <= bits) break;
                    do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0);
                }
                if (here.val < 16) {
                    do { hold >>= (here.bits); bits -= (unsigned)(here.bits); } while (0);
                    state->lens[state->have++] = here.val;
                }
                else {
                    if (here.val == 16) {
                        do { while (bits < (unsigned)(here.bits + 2)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
                        do { hold >>= (here.bits); bits -= (unsigned)(here.bits); } while (0);
                        if (state->have == 0) {
                            strm->msg = (char *)"invalid bit length repeat";
                            state->mode = BAD;
                            break;
                        }
                        len = state->lens[state->have - 1];
                        copy = 3 + ((unsigned)hold & ((1U << (2)) - 1));
                        do { hold >>= (2); bits -= (unsigned)(2); } while (0);
                    }
                    else if (here.val == 17) {
                        do { while (bits < (unsigned)(here.bits + 3)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
                        do { hold >>= (here.bits); bits -= (unsigned)(here.bits); } while (0);
                        len = 0;
                        copy = 3 + ((unsigned)hold & ((1U << (3)) - 1));
                        do { hold >>= (3); bits -= (unsigned)(3); } while (0);
                    }
                    else {
                        do { while (bits < (unsigned)(here.bits + 7)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
                        do { hold >>= (here.bits); bits -= (unsigned)(here.bits); } while (0);
                        len = 0;
                        copy = 11 + ((unsigned)hold & ((1U << (7)) - 1));
                        do { hold >>= (7); bits -= (unsigned)(7); } while (0);
                    }
                    if (state->have + copy > state->nlen + state->ndist) {
                        strm->msg = (char *)"invalid bit length repeat";
                        state->mode = BAD;
                        break;
                    }
                    while (copy--)
                        state->lens[state->have++] = (unsigned short)len;
                }
            }


            if (state->mode == BAD) break;


            if (state->lens[256] == 0) {
                strm->msg = (char *)"invalid code -- missing end-of-block";
                state->mode = BAD;
                break;
            }




            state->next = state->codes;
            state->lencode = (const code *)(state->next);
            state->lenbits = 9;
            ret = inflate_table(LENS, state->lens, state->nlen, &(state->next),
                                &(state->lenbits), state->work);
            if (ret) {
                strm->msg = (char *)"invalid literal/lengths set";
                state->mode = BAD;
                break;
            }
            state->distcode = (const code *)(state->next);
            state->distbits = 6;
            ret = inflate_table(DISTS, state->lens + state->nlen, state->ndist,
                            &(state->next), &(state->distbits), state->work);
            if (ret) {
                strm->msg = (char *)"invalid distances set";
                state->mode = BAD;
                break;
            }
                                                         ;
            state->mode = LEN_;
            if (flush == 6) goto inf_leave;
        case LEN_:
            state->mode = LEN;
        case LEN:
            if (have >= 6 && left >= 258) {
                do { strm->next_out = put; strm->avail_out = left; strm->next_in = next; strm->avail_in = have; state->hold = hold; state->bits = bits; } while (0);
                inflate_fast(strm, out);
                do { put = strm->next_out; left = strm->avail_out; next = strm->next_in; have = strm->avail_in; hold = state->hold; bits = state->bits; } while (0);
                if (state->mode == TYPE)
                    state->back = -1;
                break;
            }
            state->back = 0;
            for (;;) {
                here = state->lencode[((unsigned)hold & ((1U << (state->lenbits)) - 1))];
                if ((unsigned)(here.bits) <= bits) break;
                do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0);
            }
            if (here.op && (here.op & 0xf0) == 0) {
                last = here;
                for (;;) {
                    here = state->lencode[last.val +
                            (((unsigned)hold & ((1U << (last.bits + last.op)) - 1)) >> last.bits)];
                    if ((unsigned)(last.bits + here.bits) <= bits) break;
                    do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0);
                }
                do { hold >>= (last.bits); bits -= (unsigned)(last.bits); } while (0);
                state->back += last.bits;
            }
            do { hold >>= (here.bits); bits -= (unsigned)(here.bits); } while (0);
            state->back += here.bits;
            state->length = (unsigned)here.val;
            if ((int)(here.op) == 0) {


                                                                       ;
                state->mode = LIT;
                break;
            }
            if (here.op & 32) {
                                                                    ;
                state->back = -1;
                state->mode = TYPE;
                break;
            }
            if (here.op & 64) {
                strm->msg = (char *)"invalid literal/length code";
                state->mode = BAD;
                break;
            }
            state->extra = (unsigned)(here.op) & 15;
            state->mode = LENEXT;
        case LENEXT:
            if (state->extra) {
                do { while (bits < (unsigned)(state->extra)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
                state->length += ((unsigned)hold & ((1U << (state->extra)) - 1));
                do { hold >>= (state->extra); bits -= (unsigned)(state->extra); } while (0);
                state->back += state->extra;
            }
                                                                            ;
            state->was = state->length;
            state->mode = DIST;
        case DIST:
            for (;;) {
                here = state->distcode[((unsigned)hold & ((1U << (state->distbits)) - 1))];
                if ((unsigned)(here.bits) <= bits) break;
                do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0);
            }
            if ((here.op & 0xf0) == 0) {
                last = here;
                for (;;) {
                    here = state->distcode[last.val +
                            (((unsigned)hold & ((1U << (last.bits + last.op)) - 1)) >> last.bits)];
                    if ((unsigned)(last.bits + here.bits) <= bits) break;
                    do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0);
                }
                do { hold >>= (last.bits); bits -= (unsigned)(last.bits); } while (0);
                state->back += last.bits;
            }
            do { hold >>= (here.bits); bits -= (unsigned)(here.bits); } while (0);
            state->back += here.bits;
            if (here.op & 64) {
                strm->msg = (char *)"invalid distance code";
                state->mode = BAD;
                break;
            }
            state->offset = (unsigned)here.val;
            state->extra = (unsigned)(here.op) & 15;
            state->mode = DISTEXT;
        case DISTEXT:
            if (state->extra) {
                do { while (bits < (unsigned)(state->extra)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
                state->offset += ((unsigned)hold & ((1U << (state->extra)) - 1));
                do { hold >>= (state->extra); bits -= (unsigned)(state->extra); } while (0);
                state->back += state->extra;
            }







                                                                              ;
            state->mode = MATCH;
        case MATCH:
            if (left == 0) goto inf_leave;
            copy = out - left;
            if (state->offset > copy) {
                copy = state->offset - copy;
                if (copy > state->whave) {
                    if (state->sane) {
                        strm->msg = (char *)"invalid distance too far back";
                        state->mode = BAD;
                        break;
                    }
                }
                if (copy > state->wnext) {
                    copy -= state->wnext;
                    from = state->window + (state->wsize - copy);
                }
                else
                    from = state->window + (state->wnext - copy);
                if (copy > state->length) copy = state->length;
            }
            else {
                from = put - state->offset;
                copy = state->length;
            }
            if (copy > left) copy = left;
            left -= copy;
            state->length -= copy;
            do {
                *put++ = *from++;
            } while (--copy);
            if (state->length == 0) state->mode = LEN;
            break;
        case LIT:
            if (left == 0) goto inf_leave;
            *put++ = (unsigned char)(state->length);
            left--;
            state->mode = LEN;
            break;
        case CHECK:
            if (state->wrap) {
                do { while (bits < (unsigned)(32)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
                out -= left;
                strm->total_out += out;
                state->total += out;
                if ((state->wrap & 4) && out)
                    strm->adler = state->check =
                        (state->flags ? crc32(state->check, put - out, out) : adler32(state->check, put - out, out));
                out = left;
                if ((state->wrap & 4) && (

                     state->flags ? hold :

                     ((((hold) >> 24) & 0xff) + (((hold) >> 8) & 0xff00) + (((hold) & 0xff00) << 8) + (((hold) & 0xff) << 24))) != state->check) {
                    strm->msg = (char *)"incorrect data check";
                    state->mode = BAD;
                    break;
                }
                do { hold = 0; bits = 0; } while (0);
                                                                      ;
            }

            state->mode = LENGTH;
        case LENGTH:
            if (state->wrap && state->flags) {
                do { while (bits < (unsigned)(32)) do { if (have == 0) goto inf_leave; have--; hold += (unsigned long)(*next++) << bits; bits += 8; } while (0); } while (0);
                if (hold != (state->total & 0xffffffffUL)) {
                    strm->msg = (char *)"incorrect length check";
                    state->mode = BAD;
                    break;
                }
                do { hold = 0; bits = 0; } while (0);
                                                                       ;
            }

            state->mode = DONE;
        case DONE:
            ret = 1;
            goto inf_leave;
        case BAD:
            ret = (-3);
            goto inf_leave;
        case MEM:
            return (-4);
        case SYNC:
        default:
            return (-2);
        }







  inf_leave:
    do { strm->next_out = put; strm->avail_out = left; strm->next_in = next; strm->avail_in = have; state->hold = hold; state->bits = bits; } while (0);
    if (state->wsize || (out != strm->avail_out && state->mode < BAD &&
            (state->mode < CHECK || flush != 4)))
        if (updatewindow(strm, strm->next_out, out - strm->avail_out)) {
            state->mode = MEM;
            return (-4);
        }
    in -= strm->avail_in;
    out -= strm->avail_out;
    strm->total_in += in;
    strm->total_out += out;
    state->total += out;
    if ((state->wrap & 4) && out)
        strm->adler = state->check =
            (state->flags ? crc32(state->check, strm->next_out - out, out) : adler32(state->check, strm->next_out - out, out));
    strm->data_type = (int)state->bits + (state->last ? 64 : 0) +
                      (state->mode == TYPE ? 128 : 0) +
                      (state->mode == LEN_ || state->mode == COPY_ ? 256 : 0);
    if (((in == 0 && out == 0) || flush == 4) && ret == 0)
        ret = (-5);
    return ret;
}

int inflateEnd(strm)
z_streamp strm;
{
    struct inflate_state *state;
    if (inflateStateCheck(strm))
        return (-2);
    state = (struct inflate_state *)strm->state;
    if (state->window != 0) (*((strm)->zfree))((strm)->opaque, (voidpf)(state->window));
    (*((strm)->zfree))((strm)->opaque, (voidpf)(strm->state));
    strm->state = 0;
                                      ;
    return 0;
}

int inflateGetDictionary(strm, dictionary, dictLength)
z_streamp strm;
Bytef *dictionary;
uInt *dictLength;
{
    struct inflate_state *state;


    if (inflateStateCheck(strm)) return (-2);
    state = (struct inflate_state *)strm->state;


    if (state->whave && dictionary != 0) {
        memcpy(dictionary, state->window + state->wnext,
                state->whave - state->wnext);
        memcpy(dictionary + state->whave - state->wnext,
                state->window, state->wnext);
    }
    if (dictLength != 0)
        *dictLength = state->whave;
    return 0;
}

int inflateSetDictionary(strm, dictionary, dictLength)
z_streamp strm;
const Bytef *dictionary;
uInt dictLength;
{
    struct inflate_state *state;
    unsigned long dictid;
    int ret;


    if (inflateStateCheck(strm)) return (-2);
    state = (struct inflate_state *)strm->state;
    if (state->wrap != 0 && state->mode != DICT)
        return (-2);


    if (state->mode == DICT) {
        dictid = adler32(0L, 0, 0);
        dictid = adler32(dictid, dictionary, dictLength);
        if (dictid != state->check)
            return (-3);
    }



    ret = updatewindow(strm, dictionary + dictLength, dictLength);
    if (ret) {
        state->mode = MEM;
        return (-4);
    }
    state->havedict = 1;
                                                   ;
    return 0;
}

int inflateGetHeader(strm, head)
z_streamp strm;
gz_headerp head;
{
    struct inflate_state *state;


    if (inflateStateCheck(strm)) return (-2);
    state = (struct inflate_state *)strm->state;
    if ((state->wrap & 2) == 0) return (-2);


    state->head = head;
    head->done = 0;
    return 0;
}
static unsigned syncsearch(have, buf, len)
unsigned *have;
const unsigned char *buf;
unsigned len;
{
    unsigned got;
    unsigned next;

    got = *have;
    next = 0;
    while (next < len && got < 4) {
        if ((int)(buf[next]) == (got < 2 ? 0 : 0xff))
            got++;
        else if (buf[next])
            got = 0;
        else
            got = 4 - got;
        next++;
    }
    *have = got;
    return next;
}

int inflateSync(strm)
z_streamp strm;
{
    unsigned len;
    unsigned long in, out;
    unsigned char buf[4];
    struct inflate_state *state;


    if (inflateStateCheck(strm)) return (-2);
    state = (struct inflate_state *)strm->state;
    if (strm->avail_in == 0 && state->bits < 8) return (-5);


    if (state->mode != SYNC) {
        state->mode = SYNC;
        state->hold <<= state->bits & 7;
        state->bits -= state->bits & 7;
        len = 0;
        while (state->bits >= 8) {
            buf[len++] = (unsigned char)(state->hold);
            state->hold >>= 8;
            state->bits -= 8;
        }
        state->have = 0;
        syncsearch(&(state->have), buf, len);
    }


    len = syncsearch(&(state->have), strm->next_in, strm->avail_in);
    strm->avail_in -= len;
    strm->next_in += len;
    strm->total_in += len;


    if (state->have != 4) return (-3);
    in = strm->total_in; out = strm->total_out;
    inflateReset(strm);
    strm->total_in = in; strm->total_out = out;
    state->mode = TYPE;
    return 0;
}
int inflateSyncPoint(strm)
z_streamp strm;
{
    struct inflate_state *state;

    if (inflateStateCheck(strm)) return (-2);
    state = (struct inflate_state *)strm->state;
    return state->mode == STORED && state->bits == 0;
}

int inflateCopy(dest, source)
z_streamp dest;
z_streamp source;
{
    struct inflate_state *state;
    struct inflate_state *copy;
    unsigned char *window;
    unsigned wsize;


    if (inflateStateCheck(source) || dest == 0)
        return (-2);
    state = (struct inflate_state *)source->state;


    copy = (struct inflate_state *)
           (*((source)->zalloc))((source)->opaque, (1), (sizeof(struct inflate_state)));
    if (copy == 0) return (-4);
    window = 0;
    if (state->window != 0) {
        window = (unsigned char *)
                 (*((source)->zalloc))((source)->opaque, (1U << state->wbits), (sizeof(unsigned char)));
        if (window == 0) {
            (*((source)->zfree))((source)->opaque, (voidpf)(copy));
            return (-4);
        }
    }


    memcpy((voidpf)dest, (voidpf)source, sizeof(z_stream));
    memcpy((voidpf)copy, (voidpf)state, sizeof(struct inflate_state));
    copy->strm = dest;
    if (state->lencode >= state->codes &&
        state->lencode <= state->codes + (852 +592) - 1) {
        copy->lencode = copy->codes + (state->lencode - state->codes);
        copy->distcode = copy->codes + (state->distcode - state->codes);
    }
    copy->next = copy->codes + (state->next - state->codes);
    if (window != 0) {
        wsize = 1U << state->wbits;
        memcpy(window, state->window, wsize);
    }
    copy->window = window;
    dest->state = (struct internal_state *)copy;
    return 0;
}

int inflateUndermine(strm, subvert)
z_streamp strm;
int subvert;
{
    struct inflate_state *state;

    if (inflateStateCheck(strm)) return (-2);
    state = (struct inflate_state *)strm->state;




    (void)subvert;
    state->sane = 1;
    return (-3);

}

int inflateValidate(strm, check)
z_streamp strm;
int check;
{
    struct inflate_state *state;

    if (inflateStateCheck(strm)) return (-2);
    state = (struct inflate_state *)strm->state;
    if (check)
        state->wrap |= 4;
    else
        state->wrap &= ~4;
    return 0;
}

long inflateMark(strm)
z_streamp strm;
{
    struct inflate_state *state;

    if (inflateStateCheck(strm))
        return -(1L << 16);
    state = (struct inflate_state *)strm->state;
    return (long)(((unsigned long)((long)state->back)) << 16) +
        (state->mode == COPY ? state->length :
            (state->mode == MATCH ? state->was - state->length : 0));
}

unsigned long inflateCodesUsed(strm)
z_streamp strm;
{
    struct inflate_state *state;
    if (inflateStateCheck(strm)) return (unsigned long)-1;
    state = (struct inflate_state *)strm->state;
    return (unsigned long)(state->next - state->codes);
}
