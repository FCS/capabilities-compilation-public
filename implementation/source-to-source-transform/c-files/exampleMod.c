static int doubleIt(int a)
{
        int b;

        b = a * 2;
	b++;
	b--;
	b += 3;

        return b;
}

static int tripleIt(int a)
{
        int b;

        b = doubleIt(a) + a;

        return b;
}

void pentaIt(int *a)
{
        int b;

        b = doubleIt(*a) + tripleIt(*a);
        *a = b;

        return;
}
