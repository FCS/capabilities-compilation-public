typedef long __intcap_t;
typedef long __uintcap_t;






typedef signed char __int8_t;
typedef unsigned char __uint8_t;
typedef short __int16_t;
typedef unsigned short __uint16_t;
typedef int __int32_t;
typedef unsigned int __uint32_t;

typedef long __int64_t;
typedef unsigned long __uint64_t;
typedef __int32_t __clock_t;
typedef double __double_t;
typedef float __float_t;






typedef __intcap_t __critical_t;
typedef __intcap_t __intfptr_t;
typedef __intcap_t __intptr_t;






typedef __int64_t __intmax_t;
typedef __int32_t __int_fast8_t;
typedef __int32_t __int_fast16_t;
typedef __int32_t __int_fast32_t;
typedef __int64_t __int_fast64_t;
typedef __int8_t __int_least8_t;
typedef __int16_t __int_least16_t;
typedef __int32_t __int_least32_t;
typedef __int64_t __int_least64_t;

typedef __int64_t __register_t;
typedef __int64_t f_register_t;





typedef __int64_t __ptrdiff_t;
typedef __int64_t __segsz_t;
typedef __uint64_t __size_t;
typedef __int64_t __ssize_t;




typedef __uintcap_t __uintfptr_t;
typedef __uintcap_t __uintptr_t;
typedef __int64_t __time_t;
typedef __uint64_t __uintmax_t;
typedef __uint32_t __uint_fast8_t;
typedef __uint32_t __uint_fast16_t;
typedef __uint32_t __uint_fast32_t;
typedef __uint64_t __uint_fast64_t;
typedef __uint8_t __uint_least8_t;
typedef __uint16_t __uint_least16_t;
typedef __uint32_t __uint_least32_t;
typedef __uint64_t __uint_least64_t;

typedef __uint64_t __u_register_t;




typedef __uint64_t __vm_offset_t;
typedef __uint64_t __vm_size_t;





typedef __uint64_t __vm_paddr_t;




typedef __int64_t __vm_ooffset_t;
typedef __uint64_t __vm_pindex_t;
typedef int ___wchar_t;
typedef __builtin_va_list __va_list;






typedef __va_list __gnuc_va_list;




typedef __int32_t __blksize_t;
typedef __int64_t __blkcnt_t;
typedef __int32_t __clockid_t;
typedef __uint32_t __fflags_t;
typedef __uint64_t __fsblkcnt_t;
typedef __uint64_t __fsfilcnt_t;
typedef __uint32_t __gid_t;
typedef __int64_t __id_t;
typedef __uint32_t __ino_t;
typedef long __key_t;
typedef __int32_t __lwpid_t;
typedef __uint16_t __mode_t;
typedef int __accmode_t;
typedef int __nl_item;
typedef __uint16_t __nlink_t;
typedef __int64_t __off_t;
typedef __int64_t __off64_t;
typedef __int32_t __pid_t;
typedef __int64_t __rlim_t;


typedef __uint8_t __sa_family_t;
typedef __uint32_t __socklen_t;
typedef long __suseconds_t;
typedef struct __timer *__timer_t;
typedef struct __mq *__mqd_t;
typedef __uint32_t __uid_t;
typedef unsigned int __useconds_t;
typedef int __cpuwhich_t;
typedef int __cpulevel_t;
typedef int __cpusetid_t;
typedef int __ct_rune_t;
typedef __ct_rune_t __rune_t;
typedef __ct_rune_t __wint_t;



typedef __uint_least16_t __char16_t;
typedef __uint_least32_t __char32_t;







typedef struct {
        long long __max_align1;
 long double __max_align2;
} __max_align_t;

typedef __uint32_t __dev_t;

typedef __uint32_t __fixpt_t;





typedef union {
 char __mbstate8[128];
 __int64_t _mbstateL;
} __mbstate_t;

typedef __uintmax_t __rman_res_t;


typedef __ptrdiff_t ptrdiff_t;





typedef __rune_t rune_t;





typedef __size_t size_t;





typedef ___wchar_t wchar_t;






typedef __max_align_t max_align_t;
 typedef size_t z_size_t;
typedef unsigned char Byte;

typedef unsigned int uInt;
typedef unsigned long uLong;





   typedef Byte Bytef;

typedef char charf;
typedef int intf;
typedef uInt uIntf;
typedef uLong uLongf;


   typedef void const *voidpc;
   typedef void *voidpf;
   typedef void *voidp;










   typedef unsigned z_crc_t;
static __inline __uint16_t
__bswap16_var(__uint16_t _x)
{

 return ((_x >> 8) | ((_x << 8) & 0xff00));
}

static __inline __uint32_t
__bswap32_var(__uint32_t _x)
{

 return ((_x >> 24) | ((_x >> 8) & 0xff00) | ((_x << 8) & 0xff0000) |
     ((_x << 24) & 0xff000000));
}

static __inline __uint64_t
__bswap64_var(__uint64_t _x)
{

 return ((_x >> 56) | ((_x >> 40) & 0xff00) | ((_x >> 24) & 0xff0000) |
     ((_x >> 8) & 0xff000000) | ((_x << 8) & ((__uint64_t)0xff << 32)) |
     ((_x << 24) & ((__uint64_t)0xff << 40)) |
     ((_x << 40) & ((__uint64_t)0xff << 48)) | ((_x << 56)));
}


struct pthread;
struct pthread_attr;
struct pthread_cond;
struct pthread_cond_attr;
struct pthread_mutex;
struct pthread_mutex_attr;
struct pthread_once;
struct pthread_rwlock;
struct pthread_rwlockattr;
struct pthread_barrier;
struct pthread_barrier_attr;
struct pthread_spinlock;
typedef struct pthread *pthread_t;


typedef struct pthread_attr *pthread_attr_t;
typedef struct pthread_mutex *pthread_mutex_t;
typedef struct pthread_mutex_attr *pthread_mutexattr_t;
typedef struct pthread_cond *pthread_cond_t;
typedef struct pthread_cond_attr *pthread_condattr_t;
typedef int pthread_key_t;
typedef struct pthread_once pthread_once_t;
typedef struct pthread_rwlock *pthread_rwlock_t;
typedef struct pthread_rwlockattr *pthread_rwlockattr_t;
typedef struct pthread_barrier *pthread_barrier_t;
typedef struct pthread_barrierattr *pthread_barrierattr_t;
typedef struct pthread_spinlock *pthread_spinlock_t;







typedef void *pthread_addr_t;
typedef void *(*pthread_startroutine_t)(void *);




struct pthread_once {
 int state;
 pthread_mutex_t mutex;
};


typedef unsigned char u_char;
typedef unsigned short u_short;
typedef unsigned int u_int;
typedef unsigned long u_long;

typedef unsigned short ushort;
typedef unsigned int uint;







typedef __int8_t int8_t;




typedef __int16_t int16_t;




typedef __int32_t int32_t;




typedef __int64_t int64_t;




typedef __uint8_t uint8_t;




typedef __uint16_t uint16_t;




typedef __uint32_t uint32_t;




typedef __uint64_t uint64_t;




typedef __intptr_t intptr_t;



typedef __uintptr_t uintptr_t;



typedef __intmax_t intmax_t;



typedef __uintmax_t uintmax_t;






typedef __uint64_t vaddr_t;

typedef __uint8_t u_int8_t;
typedef __uint16_t u_int16_t;
typedef __uint32_t u_int32_t;
typedef __uint64_t u_int64_t;

typedef __uint64_t u_quad_t;
typedef __int64_t quad_t;
typedef quad_t * qaddr_t;

typedef char * caddr_t;
typedef const char * c_caddr_t;


typedef __blksize_t blksize_t;



typedef __cpuwhich_t cpuwhich_t;
typedef __cpulevel_t cpulevel_t;
typedef __cpusetid_t cpusetid_t;


typedef __blkcnt_t blkcnt_t;




typedef __clock_t clock_t;




typedef __clockid_t clockid_t;



typedef __critical_t critical_t;
typedef __int64_t daddr_t;


typedef __dev_t dev_t;




typedef __fflags_t fflags_t;



typedef __fixpt_t fixpt_t;


typedef __fsblkcnt_t fsblkcnt_t;
typedef __fsfilcnt_t fsfilcnt_t;




typedef __gid_t gid_t;




typedef __uint32_t in_addr_t;




typedef __uint16_t in_port_t;




typedef __id_t id_t;




typedef __ino_t ino_t;




typedef __key_t key_t;




typedef __lwpid_t lwpid_t;




typedef __mode_t mode_t;




typedef __accmode_t accmode_t;




typedef __nlink_t nlink_t;




typedef __off_t off_t;




typedef __off64_t off64_t;




typedef __pid_t pid_t;



typedef __register_t register_t;


typedef __rlim_t rlim_t;



typedef __int64_t sbintime_t;

typedef __segsz_t segsz_t;







typedef __ssize_t ssize_t;




typedef __suseconds_t suseconds_t;




typedef __time_t time_t;




typedef __timer_t timer_t;




typedef __mqd_t mqd_t;



typedef __u_register_t u_register_t;


typedef __uid_t uid_t;




typedef __useconds_t useconds_t;





typedef unsigned long cap_ioctl_t;




struct cap_rights;

typedef struct cap_rights cap_rights_t;


typedef __vm_offset_t vm_offset_t;
typedef __vm_ooffset_t vm_ooffset_t;
typedef __vm_paddr_t vm_paddr_t;
typedef __vm_pindex_t vm_pindex_t;
typedef __vm_size_t vm_size_t;

typedef __rman_res_t rman_res_t;
static __inline __uint16_t
__bitcount16(__uint16_t _x)
{

 _x = (_x & 0x5555) + ((_x & 0xaaaa) >> 1);
 _x = (_x & 0x3333) + ((_x & 0xcccc) >> 2);
 _x = (_x + (_x >> 4)) & 0x0f0f;
 _x = (_x + (_x >> 8)) & 0x00ff;
 return (_x);
}

static __inline __uint32_t
__bitcount32(__uint32_t _x)
{

 _x = (_x & 0x55555555) + ((_x & 0xaaaaaaaa) >> 1);
 _x = (_x & 0x33333333) + ((_x & 0xcccccccc) >> 2);
 _x = (_x + (_x >> 4)) & 0x0f0f0f0f;
 _x = (_x + (_x >> 8));
 _x = (_x + (_x >> 16)) & 0x000000ff;
 return (_x);
}


static __inline __uint64_t
__bitcount64(__uint64_t _x)
{

 _x = (_x & 0x5555555555555555) + ((_x & 0xaaaaaaaaaaaaaaaa) >> 1);
 _x = (_x & 0x3333333333333333) + ((_x & 0xcccccccccccccccc) >> 2);
 _x = (_x + (_x >> 4)) & 0x0f0f0f0f0f0f0f0f;
 _x = (_x + (_x >> 8));
 _x = (_x + (_x >> 16));
 _x = (_x + (_x >> 32)) & 0x000000ff;
 return (_x);
}
typedef struct __sigset {
 __uint32_t __bits[4];
} __sigset_t;
struct timeval {
 time_t tv_sec;
 suseconds_t tv_usec;
};
struct timespec {
 time_t tv_sec;
 long tv_nsec;
};
struct itimerspec {
 struct timespec it_interval;
 struct timespec it_value;
};

typedef unsigned long __fd_mask;

typedef __fd_mask fd_mask;




typedef __sigset_t sigset_t;
typedef struct fd_set {
 __fd_mask __fds_bits[(((1024) + (((sizeof(__fd_mask) * 8)) - 1)) / ((sizeof(__fd_mask) * 8)))];
} fd_set;
int pselect(int, fd_set *restrict, fd_set *restrict, fd_set *restrict,
 const struct timespec *restrict, const sigset_t *restrict);



int select(int, fd_set *, fd_set *, fd_set *, struct timeval *);
int ftruncate(int, off_t);



off_t lseek(int, off_t, int);



void * mmap(void *, size_t, int, int, int, off_t);



int truncate(const char *, off_t);





typedef __va_list va_list;
void _exit(int) __attribute__((__noreturn__));
int access(const char *, int);
unsigned int alarm(unsigned int);
int chdir(const char *);
int chown(const char *, uid_t, gid_t);
int close(int);
void closefrom(int);
int dup(int);
int dup2(int, int);
int execl(const char *, const char *, ...) __attribute__((__sentinel__));
int execle(const char *, const char *, ...);
int execlp(const char *, const char *, ...) __attribute__((__sentinel__));
int execv(const char *, char * const *);
int execve(const char *, char * const *, char * const *);
int execvp(const char *, char * const *);
pid_t fork(void);
long fpathconf(int, int);
char *getcwd(char *, size_t);
gid_t getegid(void);
uid_t geteuid(void);
gid_t getgid(void);
int getgroups(int, gid_t []);
char *getlogin(void);
pid_t getpgrp(void);
pid_t getpid(void);
pid_t getppid(void);
uid_t getuid(void);
int isatty(int);
int link(const char *, const char *);




long pathconf(const char *, int);
int pause(void);
int pipe(int *);
ssize_t read(int, void *, size_t);
int rmdir(const char *);
int setgid(gid_t);
int setpgid(pid_t, pid_t);
pid_t setsid(void);
int setuid(uid_t);
unsigned int sleep(unsigned int);
long sysconf(int);
pid_t tcgetpgrp(int);
int tcsetpgrp(int, pid_t);
char *ttyname(int);
int ttyname_r(int, char *, size_t);
int unlink(const char *);
ssize_t write(int, const void *, size_t);



size_t confstr(int, char *, size_t);


int getopt(int, char * const [], const char *);

extern char *optarg;
extern int optind, opterr, optopt;





int fsync(int);
int fdatasync(int);
int getlogin_r(char *, int);




int fchown(int, uid_t, gid_t);
ssize_t readlink(const char * restrict, char * restrict, size_t);


int gethostname(char *, size_t);
int setegid(gid_t);
int seteuid(uid_t);




int getsid(pid_t _pid);
int fchdir(int);
int getpgid(pid_t _pid);
int lchown(const char *, uid_t, gid_t);
ssize_t pread(int, void *, size_t, off_t);
ssize_t pwrite(int, const void *, size_t, off_t);
int faccessat(int, const char *, int, int);
int fchownat(int, const char *, uid_t, gid_t, int);
int fexecve(int, char *const [], char *const []);
int linkat(int, const char *, int, const char *, int);
ssize_t readlinkat(int, const char * restrict, char * restrict, size_t);
int symlinkat(const char *, int, const char *);
int unlinkat(int, const char *, int);
int symlink(const char * restrict, const char * restrict);




char *crypt(const char *, const char *);
long gethostid(void);
int lockf(int, int, off_t);
int nice(int);
int setregid(gid_t, gid_t);
int setreuid(uid_t, uid_t);



void swab(const void * restrict, void * restrict, ssize_t);


void sync(void);




int brk(const void *);
int chroot(const char *);
int getdtablesize(void);
int getpagesize(void) __attribute__((__const__));
char *getpass(const char *);
void *sbrk(intptr_t);



char *getwd(char *);
useconds_t
  ualarm(useconds_t, useconds_t);
int usleep(useconds_t);
pid_t vfork(void) __attribute__((__returns_twice__));



struct timeval;

struct crypt_data {
 int initialized;
 char __buf[256];
};

int acct(const char *);
int async_daemon(void);
int check_utility_compat(const char *);
const char *
  crypt_get_format(void);
char *crypt_r(const char *, const char *, struct crypt_data *);
int crypt_set_format(const char *);
int dup3(int, int, int);
int eaccess(const char *, int);
void endusershell(void);
int exect(const char *, char * const *, char * const *);
int execvP(const char *, const char *, char * const *);
int feature_present(const char *);
char *fflagstostr(u_long);
int getdomainname(char *, int);
int getgrouplist(const char *, gid_t, gid_t *, int *);
int getloginclass(char *, size_t);
mode_t getmode(const void *, mode_t);
int getosreldate(void);
int getpeereid(int, uid_t *, gid_t *);
int getresgid(gid_t *, gid_t *, gid_t *);
int getresuid(uid_t *, uid_t *, uid_t *);
char *getusershell(void);
int initgroups(const char *, gid_t);
int iruserok(unsigned long, int, const char *, const char *);
int iruserok_sa(const void *, int, int, const char *, const char *);
int issetugid(void);
void __FreeBSD_libc_enter_restricted_mode(void);
long lpathconf(const char *, int);

char *mkdtemp(char *);



int mknod(const char *, mode_t, dev_t);



int mkstemp(char *);


int mkstemps(char *, int);

char *mktemp(char *);


int nfssvc(int, void *);
int nlm_syscall(int, int, int, char **);
int pipe2(int *, int);
int profil(char *, size_t, vm_offset_t, int);
int rcmd(char **, int, const char *, const char *, const char *, int *);
int rcmd_af(char **, int, const char *,
  const char *, const char *, int *, int);
int rcmdsh(char **, int, const char *,
  const char *, const char *, const char *);
char *re_comp(const char *);
int re_exec(const char *);
int reboot(int);
int revoke(const char *);
pid_t rfork(int);
pid_t rfork_thread(int, void *, int (*)(void *), void *);
int rresvport(int *);
int rresvport_af(int *, int);
int ruserok(const char *, int, const char *, const char *);






int setdomainname(const char *, int);
int setgroups(int, const gid_t *);
void sethostid(long);
int sethostname(const char *, int);
int setlogin(const char *);
int setloginclass(const char *);
void *setmode(const char *);
int setpgrp(pid_t, pid_t);
void setproctitle(const char *_fmt, ...) __attribute__((__format__ (__printf0__, 1, 2)));
int setresgid(gid_t, gid_t, gid_t);
int setresuid(uid_t, uid_t, uid_t);
int setrgid(gid_t);
int setruid(uid_t);
void setusershell(void);
int strtofflags(char **, u_long *, u_long *);
int swapon(const char *);
int swapoff(const char *);
int syscall(int, ...);
off_t __syscall(quad_t, ...);
int undelete(const char *);
int unwhiteout(const char *);
void *valloc(size_t);



extern int optreset;



ssize_t write_c(int, __attribute__((address_space(200))) const void *, size_t);
typedef voidpf (*alloc_func) (voidpf opaque, uInt items, uInt size);
typedef void (*free_func) (voidpf opaque, voidpf address);

struct internal_state;

typedef struct z_stream_s {
            Bytef *next_in;
    uInt avail_in;
    uLong total_in;

    Bytef *next_out;
    uInt avail_out;
    uLong total_out;

            char *msg;
    struct internal_state *state;

    alloc_func zalloc;
    free_func zfree;
    voidpf opaque;

    int data_type;

    uLong adler;
    uLong reserved;
} z_stream;

typedef z_stream *z_streamp;





typedef struct gz_header_s {
    int text;
    uLong time;
    int xflags;
    int os;
    Bytef *extra;
    uInt extra_len;
    uInt extra_max;
    Bytef *name;
    uInt name_max;
    Bytef *comment;
    uInt comm_max;
    int hcrc;
    int done;

} gz_header;

typedef gz_header *gz_headerp;
typedef unsigned (*in_func) (void *, unsigned char * *);

typedef int (*out_func) (void *, unsigned char *, unsigned);
typedef struct gzFile_s *gzFile;

struct gzFile_s {
    unsigned have;
    unsigned char *next;
    off_t pos;
};





int bcmp(const void *, const void *, size_t) __attribute__((__pure__));
void bcopy(const void *, void *, size_t);
void bzero(void *, size_t);


void explicit_bzero(void *, size_t);


int ffs(int) __attribute__((__const__));


int ffsl(long) __attribute__((__const__));
int ffsll(long long) __attribute__((__const__));
int fls(int) __attribute__((__const__));
int flsl(long) __attribute__((__const__));
int flsll(long long) __attribute__((__const__));


char *index(const char *, int) __attribute__((__pure__));
char *rindex(const char *, int) __attribute__((__pure__));

int strcasecmp(const char *, const char *) __attribute__((__pure__));
int strncasecmp(const char *, const char *, size_t) __attribute__((__pure__));



typedef struct _xlocale *locale_t;
int strcasecmp_l(const char *, const char *, locale_t);
int strncasecmp_l(const char *, const char *, size_t, locale_t);
void *memccpy(void * restrict, const void * restrict, int, size_t);

void *memchr(const void *, int, size_t) __attribute__((__pure__));

void *memrchr(const void *, int, size_t) __attribute__((__pure__));

int memcmp(const void *, const void *, size_t) __attribute__((__pure__));
void *memcpy(void * restrict, const void * restrict, size_t);

void *memmem(const void *, size_t, const void *, size_t) __attribute__((__pure__));

void *memmove(void *, const void *, size_t);
void *memset(void *, int, size_t);

char *stpcpy(char * restrict, const char * restrict);
char *stpncpy(char * restrict, const char * restrict, size_t);


char *strcasestr(const char *, const char *) __attribute__((__pure__));

char *strcat(char * restrict, const char * restrict);
char *strchr(const char *, int) __attribute__((__pure__));

char *strchrnul(const char*, int) __attribute__((__pure__));

int strcmp(const char *, const char *) __attribute__((__pure__));
int strcoll(const char *, const char *);
char *strcpy(char * restrict, const char * restrict);
size_t strcspn(const char *, const char *) __attribute__((__pure__));

char *strdup(const char *) __attribute__((__malloc__));

char *strerror(int);

int strerror_r(int, char *, size_t);


size_t strlcat(char * restrict, const char * restrict, size_t);
size_t strlcpy(char * restrict, const char * restrict, size_t);

size_t strlen(const char *) __attribute__((__pure__));

void strmode(int, char *);

char *strncat(char * restrict, const char * restrict, size_t);
int strncmp(const char *, const char *, size_t) __attribute__((__pure__));
char *strncpy(char * restrict, const char * restrict, size_t);

char *strndup(const char *, size_t) __attribute__((__malloc__));
size_t strnlen(const char *, size_t) __attribute__((__pure__));


char *strnstr(const char *, const char *, size_t) __attribute__((__pure__));

char *strpbrk(const char *, const char *) __attribute__((__pure__));
char *strrchr(const char *, int) __attribute__((__pure__));

char *strsep(char **, const char *);


char *strsignal(int);

size_t strspn(const char *, const char *) __attribute__((__pure__));
char *strstr(const char *, const char *) __attribute__((__pure__));
char *strtok(char * restrict, const char * restrict);

char *strtok_r(char *, const char *, char **);

size_t strxfrm(char * restrict, const char * restrict, size_t);
__attribute__((address_space(200))) const void *
  memchr_c_const(__attribute__((address_space(200))) const void *, int, size_t) __attribute__((__pure__));
__attribute__((address_space(200))) void
 *memchr_c(__attribute__((address_space(200))) const void *, int, size_t) __attribute__((__pure__));
int memcmp_c(__attribute__((address_space(200))) const void *, __attribute__((address_space(200))) const void *, size_t)
     __attribute__((__pure__));
__attribute__((address_space(200))) void
 *memcpy_c(__attribute__((address_space(200))) void * restrict,
     __attribute__((address_space(200))) const void * restrict, size_t);
void *memcpy_c_fromcap(void * restrict,
     __attribute__((address_space(200))) const void * restrict, size_t);
__attribute__((address_space(200))) void
 *memcpy_c_tocap(__attribute__((address_space(200))) void * restrict,
     const void * restrict, size_t);
__attribute__((address_space(200))) void
 *memmove_c(__attribute__((address_space(200))) void * restrict,
     __attribute__((address_space(200))) const void * restrict, size_t);
__attribute__((address_space(200))) void
 *memset_c(__attribute__((address_space(200))) void *, int, size_t);

__attribute__((address_space(200))) char
 *strchr_c(__attribute__((address_space(200))) const char *, int) __attribute__((__pure__));
int strcmp_c(__attribute__((address_space(200))) const char *,
     __attribute__((address_space(200))) const char *s2) __attribute__((__pure__));
__attribute__((address_space(200))) char
 *strcpy_c(__attribute__((address_space(200))) char * restrict,
     __attribute__((address_space(200))) const char * restrict);
int strncmp_c(__attribute__((address_space(200))) const char *, __attribute__((address_space(200))) const char *,
     size_t) __attribute__((__pure__));
__attribute__((address_space(200))) char
*strncpy_c(__attribute__((address_space(200))) char * restrict, __attribute__((address_space(200))) const char * restrict,
     size_t);
char *strncpy_c_fromcap(char * restrict,
     __attribute__((address_space(200))) const char * restrict, size_t);
__attribute__((address_space(200))) char
 *strncpy_c_tocap(__attribute__((address_space(200))) char * restrict,
     const char * restrict, size_t);
size_t strnlen_c(__attribute__((address_space(200))) const char *, size_t) __attribute__((__pure__));


int timingsafe_bcmp(const void *, const void *, size_t);
int timingsafe_memcmp(const void *, const void *, size_t);




int strcoll_l(const char *, const char *, locale_t);
size_t strxfrm_l(char *, const char *, size_t, locale_t);
typedef struct {
 int quot;
 int rem;
} div_t;

typedef struct {
 long quot;
 long rem;
} ldiv_t;
extern int __mb_cur_max;
extern int ___mb_cur_max(void);


void abort(void);
int abs(int) __attribute__((__const__));
int atexit(void (*)(void));
double atof(const char *);
int atoi(const char *);
long atol(const char *);
void *bsearch(const void *, const void *, size_t,
     size_t, int (*)(const void *, const void *));
void *calloc(size_t, size_t) __attribute__((__malloc__)) __attribute__((__warn_unused_result__))
                                     ;
div_t div(int, int) __attribute__((__const__));
void exit(int);
void free(void *);
char *getenv(const char *);
long labs(long) __attribute__((__const__));
ldiv_t ldiv(long, long) __attribute__((__const__));
void *malloc(size_t) __attribute__((__malloc__)) __attribute__((__warn_unused_result__)) ;
int mblen(const char *, size_t);
size_t mbstowcs(wchar_t * restrict , const char * restrict, size_t);
int mbtowc(wchar_t * restrict, const char * restrict, size_t);
void qsort(void *, size_t, size_t,
     int (*)(const void *, const void *));
int rand(void);
void *realloc(void *, size_t) __attribute__((__warn_unused_result__)) ;
void srand(unsigned);
double strtod(const char * restrict, char ** restrict);
float strtof(const char * restrict, char ** restrict);
long strtol(const char * restrict, char ** restrict, int);
long double
  strtold(const char * restrict, char ** restrict);
unsigned long
  strtoul(const char * restrict, char ** restrict, int);
int system(const char *);
int wctomb(char *, wchar_t);
size_t wcstombs(char * restrict, const wchar_t * restrict, size_t);
typedef struct {
 long long quot;
 long long rem;
} lldiv_t;


long long
  atoll(const char *);

long long
  llabs(long long) __attribute__((__const__));

lldiv_t lldiv(long long, long long) __attribute__((__const__));

long long
  strtoll(const char * restrict, char ** restrict, int);

unsigned long long
  strtoull(const char * restrict, char ** restrict, int);


void _Exit(int);






void * aligned_alloc(size_t, size_t) __attribute__((__malloc__))
                    ;
int at_quick_exit(void (*)(void));
void  quick_exit(int);





char *realpath(const char * restrict, char * restrict);


int rand_r(unsigned *);


int posix_memalign(void **, size_t, size_t) __attribute__((__nonnull__(1)));
int setenv(const char *, const char *, int);
int unsetenv(const char *);



int getsubopt(char **, char *const *, char **);
long a64l(const char *);
double drand48(void);

double erand48(unsigned short[3]);


int grantpt(int);
char *initstate(unsigned int, char *, size_t);
long jrand48(unsigned short[3]);
char *l64a(long);
void lcong48(unsigned short[7]);
long lrand48(void);




long mrand48(void);
long nrand48(unsigned short[3]);
int posix_openpt(int);
char *ptsname(int);
int putenv(char *);
long random(void);
unsigned short
 *seed48(unsigned short[3]);
char *setstate( char *);
void srand48(long);
void srandom(unsigned int);
int unlockpt(int);



extern const char *malloc_conf;
extern void (*malloc_message)(void *, const char *);
void abort2(const char *, int, void **) __attribute__((__noreturn__));
__uint32_t
  arc4random(void);
void arc4random_addrandom(unsigned char *, int);
void arc4random_buf(void *, size_t);
void arc4random_stir(void);
__uint32_t
  arc4random_uniform(__uint32_t);





char *getbsize(int *, long *);

char *cgetcap(char *, const char *, int);
int cgetclose(void);
int cgetent(char **, char **, const char *);
int cgetfirst(char **, char **);
int cgetmatch(const char *, const char *);
int cgetnext(char **, char **);
int cgetnum(char *, const char *, long *);
int cgetset(const char *);
int cgetstr(char *, const char *, char **);
int cgetustr(char *, const char *, char **);

int daemon(int, int);
char *devname(__dev_t, __mode_t);
char *devname_r(__dev_t, __mode_t, char *, int);
char *fdevname(int);
char *fdevname_r(int, char *, int);
int getloadavg(double [], int);
const char *
  getprogname(void);

int heapsort(void *, size_t, size_t, int (*)(const void *, const void *));





int l64a_r(long, char *, int);
int mergesort(void *, size_t, size_t, int (*)(const void *, const void *));



int mkostemp(char *, int);
int mkostemps(char *, int, int);
void qsort_r(void *, size_t, size_t, void *,
     int (*)(void *, const void *, const void *));
int radixsort(const unsigned char **, int, const unsigned char *,
     unsigned);
void *reallocarray(void *, size_t, size_t) __attribute__((__warn_unused_result__))
                    ;
void *reallocf(void *, size_t) ;
int rpmatch(const char *);
void setprogname(const char *);
int sradixsort(const unsigned char **, int, const unsigned char *,
     unsigned);
void sranddev(void);
void srandomdev(void);
long long
 strtonum(const char *, long long, long long, const char **);


__int64_t
  strtoq(const char *, char **, int);
__uint64_t
  strtouq(const char *, char **, int);


long strtol_c(__attribute__((address_space(200))) const char * restrict,
     __attribute__((address_space(200))) char ** restrict, int);
long double
  strtold_c(__attribute__((address_space(200))) const char * restrict,
     __attribute__((address_space(200))) char ** restrict);


int timsort(void *base, size_t nel, size_t width,
     int (*compar) (const void *, const void *));

extern char *suboptarg;
typedef unsigned char uch;
typedef uch uchf;
typedef unsigned short ush;
typedef ush ushf;
typedef unsigned long ulg;

extern char * const z_errmsg[10];
    extern uLong adler32_combine64 (uLong, uLong, off_t);
    extern uLong crc32_combine64 (uLong, uLong, off_t);
   voidpf zcalloc (voidpf opaque, unsigned items, unsigned size);

   void zcfree (voidpf opaque, voidpf ptr);

typedef __off_t fpos_t;
struct __sbuf {
 unsigned char *_base;
 int _size;
};
struct __sFILE {
 unsigned char *_p;
 int _r;
 int _w;
 short _flags;
 short _file;
 struct __sbuf _bf;
 int _lbfsize;


 void *_cookie;
 int (*_close)(void *);
 int (*_read)(void *, char *, int);
 fpos_t (*_seek)(void *, fpos_t, int);
 int (*_write)(void *, const char *, int);


 struct __sbuf _ub;
 unsigned char *_up;
 int _ur;


 unsigned char _ubuf[3];
 unsigned char _nbuf[1];


 struct __sbuf _lb;


 int _blksize;
 fpos_t _offset;

 struct pthread_mutex *_fl_mutex;
 struct pthread *_fl_owner;
 int _fl_count;
 int _orientation;
 __mbstate_t _mbstate;
 int _flags2;
};


typedef struct __sFILE FILE;



extern FILE *__stdinp;
extern FILE *__stdoutp;
extern FILE *__stderrp;
void clearerr(FILE *);
int fclose(FILE *);
int feof(FILE *);
int ferror(FILE *);
int fflush(FILE *);
int fgetc(FILE *);
int fgetpos(FILE * restrict, fpos_t * restrict);
char *fgets(char * restrict, int, FILE * restrict);
FILE *fopen(const char * restrict, const char * restrict);
int fprintf(FILE * restrict, const char * restrict, ...) __attribute__((__format__ (__printf__, 2, 3)));
int fputc(int, FILE *);
int fputs(const char * restrict, FILE * restrict);
size_t fread(void * restrict, size_t, size_t, FILE * restrict);
FILE *freopen(const char * restrict, const char * restrict, FILE * restrict);
int fscanf(FILE * restrict, const char * restrict, ...) __attribute__((__format__ (__scanf__, 2, 3)));;
int fseek(FILE *, long, int);
int fsetpos(FILE *, const fpos_t *);
long ftell(FILE *);
size_t fwrite(const void * restrict, size_t, size_t, FILE * restrict);
int getc(FILE *);
int getchar(void);
char *gets(char *);
void perror(const char *);
int printf(const char * restrict, ...) __attribute__((__format__ (__printf__, 1, 2)));
int putc(int, FILE *);
int putchar(int);
int puts(const char *);
int remove(const char *);
int rename(const char *, const char *);
void rewind(FILE *);
int scanf(const char * restrict, ...) __attribute__((__format__ (__scanf__, 1, 2)));
void setbuf(FILE * restrict, char * restrict);
int setvbuf(FILE * restrict, char * restrict, int, size_t);
int sprintf(char * restrict, const char * restrict, ...) __attribute__((__format__ (__printf__, 2, 3)));
int sscanf(const char * restrict, const char * restrict, ...) __attribute__((__format__ (__scanf__, 2, 3)));
FILE *tmpfile(void);
char *tmpnam(char *);
int ungetc(int, FILE *);
int vfprintf(FILE * restrict, const char * restrict,
     __va_list);
int vprintf(const char * restrict, __va_list);
int vsprintf(char * restrict, const char * restrict,
     __va_list);


int snprintf(char * restrict, size_t, const char * restrict,
     ...) __attribute__((__format__ (__printf__, 3, 4)));
int vfscanf(FILE * restrict, const char * restrict, __va_list)
     __attribute__((__format__ (__scanf__, 2, 0)));
int vscanf(const char * restrict, __va_list) __attribute__((__format__ (__scanf__, 1, 0)));
int vsnprintf(char * restrict, size_t, const char * restrict,
     __va_list) __attribute__((__format__ (__printf__, 3, 0)));
int vsscanf(const char * restrict, const char * restrict, __va_list)
     __attribute__((__format__ (__scanf__, 2, 0)));
char *ctermid(char *);
FILE *fdopen(int, const char *);
int fileno(FILE *);



int pclose(FILE *);
FILE *popen(const char *, const char *);



int ftrylockfile(FILE *);
void flockfile(FILE *);
void funlockfile(FILE *);





int getc_unlocked(FILE *);
int getchar_unlocked(void);
int putc_unlocked(int, FILE *);
int putchar_unlocked(int);


void clearerr_unlocked(FILE *);
int feof_unlocked(FILE *);
int ferror_unlocked(FILE *);
int fileno_unlocked(FILE *);



int fseeko(FILE *, __off_t, int);
__off_t ftello(FILE *);



int getw(FILE *);
int putw(int, FILE *);



char *tempnam(const char *, const char *);



FILE *fmemopen(void * restrict, size_t, const char * restrict);
ssize_t getdelim(char ** restrict, size_t * restrict, int,
     FILE * restrict);
FILE *open_memstream(char **, size_t *);
int renameat(int, const char *, int, const char *);
int vdprintf(int, const char * restrict, __va_list) __attribute__((__format__ (__printf__, 2, 0)));

ssize_t getline(char ** restrict, size_t * restrict, FILE * restrict);
int dprintf(int, const char * restrict, ...) __attribute__((__format__ (__printf__, 2, 3)));






int asprintf(char **, const char *, ...) __attribute__((__format__ (__printf__, 2, 3)));
char *ctermid_r(char *);
void fcloseall(void);
int fdclose(FILE *, int *);
char *fgetln(FILE *, size_t *);
const char *fmtcheck(const char *, const char *) __attribute__((__format_arg__ (2)));
int fpurge(FILE *);
void setbuffer(FILE *, char *, int);
int setlinebuf(FILE *);
int vasprintf(char **, const char *, __va_list)
     __attribute__((__format__ (__printf__, 2, 0)));






extern const int sys_nerr;
extern const char * const sys_errlist[];




FILE *funopen(const void *,
     int (*)(void *, char *, int),
     int (*)(void *, const char *, int),
     fpos_t (*)(void *, fpos_t, int),
     int (*)(void *));



typedef __ssize_t cookie_read_function_t(void *, char *, size_t);
typedef __ssize_t cookie_write_function_t(void *, const char *, size_t);
typedef int cookie_seek_function_t(void *, off64_t *, int);
typedef int cookie_close_function_t(void *);
typedef struct {
 cookie_read_function_t *read;
 cookie_write_function_t *write;
 cookie_seek_function_t *seek;
 cookie_close_function_t *close;
} cookie_io_functions_t;
FILE *fopencookie(void *, const char *, cookie_io_functions_t);
int __srget(FILE *);
int __swbuf(int, FILE *);







static __inline int __sputc(int _c, FILE *_p) {
 if (--_p->_w >= 0 || (_p->_w >= _p->_lbfsize && (char)_c != '\n'))
  return (*_p->_p++ = _c);
 else
  return (__swbuf(_c, _p));
}
extern int __isthreaded;
struct flock {
 off_t l_start;
 off_t l_len;
 pid_t l_pid;
 short l_type;
 short l_whence;
 int l_sysid;
};






struct __oflock {
 off_t l_start;
 off_t l_len;
 pid_t l_pid;
 short l_type;
 short l_whence;
};
int open(const char *, int, ...);
int creat(const char *, mode_t);
int fcntl(int, int, ...);

int flock(int, int);


int openat(int, const char *, int, ...);


int posix_fadvise(int, off_t, off_t, int);
int posix_fallocate(int, off_t, off_t);
int * __error(void);








    extern gzFile gzopen64 (const char *, const char *);
    extern off_t gzseek64 (gzFile, off_t, int);
    extern off_t gztell64 (gzFile);
    extern off_t gzoffset64 (gzFile);
typedef struct {

    struct gzFile_s x;




    int mode;
    int fd;
    char *path;
    unsigned size;
    unsigned want;
    unsigned char *in;
    unsigned char *out;
    int direct;

    int how;
    off_t start;
    int eof;
    int past;

    int level;
    int strategy;

    off_t skip;
    int seek;

    int err;
    char *msg;

    z_stream strm;
} gz_state;
typedef gz_state *gz_statep;


void gz_error (gz_statep, int, const char *);


        char * const z_errmsg[10] = {
    ( char *)"need dictionary",
    ( char *)"stream end",
    ( char *)"",
    ( char *)"file error",
    ( char *)"stream error",
    ( char *)"data error",
    ( char *)"insufficient memory",
    ( char *)"buffer error",
    ( char *)"incompatible version",
    ( char *)""
};


const char * zlibVersion()
{
    return "1.2.11";
}

uLong zlibCompileFlags()
{
    uLong flags;

    flags = 0;
    switch ((int)(sizeof(uInt))) {
    case 2: break;
    case 4: flags += 1; break;
    case 8: flags += 2; break;
    default: flags += 3;
    }
    switch ((int)(sizeof(uLong))) {
    case 2: break;
    case 4: flags += 1 << 2; break;
    case 8: flags += 2 << 2; break;
    default: flags += 3 << 2;
    }
    switch ((int)(sizeof(voidpf))) {
    case 2: break;
    case 4: flags += 1 << 4; break;
    case 8: flags += 2 << 4; break;
    default: flags += 3 << 4;
    }
    switch ((int)(sizeof(off_t))) {
    case 2: break;
    case 4: flags += 1 << 6; break;
    case 8: flags += 2 << 6; break;
    default: flags += 3 << 6;
    }
    return flags;
}
const char * zError(err)
    int err;
{
    return z_errmsg[2 -(err)];
}
voidpf zcalloc (opaque, items, size)
    voidpf opaque;
    unsigned items;
    unsigned size;
{
    (void)opaque;
    return sizeof(uInt) > 2 ? (voidpf)malloc(items * size) :
                              (voidpf)calloc(items, size);
}

void zcfree (opaque, ptr)
    voidpf opaque;
    voidpf ptr;
{
    (void)opaque;
    free(ptr);
}
