extern char *lib2_fun(void);

static void ccopy(char *src, char *des, unsigned int n);
static void fill(char b, char *des, unsigned int n);

char *lib2_fun(void)
{
	char *a, *b;
	char c[100];

	a = (char *)malloc(0x100 * sizeof(char));
	b = (char *)malloc(0x100 * sizeof(char));

	fill('z', a, 0x100);
	ccopy(a, b, 0x100);

	return a;
}

void ccopy(char *src, char *des, unsigned int n)
{
	unsigned int i = 0;

	for (i = 0 ; i < n ; i++)
		des[i] = src[i];
}

void fill(char b, char *des, unsigned int n)
{
	unsigned int i = 0;

	for (i = 0 ; i < n ; i++)
		des[i] = b;
}
