#include <cheri/cheri.h>
#include <cheri/cheric.h>
#include <cheri/sandbox.h>

#include <stdio.h>

struct cheri_object mainmod;
static struct sandbox_object *s_main_objectp;

#define	CHERI_CCALLER_MAIN					\
    __attribute__((cheri_ccall))				\
    __attribute__((cheri_method_suffix("_cap")))		\
    __attribute__((cheri_method_class(mainmod)))

CHERI_CCALLER_MAIN
extern int t_main(int argc, char *argv[]);

int main(int argc, char *argv[])
{
	sandbox_chain_load("mainmod", &s_main_objectp);
	mainmod = sandbox_object_getobject(s_main_objectp);

	t_main(argc, argv);

	return 0;
}
