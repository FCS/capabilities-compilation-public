struct cheri_object {
#if __has_feature(capabilities)
        __capability void	*co_codecap;
        __capability void	*co_datacap;
#else
#error "Needs capabilities"
#endif
};

#define MAX_CLASS_NAME 0x20
#define MAX_DEPS 0x20

/* Representation of an external dependency */
struct external_dep {
        char libname[MAX_CLASS_NAME];
        struct cheri_object lib;
};

struct sandbox_metadata {
        long	sbm_heapbase;			/* Offset: 0 */
        long	sbm_heaplen;			/* Offset: 8 */
        unsigned long	_sbm_reserved0;			/* Offset: 16 */
        unsigned long	_sbm_reserved1;			/* Offset: 24 */
        struct cheri_object	sbm_system_object;	/* Offset: 32 */
#if __has_feature(capabilities)
        __capability unsigned long	*sbm_vtable;		/* Cap-offset: 2 */
        __capability void	*sbm_stackcap;		/* Cap-offset: 3 */
#else
        struct chericap	sbm_vtable;
        struct chericap	sbm_stackcap;
#endif
        unsigned long sbm_depnum;
        struct external_dep sbm_deps[MAX_DEPS];  /* Dependencies */
};

/* A hack? It won't compile else */
void invoke(void);
void invoke(void) { abort(); }

static __inline struct external_dep *
sandbox_dep_lookup(struct external_dep *d, unsigned long depnum, char *name)
{
        int i;

        for (i = 0 ; i < depnum ; i++)
                if (!strncmp(d[i].libname, name, MAX_CLASS_NAME))
                        return &d[i];

        return 0;
}

#define GET_METADATA() ({ __attribute__((address_space(200))) void *_cap; _cap = ({ __attribute__((address_space(200))) void *_cap; __asm volatile ("cmove %0, $c" "0" : "=C" (_cap)); _cap; }); _cap = __builtin_cheri_cap_offset_set( ((__attribute__((address_space(200))) void *)(__intcap_t)(__attribute__((address_space(200))) const void *)((_cap))), (0x1000)); _cap; });
extern struct cheri_object mainmod;
struct cheri_object lib1;
struct cheri_object lib2;
__attribute__((constructor)) static void sandboxes_init(void)
{
    struct sandbox_metadata * mdata;
    struct external_dep * lib1_dep, * lib2_dep;
    mdata = (struct sandbox_metadata *) GET_METADATA();
    lib1_dep = sandbox_dep_lookup(mdata->sbm_deps,
                                  mdata->sbm_depnum,
                                  "lib1");
    lib1 = lib1_dep->lib;
    lib2_dep = sandbox_dep_lookup(mdata->sbm_deps,
                                  mdata->sbm_depnum,
                                  "lib2");
    lib2 = lib2_dep->lib;
}
__attribute__((cheri_ccall)) __attribute__((cheri_method_suffix("_cap"))) __attribute__((cheri_method_class(lib1))) extern char * lib1_fun(void);
__attribute__((cheri_ccall)) __attribute__((cheri_method_suffix("_cap"))) __attribute__((cheri_method_class(lib2))) extern char * lib2_fun(void);
__attribute__((cheri_ccallee)) __attribute__((cheri_method_class(mainmod))) extern int t_main(int argc,
                                                                                         char * argv[]);
int t_main(int argc, char * argv[])
{
        printf("wazzza\n");

    lib1_fun();
    lib2_fun();
    return 0;
}
