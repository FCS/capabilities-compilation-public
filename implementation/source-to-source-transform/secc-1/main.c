extern char *lib1_fun(void);
extern char *lib2_fun(void);

extern int main(int argc, char *argv[]);

int main(int argc, char *argv[])
{
	lib1_fun();
	lib2_fun();

	return 0;
}
