{-# LANGUAGE FlexibleInstances #-}
{-# OPTIONS_GHC -fwarn-incomplete-patterns #-}

-- Things to consider when running the c2c through pre-processed CHERIc files
-- - The _noreturn typedefs
-- - The __max_aling_t struct

-- Other things to consider
-- - Rename main.c, the cheri_object and the main function (def + decl) accordingly
-- - No init being created automatically

module Main where
import System.Environment
import System.FilePath
import System.Directory
import System.Exit
import System.IO
import Control.Monad
import Debug.Trace
import Text.PrettyPrint.HughesPJ
import Data.List

import Language.C
import Language.C.Analysis
--import Language.C.System.GCC   -- preprocessor used
import Language.C.Data.Ident
import Language.C.Analysis.TravMonad
import Language.C.Analysis.DefTable
import Language.C.Analysis.TypeUtils
import Language.C.Data.InputStream
import Language.C.Data.Position
import Language.C.Analysis.Builtins
import Language.C.Analysis.SemRep

import qualified Data.Map as Map
import Data.Maybe
import Data.Either

import Libc
import Posix
import FreeBSDCalls

usageMsg :: String -> String
usageMsg prg = render $
  text "Usage:" <+> text prg <+> hsep (map text ["input-dir output-dir"])

main = do
    let usageErr = (hPutStrLn stderr (usageMsg "./progname") >> exitWith (ExitFailure 1))

    args <- getArgs
    when (length args < 2) usageErr
    let indir = head args
    let outdir = last args

    exin <- doesDirectoryExist indir
    exout <- doesDirectoryExist outdir
    when (exin == False || exout == False) $ exitWith (ExitFailure 2)

    contents <- fmap (fmap ((indir ++ "/") ++)) $ listDirectory indir
    let files = filter (\s -> takeExtension s == ".c") contents

    -- parse
    asts <- mapM (\f -> errorOnLeftM "Parse Error" $ parseCFilePre f) files

    -- Secc analysis
    libs <- mapM (\a -> errorOnLeft "Semantic Error" $ seccAnalysis a) asts

    -- Transform
    let newAsts = fmap (cheriTransform libs) asts
--    mapM (\ast -> print $ pretty $ ast) newAsts

    let newfilesZ = zip newAsts $ fmap (((outdir ++ "/") ++) . addPrefix . takeFileName) files
    headerStr <- readFile "./resources/cheri_minimal.h"
    mapM (\(ast, f) -> writeFile f $ (++) headerStr $ show $ pretty $ ast) newfilesZ

errorOnLeft :: (Show a) => String -> (Either a b) -> IO b
errorOnLeft msg = either (error . ((msg ++ ": ")++).show) return
errorOnLeftM :: (Show a) => String -> IO (Either a b) -> IO b
errorOnLeftM msg action = action >>= errorOnLeft msg

-- New representation of a dependencies based on the results of the semantic analysis
-- An Ident as name
-- Two mappings (exports - dependencies) between identifiers and their respective declarations
data Lib = Lib { lid :: Ident
               , exps :: Map.Map Ident IdentDecl
               , deps :: Map.Map Ident IdentDecl}

idFromAst :: CTranslUnit -> Ident
idFromAst ast = internalIdent $ takeBaseName $ fromJust $ fileOfNode $ nodeInfo ast

-- Analyze a translation unit for dependency resolution and transformation
-- First declaration table holds exported function declarations, while second holds dependencies
-- TODO Remove libc functions from the dependency list
seccAnalysis :: CTranslUnit -> Either [CError] Lib
seccAnalysis ast =
  case runTrav_ $ analyseAST ast of
      Right (y, _) -> Right $ Lib { lid = idFromAst ast,
                                    exps = onlyFunDef y,
                                    deps = Map.difference (onlyFunDecl y) (builtinsDecl)}
      Left err -> Left err
  where
    onlyFunDef g = Map.filter isFunDefExt (gObjs g)
    onlyFunDecl g = Map.filter isFunDeclExt (gObjs g)
    isFunDefExt (FunctionDef f) = (not . isInline . functionAttrs $ getVarDecl f) &&
                                  (declLinkage f == ExternalLinkage)
    isFunDefExt _ = False
    isFunDeclExt (Declaration (Decl (VarDecl x (DeclAttrs a (FunLinkage s) _) t) _)) =
      s == ExternalLinkage && isFunctionType t && (not . isIgnoredFun) x && (not . isInline) a
    isFunDeclExt _ = False
    isIgnoredFun (VarName (Ident n _ _) _) = elem n (ignoreList ++ posixList ++ freeList)
    isIgnoredFun _ = False
    builtinsDecl = gObjs $ globalDefs builtins

-- Create a new AST for given translation unit
-- Arguments are the results of the semantic analysis for all modules and the target module
cheriTransform :: [Lib] -> CTranslUnit  -> CTranslUnit
cheriTransform libs ast =
  let
    clib = head $ filter (\l -> idFromAst ast == lid l) libs
    modObj = CDeclExt $ cheriObjectDecl True $ addPrefixId $ lid clib -- TODO: object produced even if no exports
    isRelevant = \lib -> any (\(id, _) -> Map.member id $ exps lib) $ Map.toList $ deps clib
    relevantLibs = filter isRelevant libs -- Libraries that hold definitions of interest
    extObj = fmap (CDeclExt . cheriObjectDecl False . addPrefixId . lid) relevantLibs
    constructor = fromParse $ flip parseC (initPos []) $ inputStreamFromString $
                  constStr $ fmap (identToString . addPrefixId . lid) relevantLibs
    (expnodes, extnodes) = (fmap nodeInfo $ exps clib, fmap nodeInfo $ deps clib) -- Nodes from decls
    extPairs = fmap (\(n, i) -> (n, fromJust i)) $
      Map.filter (\p -> isJust $ snd p) $
      Map.mapWithKey (\k n -> (n, locateLib k libs)) extnodes -- Trickier case
    expPairs = fmap (\n -> (n,lid clib)) expnodes
    transforms = transformDecl extPairs Ccaller $ transformDecl expPairs Ccallee $ getDecls ast
  in
    CTranslUnit (modObj:extObj ++ constructor ++ transforms) undefNode
  where
    getDecls (CTranslUnit cdecls _) = cdecls
    fromParse (Left i) = error $ show i
    fromParse (Right cTUnit) = getDecls cTUnit

locateLib :: Ident -> [Lib] -> Maybe Ident
locateLib ident libs = case find (\l -> Map.member ident $ exps l) libs of
                      Just i -> Just $ lid i
                      Nothing -> trace ("Unable to locate symbol:" ++ show ident) $ Nothing

transformDecl :: Map.Map Ident (NodeInfo, Ident) -> CheriCcallType -> [CExtDecl] -> [CExtDecl]
transformDecl identMap dir decls =
  let
    getLibIdent = \ident -> snd $ fromJust $ Map.lookup ident identMap
    pairs = foldl (flip pairUp) (fmap (\d -> (Nothing, d)) decls) $ fmap fst $ Map.toList $ identMap
    transformPair = \(i, decl) -> case i of
                                     Just ident -> cheriAddAttr (getLibIdent ident) dir decl
                                     Nothing -> decl
  in
    fmap transformPair pairs

-- Figure out which external declaration should be transformed for given identifier
pairUp :: Ident -> [(Maybe Ident, CExtDecl)] -> [(Maybe Ident, CExtDecl)]
pairUp ident [] = []
pairUp ident (d:ds) = case identFromDecl $ snd d of
                                  Just i -> case ident == i of
                                    True -> (Just ident, snd d) : ds
                                    False -> d : pairUp ident ds
                                  _ -> d : pairUp ident ds

-- Ugly!
identFromDecl :: CExtDecl -> Maybe Ident
identFromDecl (CDeclExt (CDecl _ [(Just (CDeclr (Just ident) _ _ _ _) ,Nothing, Nothing)] _)) = Just ident
identFromDecl (CFDefExt (CFunDef _ (CDeclr (Just ident) _ _ _ _) _ _ _)) = Just ident
identFromDecl _ = Nothing

cheriAddAttr :: Ident -> CheriCcallType -> CExtDecl -> CExtDecl
cheriAddAttr lid direction (CDeclExt (CDecl x a b)) =
  CDeclExt (CDecl (cheriMakeAttr lid direction ++ x) a b)
cheriAddAttr lid direction (CFDefExt (CFunDef x declr decl stmt n)) =
  (CFDefExt (CFunDef (cheriMakeAttr lid direction ++ x) declr decl stmt n))
cheriAddAttr _ _ a = a

-- TODO: Make it string based
cheriMakeAttr :: Ident -> CheriCcallType -> [CDeclSpec]
cheriMakeAttr clid Ccaller =
  [CTypeQual (CAttrQual (CAttr (internalIdent "cheri_ccall") [] undefNode)),
   CTypeQual (CAttrQual (CAttr (internalIdent "cheri_method_suffix")
                          [CConst $ CStrConst (CString "_cap" False) undefNode] undefNode)),
   CTypeQual (CAttrQual (CAttr (internalIdent "cheri_method_class")
                          [CVar (addPrefixId clid) undefNode] undefNode))]
cheriMakeAttr clid Ccallee =
    [CTypeQual (CAttrQual (CAttr (internalIdent "cheri_ccallee") [] undefNode)),
     CTypeQual (CAttrQual (CAttr (internalIdent "cheri_method_class")
                          [CVar (addPrefixId clid) undefNode] undefNode))]

-- TODO: Make it string based
cheriObjectDecl :: Bool -> Ident -> CDecl
cheriObjectDecl True i = CDecl [CStorageSpec (CExtern undefNode),CTypeSpec
                                 (CSUType (CStruct CStructTag
                                           (Just (internalIdent "cheri_object")) Nothing [] undefNode) undefNode)]
                         [(Just (CDeclr (Just i) [] Nothing [] undefNode),Nothing,Nothing)] undefNode
cheriObjectDecl False i = CDecl [CTypeSpec (CSUType (CStruct CStructTag (Just (internalIdent "cheri_object"))
                                                     Nothing [] undefNode) undefNode)]
                          [(Just (CDeclr (Just i) [] Nothing [] undefNode),Nothing,Nothing)] undefNode


data CheriCcallType = Ccaller | Ccallee deriving (Eq)

cheriPrefix = "cheri_"
addPrefix = (cheriPrefix ++)
addPrefixId (Ident name i n) = Ident (cheriPrefix ++ name) i n

constStr :: [String] -> String
constStr [] = []
constStr (obj:objs) =
  "__attribute__ ((constructor)) static void " ++
      "sandboxes_init(void)" ++
      "{" ++
      "struct sandbox_metadata *mdata;" ++
      "struct external_dep " ++ cheriDecls (obj:objs) ++
      "mdata = (struct sandbox_metadata *)GET_METADATA();" ++
      cheriAssignments (obj:objs) ++
      "}"
  where
    cheriDecls [] = ";"
    cheriDecls (obj:[]) = "*" ++ obj ++ "_dep" ++ cheriDecls []
    cheriDecls (obj:objs) = "*" ++ obj ++ "_dep, " ++ cheriDecls objs
    cheriAssignments [] = []
    cheriAssignments (obj:objs) = obj ++ "_dep = sandbox_dep_lookup(" ++
                                  "mdata->sbm_deps, mdata->sbm_depnum, \"" ++ obj ++ "\");" ++
                                  "if (" ++ obj ++ "_dep)" ++
                                  obj ++ " = " ++ obj ++ "_dep->lib;" ++ cheriAssignments objs
