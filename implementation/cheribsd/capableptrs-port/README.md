# secc fork of CheriBSD

Right now the implementation of the sandbox_chain_load() suffers from a few
limitations and will be subject to a few changes. These limitations are:

- No support for circular dependencies
- Maximum number of external libraries per sandbox is 0x20
- No support for stack-spilled arguments when invoking sandbox functions

# CheriBSD

CheriBSD extends FreeBSD/BERI to implement memory protection and
software compartmentalization features supported by the CHERI ISA.
FreeBSD/BERI is a port of the open-source FreeBSD operating system that
extends support for the Bluespec Extensible RISC implementation (BERI).
General crossbuild and use instructions for FreeBSD/BERI may be found in
the BERI Software Reference. Procedures for building and using
FreeBSD/BERI should entirely apply to CheriBSD, except as documented in
the CHERI Programmer's Guild.

The CheriBSD web page can be found here:
http://www.cl.cam.ac.uk/research/security/ctsrd/cheri/cheribsd.html

The Qemu-CHERI web page may also be useful:
http://www.cl.cam.ac.uk/research/security/ctsrd/cheri/cheri-qemu.html

More information about BERI and CHERI can be found on
http://beri-cpu.org, http://cheri-cpu.org, in the following
Technical Reports:


Bluespec Extensible RISC Implementation: BERI Hardware reference
http://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-868.pdf

Bluespec Extensible RISC Implementation: BERI Software reference
http://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-869.pdf

Capability Hardware Enhanced RISC Instructions: CHERI Programmer's Guide
http://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-877.pdf

Capability Hardware Enhanced RISC Instructions: CHERI Instruction-Set
Architecture
http://www.cl.cam.ac.uk/techreports/UCAM-CL-TR-876.pdf
