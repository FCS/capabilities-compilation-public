/*
 * Description of a method provided by a sandbox to be called via ccall.
 * This data is used to create the 'sandbox class' vtable, resolve symbols
 * to fill in caller .CHERI_CALLER variables, and store the corresponding
 * method numbers in the .CHERI_CALLEE variables of each 'sandbox object'.
 */
struct sandbox_provided_method {
	char		*spm_method;		/* Method name */
	vm_offset_t	 spm_index_offset;	/* Offset of callee variable */
};

/*
 * List of methods provided by a sandbox.  Sandbox method numbers (and
 * by extension vtable indexs) are defined by method position in the
 * spms_methods array.
 *
 * XXX: rename to sandbox_provided_class
 */
struct sandbox_provided_methods {
	char				*spms_class;	/* Class name */
	size_t				 spms_nmethods;	/* Number of methods */
	size_t				 spms_maxmethods; /* Array size */
	struct sandbox_provided_method	*spms_methods;	/* Array of methods */
};

/*
 * List of classes provided by a sandbox binary.  Each binary can
 * support one or more classes.  No two binaries can provide the same
 * class as vtable offsets would be inconsistant.
 */
struct sandbox_provided_classes {
	struct sandbox_provided_methods	**spcs_classes;	/* Class pointers */
	size_t				  spcs_nclasses; /* Number of methods */
	size_t				  spcs_maxclasses; /* Array size */
	size_t				  spcs_nmethods; /* Total methods */
	vm_offset_t			  spcs_base;	/* Base of vtable */
};

/*
 * Description of a method required by a sandbox to be called by ccall.
 */
struct sandbox_required_method {
	char		*srm_class;		/* Class name */
	char		*srm_method;		/* Method name */
	vm_offset_t	 srm_index_offset;	/* Offset of caller variable */
	vm_offset_t	 srm_vtable_offset;	/* Method number */
	bool		 srm_resolved;		/* Resolved? */
};

/*
 * List of methods required by a sandbox (or program).  Sandbox objects
 * must not be created until all symbols are resolved.
 */
struct sandbox_required_methods {
	size_t				 srms_nmethods;	/* Number of methods */
	size_t				 srms_unresolved_methods; /* Number */
	struct sandbox_required_method	*srms_methods;	/* Array of methods */
};
