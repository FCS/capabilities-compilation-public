#include "../include/yaml.h"

YAML_DECLARE(int)
yaml_parser_load(yaml_parser_t *parser, yaml_document_t *document);

int yaml_load_test(char *in, size_t size)
{
        yaml_parser_t *parser;
        yaml_document_t *document;
        yaml_node_t *node;
        int i = 1;

        parser = (yaml_parser_t *) malloc(sizeof(yaml_parser_t));
        document = (yaml_document_t *) malloc(sizeof(yaml_document_t));

        yaml_parser_initialize(parser);

        yaml_parser_set_input_string(parser, in, size);

        if (!yaml_parser_load(parser, document))
                return -1;

        yaml_parser_delete(parser);
        return 0;
}
