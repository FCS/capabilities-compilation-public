#include <stdio.h>
#include "png.h"

int read_png(char *, size_t);

struct cheri_buffer {
        char *p;
        size_t pos;
        size_t len;
};

static struct cheri_buffer buf;
static png_structp png_ptr;
static  png_infop info_ptr;

int read_png(char *in, size_t length)
{


        int sig_read = 0;
        png_uint_32 width, height;
        int bit_depth, color_type, interlace_type;

        buf.p = in;
        buf.pos = 0;
        buf.len = length;

        png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL , NULL);

        info_ptr = png_create_info_struct(png_ptr);

        png_init_io(png_ptr, (void *)&buf);

        png_read_png(png_ptr, info_ptr, PNG_TRANSFORM_IDENTITY, NULL);
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);


        return 0;
}
