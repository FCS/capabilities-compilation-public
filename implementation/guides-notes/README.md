# CHERI/CheriBSD survival kit


This file is a collection of notes regarding the usage of CheriBSD and CHERI in
general. It is not meant as a substitute for the various guides and manuals, but
it should provide complementary (and essential) support for the hopeful CheriBSD
user.

------

## CheriBSD Build/Usage notes


	- Using [cheribuild](https://github.com/CTSRD-CHERI/cheribuild). Remember 
      to install the dependencies. Some changes to utils.py were required 
      (fixed in last versions of cheribuild).
	- To avoid copying error when building freestanding-sdk, set the config paths
      without including symlinks.
	- Use --skip-update when building custom versions using cheribuild.
	- Copy files in the QEMU VM by scp'ing to port 10000 at localhost.


## libcheri


	- Add -lcheri to compile against the sandbox libcheri API.
    $CHERISDK/bin/clang -v -mabi=sandbox --sysroot=$CHERIROOT -B $CHERISDK \
    ccaller.c -o ccaller -lcheri
	- Compartment'd object files should have a certain memory layout. Providing a
    linker script when linking ("sandbox.ld") achieves this.
	- sandbox_init was being called AFTER ccaller's constructors so I moved the
    preparation of the classes to the main function instead.


## Example working compilation


    1. export CHERIROOT=/home/uname/cheri/output/rootfs256
    2. export CHERISDK=/home/uname/cheri/output/sdk
    3. $CHERISDK/bin/clang -g -v -mabi=sandbox --sysroot=$CHERIROOT -B $CHERISDK\
       ccaller.c -o ccaller -lcheri -static
    4. $CHERISDK/bin/clang -g -v -mabi=sandbox --sysroot=$CHERIROOT -B $CHERISDK\
       test_lib.c -o test_lib.co.o -lc_cheri -Wl,--script=$(CHERIROOT)/usr/libdata/ldscripts/sandbox.ld


## Several pitfalls:

  - [!!] When compiling main.co.o, flag "-lc_cheri" must be placed at the end of
       the compilation command.
  - [!!] Successively call sandbox_class_new for every compartment before creating
       the actual objects.
  - [!!] The initialization of the sandbox objects has been moved to the main
       function because of constructor precedence.
