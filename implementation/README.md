### Implementation of the source-to-source transform

#### Contents

- the source-to-source transform for C programs (`source-to-source-transform`)
- the changes to libcheri (`cheribsd`)
  + before the changes (`vanilla`)
  + after the changes (`capableptrs-port`)
- the adaptation of barcode to CapablePtrs (`barcode`)
  + before the port (`vanilla`)
  + after the port (`capableptrs-port`)
- the adaptation of libpng to CapablePtrs (`libpng`)
  + before the port (`vanilla`)
  + after the port (`capableptrs-port`)
- the adaptation of yaml to CapablePtrs (`yaml`)
  + before the port (`vanilla`)
  + after the port (`capableptrs-port`)
- the adaptation of zlib to CapablePtrs (`zlib`)
  + before the port (`vanilla`)
  + after the port (`capableptrs-port`)

