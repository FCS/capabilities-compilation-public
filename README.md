# CapablePtrs: Securely Compiling Partial Programs Using the Pointers-as-Capabilities Principle

### Contents

- the CSF 2021 paper
- the technical report with the proofs
- the implementation

---

### Authors

- Akram El-Korashy
- Stelios Tsampas
- Marco Patrignani
- Dominique Devriese
- Deepak Garg
- Frank Piessens

